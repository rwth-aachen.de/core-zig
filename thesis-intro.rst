Motivation
==========

There are many application programming languages like ECMAScript (i. e. JavaScript), Python, Java, C# or Go in popular use today.
These languages feature many productivity features like garbage collection or dynamic loading that are provided by a runtime layer and cannot meaningfully be implemented in their entirety in the language in question.
To avoid having to program in highly machine-dependent low-level languages like assembly, another category of high-level programming is needed: these are usually called systems programming languages.

The domain of systems programming languages has long been dominated by C and C++.
This pair of closely related languages distinguish themselves by their strong standardization despite being implemented by multiple vendors, with compatibility reaching back decades.
New languages in the field must therefore show that they too are building bricks to be counted on, with clear enough syntax to build another implementation when one disappears or to implement the language on a new platform.
This thesis represents an effort toward providing such a rigorous semantics for the upcoming language called Zig.

Zig's selling points over C and C++ are:

* single level of syntax:
  No textual preprocessing is needed to parse a program.
  This simplifies the construction of productivity tools and eases mental burden of programmers.
  This is replaced by an import system and compile-time evaluation features providing conditional compilation and reducing 'boilerplate' code.
* metaprogramming without a separate type-level semantics
