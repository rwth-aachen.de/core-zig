// SPDX-License-Identifier: CC0-1.0
//! Feature Level 3: integer type annotations and casts,
//! 'type' type, @TypeOf, shift operations

// lazy error: pattern expressions are coerced (not just
// compared) to the input type (would lead to overflow here)
export const pattern_coercion = switch (0) {0 => 0,
    else => switch (@as(u16, 0)) {
        0x10000 => undefined_identifier,
        else => 0,
    }
};

// allowed: shifting out 1- (unsigned) / non-sign- (signed) bits
export const shl_overflow_unsigned = @as(u16, 0x10ff) << 8;
export const shl_overflow_signed = @as(i16, -0x10ff) << 8;

// lazy error: shift RHS is coerced to the smallest unsigned type
// that can hold width-of-LHS - 1 (here u4)
export const shamt_overflow = switch (0) {0 => 0,
    else => @as(u16, 0) << 16
};
// lazy error: value of shift RHS must be smaller than width of
// LHS.
export const overshift = switch (0) {0 => 0,
    else => @as(u15, 0) << 15
};

// allowed: blocks that do not break return the singleton void
// value (and that is to my knowledge the most concise way to
// construct this value, so it is chosen as the canonical
// representation of the void value).
export const void_literal: void = {};
// allowed: void is the only type to which an expression statement
// may evaluate to.
export const void_value_ignored = {@as(void, {});};

// 'type' values cannot be exported
// export const MyType = u32;
const MyType: type = u32;

export const odd_length: u3 = 5;
export const two: MyType = odd_length *% 2;

// member-level error: @TypeOf without arguments
const empty_typeof = switch (0) {0 => 0, else => @TypeOf()};

export const unsigned_to_signed: i16 = @as(u15, 0x7fff);
// allowed: even though coercing signed to unsigned integers
// is not generally possible, for comptime-known values it is
// allowed as long as the values are in range
export const signed_to_unsigned: u16 = @as(i16, 0x7fff);

export const max_i16: i16 = 0x7fff;
// this should be allowed, but it seems the compiler does not
// implement saturating operators yet
//export const saturating = switch (max_i16 +| 1) {
//    0x7fff => 0,
//    else => undefined_identifier
//};
export const twos_complement: @TypeOf(max_i16) = switch (max_i16 +% 1) {
    -0x8000 => -0x8000,
    else => undefined_identifier
};
export const wrapping_negation = switch (-%twos_complement) {
    -0x8000 => 0,
    else => undefined_identifier
};

// lazy error: computation is done in u16 type, even if the whole
// expression is coerced to u32, leading to overflow
export const conversion_after_computation = switch (0) {0 => 0,
    else => @as(u32, @intCast(u16, 0x8000) * 2)
};

export const leading_zero_type: u001 = 1;

// lazy error: the conversion of comptime_int into the result type
// is done before computing the result
export const input_overflow = switch (0) {0 => 0,
    else => 0x10000 - @as(u16, 0xffff)
};

// lazy error: the non-wrapping, non-saturating arithmetic
// operations have undefined behavior on overflow,
// which must be caught during comptime
export const output_overflow = switch (0) {0 => 0,
    else => @as(u16, 0xffff) + 1
};

// allowed: peer type resolution assigns i16 for this, even though
// not all u16 values can be represented in it, but coercing
// comptime values is checked based on the value, not the type
// (see above for signed_to_unsigned)
export const peer_type_resolution = @as(i16, 1) + @as(u16, 1);

// allowed: switching on types. This particular example also
// confirms that indeed the above example has type i16
export const switch_on_types = switch (@TypeOf(@as(i16, 1) + @as(u16, 1))) {i16=> 0, else => undefined_identifier};

// lazy error: range patterns are not allowed for types
export const type_range = switch (0) {0 => 0,
    else => switch (i16) {i15 ... i17 => 0, else => 0}
};

// as long as any signed integer is involved, the widest signed
// integer is the result
export const ptr2 = switch (@TypeOf(@as(i8, 1) + @as(u32, 1))) {i8 => 0, else => undefined_identifier};

// lazy error: @TypeOf evaluates all comptime-known parts of the
// expression (which in this subset is all of it)
export const typeof_overflow = switch (0) {0 => 0,
    else => @TypeOf(@as(u16, 0xffff) + 1)
};

// allowed: if the input is comptime-known, other cases are not
// evaluated, so their type does not matter
export const comptime_differently_typed_cases = switch (0) {
    0 => @as(u1, 1), else => @as(i1, -1)
};

// allowed: since, as above, the inner switch is evaluated to
// @as(u1, 1) before @TypeOf is resolved, there is no type conflict
export const typeof_comptime_switch = switch (@TypeOf(switch (0) {
    0 => @as(u1, 1), else => @as(i1, -1)
})) {
    u1 => 0, else => undefined_identifier
};
