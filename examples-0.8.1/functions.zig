// SPDX-License-Identifier: CC0-1.0

// if the 'comptime' on the argument is removed,
// undefined_identifier would need to be resolved if the function
// is referenced (even if it is then only evaluated at comptime)
fn comptime_called(comptime n: u32) u32 {
    if (n == 0) {return undefined_identifier;}
    return 0;
}
export const abc = comptime_called(1);

// QUESTION: why is this allowed …
export fn shadowing_static() u32 {
    const t = struct {const def = 0; const ghi = 0;};
    return t.ghi;
}
// QUESTION: … but not this? Shouldn't shadowing be checked
// as soon as the struct type is created, not when the member is
// accessed?
fn shadowing_static2() u32 {
    const t = struct {const def = 0; const ghi = def;};
    return t.ghi;
}

// allowed: coercion of comptime parameters is done by
// comptime rules
fn comptime_coercion(comptime param: i32) u32 {return param;}
export const blah = comptime_coercion(0x7fffffff);
