// SPDX-License-Identifier: CC0-1.0

// lazy error: .{…} is considered a type level primary expression
const anon = . {} {};

// lazy error: shadowing members in non-referenced containers
const shadowing_type = struct {
    const anon = 0;
};
// allowed: instantiating the type does not make the shadowing
// error surface
export const xyz: shadowing_type = block: {break :block .{};};
// lazy error: … but trying to get the shadowing member does
const blah = shadowing_type.anon;

// lazy error: export symbols are checked for collision only when
// a type is actually evaluated; therefore y and z cannot both
// be exported, since then two exported variables called x are
// created
fn exported(n: u32) type {
    return struct {
        export const x = n;
    };
}
export const y = exported(0).x;
const z = exported(1).x;

// lazy error: type containing itself
const self_containing = struct {x: self_containing};
const a = self_containing {.x = undefined};

const self_referential = struct {
    // allowed: type containing pointers/slices to itself.
    // This is not considered a circular dependency because the
    // type of fields is evaluated lazily, i. e. after creating the type
    // variable
    x: ?*self_referential,
    // lazy error: type of member fields can be queried, even if
    // the members are instances of this very type (excluding
    // field-level cyclic dependencies)
    //z: @TypeOf(e.y),
    z: u32,
    y: f,
    // allowed: type containing members that are objects of itself
    export const e = self_referential {.x = null, .y = 0, .z = 0};
    const f = u32;
};
export const works = self_referential {.x = null, .y = 0, .z = 0};
// lazy error: value referencing its own address
const c: self_referential = self_referential {.x = &c, .y = 0, .z = 0};
export const mutable_self_ref = block: {
    var x = self_referential {.x = null, .y = 0, .z = 0};
    x = .{.x = &x, .y = 1, .z = 1};
    break :block 0;
};

const self_ref_local = block: {
    const mytype = struct {
        // lazy error: locals only get their name defined *after* the
        // defining statement, so name resolution fails here
        x: ?*mytype
    };
    break :block mytype;
};
const d = self_ref_local {.x = null};

// allowed: fields are never in scope, so they can use the same
// identifier as a variable or member in scope
const shadowing_field = struct {def: u32};
export const ghi = shadowing_field {.def = 0};

export const type_from_annotation: shadowing_field = .{.def = 0};
export const type_from_as = @as(shadowing_field, .{.def = 0});

// QUESTION: shouldn't container definitions have their own label
// namespace (since any member contained in it may be
// evaluated long after the containing blocks have exited), so this
// would be OK, and the ICE below would become a "use of
// undefined label" error?
const label_shadowing_across_containers = block: {
    const mytype = struct {
        const member = block: {break :block 0;};
    };
    break :block mytype.member;
};

// internal compiler error, nice!
//export const break_across_containers = block: {
//    const mytype = struct {
//        const member = break :block 0;
//    };
//    break :block mytype.member;
//};

// lazy error: equality does not coerce either side to the type of the other
const U32Wrapper = struct {x: u32};
export const cmp_named_anon = if (false) U32Wrapper {.x = 0} == .{.x = 0} else 0;
export const cmp_anon_named = if (false) .{.x = 0} == U32Wrapper {.x = 0} else 0;

// lazy error: switch value expressions are not coerced to the
// type of the preceding value expression
fn switch_value_coerce(i: u32) void {
    var blah = switch (i) {
        0 => U32Wrapper {.x = 0},
        else => .{.x = 0}
    };
}
