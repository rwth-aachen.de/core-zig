// SPDX-License-Identifier: CC0-1.0
//! Feature Level 4: boolean/relational operators,
//! if statement/expression

// lazy error: integers do not coerce to booleans as they do in C
export const int_coerce_bool = if (false) @as(bool, 0) else 0;

// allowed: comparisons between integers of different signedness.
// There is no Peer Type Resolution going on here, as neither
// operand can be coerced into the other's type, and in fact their
// two's complement representation is the same.
export const diff_type_cmp = if (@as(i8, -0x80) < @as(u8, 0x80)) 0 else undefined_identifier;

// allowed: this also works for equality test
export const diff_type_eq = if (@as(i8, -0x80) == @as(u8, 0x80)) undefined_identifier else 0;

// allowed: much like "break", if expressions gobble up all
// operators following them until the next closing parenthesis/semicolon
export const break_precedence = switch (block: {
    4 * break :block 1 + 2;
}) {3 => 0, else => undefined_identifier};

// allowed: "and" and "or" do not evaluate their second operand
// if the first is "false" or "true" (respectively)
export const and_short_circuit = false and undefined_identifier;
export const or_short_circuit = true or undefined_identifier;

// lazy error: errors from the first operand abort compilation even
// if the second operand could short-circuit
export const and_no_rev_short_circuit = if (false) undefined_identifier and false else 0;
export const or_no_rev_short_circuit = if (false) undefined_identifier or true else 0;

// allowed: variable-defining statement in unbraced else branch
export const else_local = {if (false) 0 else const blah = 0;};
