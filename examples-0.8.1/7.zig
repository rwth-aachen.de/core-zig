// SPDX-License-Identifier: CC0-1.0
//! Feature Level 7: Functions, without comptime parameters,
//! unreachable

// lazy error: even if the function doesn't use its argument,
// it still needs to be evaluated when evaluating a function call at
// comptime. This is independent of whether the parameter is
// marked 'comptime' itself
fn doesnt_use_parameter(n: u32) u32 {return 0;}
const def = doesnt_use_parameter(undefined_identifier);

// lazy error: shadowing is detected on analysis
fn shadowing_local() void {
    const def = 0;
}

// lazy error: analysis rejects invalid coercions of runtime values
fn invalid_coercion(param: i32) u32 {return param;}

export fn signed_unsigned_cmp(a: i8, b: u32) bool {return a < b;}
export const bleh = if (signed_unsigned_cmp(-0x80, 0x80)) 0 else undefined_identifier;

// lazy error: PTR assigns i8 as type for a + b, but the coercions
// are checked on the basis of their operand types, so this is
// rejected because u8 cannot generally be coerced to i8
export fn invalid_peer_type_res(a: i8, b: u8) i8 {return if (false) a + b else 0;}

// allowed: the coercion on the if expression is applied before PTR
// on the branches (which would fail because u8 cannot generally
// be coerced to i8). Same for switch, while and blocks.
export fn if_coerce(a: i8, b: u8, c: bool) i16 {
    return @as(i16, if (c) a else b);
}
export fn switch_coerce(a: i8, b: u8, c: bool) i16 {
    return @as(i16, switch (c) {true => a, else => b});
}
export fn while_coerce(a: i8, b: u8, c: bool) i16 {
    return @as(i16, while (c) break a else b);
}
export fn block_coerce(a: i8, b: u8, c: bool) i16 {
    return @as(i16, block: {if (c) break :block a else break :block b;});
}
