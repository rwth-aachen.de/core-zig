// SPDX-License-Identifier: CC0-1.0
//! Feature level 2: only const definitions at top-level,
//! expressions are either identifiers, integer literals,
//! integer operators or blocks. Blocks are only allowed to contain
//! const definitions and expression statements and must contain
//! at least one expression statement. Still no type
//! annotations, so everything is still comptime_int.

// member-level error: break label not defined by an enclosing
// block
const unmatched_break = break :block 0;

// member-level error: block label that is not used by any
// contained break expressions.
const unused_block_label = label: {0;};

// member-level error: labels shadowing each other.
const label_shadowing = block: {block: {break :block 0;}};

// member-level errors: shadowing {predefined identifiers,
// in-scope members, preceding variables in the same block,
// preceding variables in other blocks} in local variables.
const shadowing_predef = block: {
    break :block 0;
    const u64 = 0;
};
const local_member_shadowing = block: {
    break :block 0;
    const local_member_shadowing = 0;
};
const local_redeclaration = block: {
    break :block 0;
    const my_var = 0;
    const my_var = 0;
};
const local_local_shadowing = block: {
    const my_var = 0;
    {break :block 0;const my_var = 0;}
};

// allowed: the outer my_var is defined after the block ends,
// so the scope of my_var in the inner block has already ended
export const local_local_false_shadowing = block: {
    {break :block 0;const my_var = 0;}
    const my_var = 0;
};

// allowed: a break expression gobbles up all operators following
// it until the next closing parenthesis/semicolon
export const break_precedence = switch (block: {
    4 * break :block 1 + 2;
}) {3 => 0, else => undefined_identifier};

// lazy error: ignored non-void value
export const ignored_result = switch (0) {0 => 0,
    else => {0;}
};

export const answer_to_the_universe_and_everything = block: {
    const shared_subexpression = 3 * 2;
    break :block shared_subexpression * (shared_subexpression + 1);
};

// allowed: during lazy evaluation, breaks in a switch label
// expression prevent evaluation of other labels, but not the input
export const label_break = block: {
    switch (0) {
        break :block 0 => undefined_identifier,
        break :block undefined_identifier => undefined_identifier,
        // if uncommented, this would lead to an error:
        // range patterns are evaluated before value patterns
        //break :block undefined_identifier ... break :block undefined_identifier => undefined_identifier,
        undefined_identifier => undefined_identifier
    }
};

// allowed: block labels do not conflict with variable or predefined
// identifiers, since they are set apart by syntax everywhere they
// appear
export const unusual_label = u16: {break :u16 0;};

export const nested_breaks = switch (b1: {
    break :b1 2 * b2: {break :b2 break :b1 1;};
}) {1 => 0, else => undefined_identifier};

// QUESTION: why are these hard errors, and not member-level
// ones? One would think that labels would get unescaped at the
// same stage as identifier expressions are …
//const illformed_block_label = @"\u{110000}": {0;};
//const illformed_break_label = break :@"\u{110000}" 0;

// member-level error: local variable definition without initializer
// expression.
// Note that the syntactic form of this is reused in the SOS rules
// as a defer statement for the destruction of the corresponding
// variable.
const vars_must_be_initialized = {const blah; 0;};
