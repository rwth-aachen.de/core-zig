// SPDX-License-Identifier: CC0-1.0
//! Feature Level 5: mutable (local) variables, assignment.
//! Admittedly not very useful without loops.

// allowed: simple base example
export const simple = block: {
    var x = 0;
    x = 1;
    switch (x) {
        1 => break :block 0,
        else => undefined_identifier
    }
};

// lazy error: mutating var members.
var blah: u32 = 0;
export const mutating_var_members = if (false) {blah = 1;} else 0;

// lazy error: mutating const variables (local or member).
export const mutating_consts = if (false) {simple = 1;} else 0;

// lazy error: the value of a var member is not comptime-known
export const var_member_not_comptime = if (false) blah else 0;

// member-level error: left-hand side of expression is not an
// LValue expression
const lvalue_check = block: {break :block 0 = 0;};

// allowed: assigning to _ discards the value
export const discard = {_ = 0; _ = true;};
