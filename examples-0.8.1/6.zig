// SPDX-License-Identifier: CC0-1.0
//! Feature Level 6: while loops, defer.
//! This is the first Turing-complete feature level.

// member-level error: continue on block
const continue_on_block = block: {continue :block;};

// member-level error: continue on undefined label
const unmatched_continue = continue :loop;

// continue clause and condition expression of while loops are
// considered outside of the loop for the purposes of
// continue/break binding

// member-level errors from break/continue considered outside
// any loop
const continue_in_continue_clause = block: {
    var count = 0;
    break :block while (count < 2) : ({count += 1; continue;}) {
    } else 0;
};
const continue_in_condition = block: {
    var count = 0;
    break :block while (condition: {
        count += 1;
        if (count % 2 == 1) continue;
        break :condition count < 10;
    }) {};
};
const break_in_continue_clause = while (true) : (break 0) {};
const break_in_condition = while (break 0) {};

// allowed: break/continue affecting outer loops
export const break_outer_loop_from_condition = switch (while (true) {
    while (break 0) {}
    break 1;
}) {0 => 0, else => undefined_identifier};
export const continue_outer_loop_from_condition = switch (block: {
    var count = 0;
    break :block while (count < 0) : (count += 1) {
        while (continue) {}
        undefined_identifier;
    } else 0;
}) {0 => 0, else => undefined_identifier};
export const break_outer_loop_from_continue_clause = switch (while (true) {
    while (true) : (break 0) {}
    break 1;
}) {0 => 0, else => undefined_identifier};
export const continue_outer_loop_from_continue_clause = switch (block: {
    var count = 0;
    break :block while (count < 0) : (count += 1) {
        while (true) : (continue) {}
        undefined_identifier;
    } else 0;
}) {0 => 0, else => undefined_identifier};

// member-level errors: break/continue expressions in defer statements (whether or not they target the immediately containing block).
const break_in_defer = block: {{defer break :block 0; break :block 1;}};
const continue_in_defer = block: {
    var count = 0;
    break: block while (count == 0) : (count += 1) {
        defer continue;
    };
};

