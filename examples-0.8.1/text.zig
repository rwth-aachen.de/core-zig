

// the (static) break statements do not need to agree on a result
// type, since all branches are 
export const conflicting_types = block: {
    switch (0) {
        0 => break :block 1,
        else => break :block 'a'
    }
};