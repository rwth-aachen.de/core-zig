// SPDX-License-Identifier: CC0-1.0
//! Feature level 1: only const definitions at top-level,
//! only expressions allowed are identifiers, integer literals
//! and integer operators.

// definitions can be in any order
export const minus_two = 3 - five;

export const five = 0o5;
export const thirty = five * 0b110;
export const two = 0x21 % thirty - 0x1;

// lazy error: % only takes positive operands
// (they also must be comptime-known, but this is guaranteed by
// our language subsetting)
// since they are not exported or used by anything that is,
// no need to comment this out
export const one = switch (0) {0 => 0,
    else => five % minus_two
};

export const really_one = @mod(five, two);
export const also_one = @mod(-five, two);
export const another_one = @rem(five, two);
export const minus_one = @rem(-five, two);
// lazy error: even @mod and @rem do not define
// results for negative denominators
export const maybe_minus_one = switch (0) {0 => 0,
    else => @rem(five, minus_two)
};
export const plus_minus_one_IDK = switch (0) {0 => 0,
    else => @mod(five, minus_two)
};

// member-level error: use of undefined builtin construct
const undefined_builtin = @myUndefinedBuiltin();

// lazy name resolution: as long as the parse tree can be
// constructed and they are not referenced, circular dependencies
// and undefined names may be used
const undefined_name = 5 * foo + bar;
const foo = undefined_name + 3;

// member-level error: string and character literals with escapes
// that cannot be encoded in at most 4 bytes of UTF-8
const char_out_of_range = '\u{fffffffff}';
const string_out_of_range = "\u{fffffffff}";
// allowed: byte escapes that cannot be decoded as UTF-8
export const @"\xff" = 0;
// allowed: unicode escapes that map to surrogates
export const @"\u{d800}" = 0;
// allowed: identifiers with tab in them
export const @"	" = 0;
// member-level error: not-unescapable identifiers.
const unicode_out_of_range = @"\u{110000}";
const underscore_in_escape = @"\u{1_0}";
// this error surfaces when it is used in a definition and the
// container (in this case the module) is evaluated
// const @"\u{11_0000}" = 0;

// lazy error: all labels are evaluated to dispatch a switch
export const undefined_prong_label = switch (0) {0 => 0,
    else => switch (0) {0 => 0, undefined_identifier => 0}
};

export const answer_to_the_universe_and_everything = switch (two) {
    // prongs excluded at comptime do not have names resolved
    3 => undefined_identifier,
    // yes, you can have the same case twice, even among
    // different prongs (in that case it takes the first matching one)
    two, 2, 3 => 42,
    // but an else prong never has explicit values associated with it
    else => 0,
};

// member-level error: multiple else prongs
const blah = switch (0) {
    else => undefined_identifier,
    else => undefined_identifier2,
};

// we do not always take the first matching arm:
// explicit arms are preferred to an else prong, if any
export const else_matching = switch (0) {
    else => undefined_identifier,
    0 => 0
};

// range matches are preferred over single-item matches
export const range_matching = switch(0) {
    0 => undefined_identifier,
    -1 ... 1 => 0,
    else => undefined_identifier
};

// this is 96 = 0x60, not 0x66 as you might expect from C
// or other languages
export const bitwise_associativity = 0x7 & 0xe | 0x70 & 0xe0;

// error: shadowing predefined identifiers
//const u64 = 0;
