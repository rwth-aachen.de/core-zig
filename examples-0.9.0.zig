// SPDX-License-Identifier: CC0-1.0
// === Feature Level 1 ===
// only basic integer operations, no primitive names defined
// no blocks, the only control flow expression is switch

// definitions can be in any order
export const minus_two = 3 - five;
export const five = 0o5;
export const thirty = five * 0b110;
export const two = 0x21 % thirty - 0x1;

// lazy error: % only takes positive operands
// (they also must be comptime-known, but this is guaranteed by
// our language subsetting)
// since they are not exported or used by anything that is,
// no need to comment this out
export const one = switch (0) {0 => 0,
    else => five % minus_two
};

export const really_one = @mod(five, two);
export const also_one = @mod(-five, two);
export const another_one = @rem(five, two);
export const minus_one = @rem(-five, two);
// lazy error: even @mod and @rem do not define
// results for negative denominators
export const maybe_minus_one = switch (0) {0 => 0,
    else => @rem(five, minus_two)
};
export const plus_minus_one_IDK = switch (0) {0 => 0,
    else => @mod(five, minus_two)
};

export const bitwise_associativity = switch (0x7 & 0xe | 0x70 & 0xe0) {0x60 => 0, else => 0/0};

export const undefined_prong_label = switch (0) {0 => 0,
    else => switch (0) {0 => 0, 0/0 => 0}
};
const patternorder = switch (block: {switch (0) {break :block 1, break :block 0 ... 0 => 0/0}}) {0 => 0, else => 0/0};
const matchorder = switch (switch (1) {1 => 1, 0  ... 2 => 0, else => 0/0}) {0 => 0, else => 0/0};

// === Feature Level 2 ===
// blocks (must contain at least 1 expression statement, may also
// contain const definitions)

// lazy error: ignored non-void value
export const ignored_result = switch (0) {0 => 0,
    else => {0;}
};

// allowed: the outer my_var is defined after the block ends,
// so the scope of my_var in the inner block has already ended
export const local_local_false_shadowing = block: {
    {
        // this is wrapped in an unconditional switch to throw off
        // the fatal, but useless (see below) dead code check. Fuck
        // that crap.
        switch(0) {0=>break :block 0, else => 0}
        const my_var = 0;
        // if the compiler were to actually look at this statement it
        // would determine that it's disallowed because it has type
        // comptime_int. Since it can dispatch the above switch at
        // comptime, it can work out just fine that anything after it is
        // unreachable, so it gets discarded without typing.
        // Since making such discarding a fatal error would make
        // writing generic code basically impossible, the error does
        // not use this more precise definition of dead code, leaving
        // it a useless husk of a check
        my_var;
    }
    const my_var = 0; my_var;
};

// allowed: block labels do not conflict with variable or predefined
// identifiers, since they are set apart by syntax everywhere they
// appear
export const unusual_label = u16: {break :u16 0;};

export const answer_to_the_universe_and_everything = block: {
    const shared_subexpression = 3 * 2;
    break :block shared_subexpression * (shared_subexpression + 1);
};

// allowed: during lazy evaluation, breaks in a switch label
// expression prevent evaluation of other labels, but not the input
export const label_break = block: {
    switch (0) {
        break :block 0 => 0/0,
        break :block 0/0 => 0/0,
        // if uncommented, this would lead to an error:
        // range patterns are evaluated before value patterns
        //break :block 0/0 ... break :block 0/0 => 0/0,
        0/0 => 0/0
    }
};

// === Feature Level 3 ===
// integer types, `type` type, void type, @TypeOf
const @"u64": u64 = 0;
const @"u16" = u32;
export const redefining_primitives = switch (@"u16") {u32 => switch (u16) {@"u16" => 0/0, else => 0}, else => 0/0};

// integer ops do not propagate coercion to their operand(s)
export const unary_coercion_propagation = @as(u8, ~@as(u16, 0xff00));

const non_exhaustive_switch = switch (@as(u1, 0)) {0 => 0};

// lazy error: pattern expressions are coerced (not just
// compared) to the input type (would lead to overflow here)
export const pattern_coercion = switch (0) {0 => 0,
    else => switch (@as(u16, 0)) {
        0x10000 => 0/0,
        else => 0,
    }
};

// allowed: shifting out 1- (unsigned) / non-sign- (signed) bits
export const shl_overflow_unsigned = @as(u16, 0x10ff) << 8;
export const shl_overflow_signed = @as(i16, -0x10ff) << 8;

// lazy error: shift RHS is coerced to the smallest unsigned type
// that can hold width-of-LHS - 1 (here u4)
export const shamt_overflow = switch (0) {0 => 0,
    else => @as(u16, 0) << 16
};
// lazy error: value of shift RHS must be smaller than width of
// LHS.
export const overshift = switch (0) {0 => 0,
    else => @as(u15, 0) << 15
};

export const shift_sat = switch (@as(u8, 1) <<| 8) {255 => 0, else => 0/0};
export const shift_sat2 = switch (@as(u8, 2) <<| 7) {255 => 0, else => 0/0};
export const shift_sat_neg = switch (@as(i8, 1) <<| 7) {127 => 0, else => 0/0};
export const shift_sat_neg2 = switch (@as(i8, 2) <<| 6) {127 => 0, else => 0/0};

// allowed: blocks that do not break return the singleton void
// value (and that is to my knowledge the most concise way to
// construct this value, so it is chosen as the canonical
// representation of the void value).
export const void_literal: void = {};
// allowed: void is the only type to which an expression statement
// may evaluate to.
export const void_value_ignored = {@as(void, {});};

// 'type' values cannot be exported
// export const MyType = u32;
const MyType: type = u32;

export const odd_length: u3 = 5;
export const mytype_two: MyType = odd_length *% 2;

export const unsigned_to_signed: i16 = @as(u15, 0x7fff);
// allowed: even though coercing signed to unsigned integers
// is not generally possible, for comptime-known values it is
// allowed as long as the values are in range
export const signed_to_unsigned: u16 = @as(i16, 0x7fff);

export const max_i16: i16 = 0x7fff;
export const saturating = switch (max_i16 +| 1) {
    0x7fff => 0,
    else => 0/0
};
export const twos_complement: @TypeOf(max_i16) = switch (max_i16 +% 1) {
    -0x8000 => -0x8000,
    else => 0/0
};
export const wrapping_negation = switch (-%twos_complement) {
    -0x8000 => 0,
    else => 0/0
};

// lazy error: computation is done in u16 type, even if the whole
// expression is coerced to u32, leading to overflow
export const conversion_after_computation = switch (0) {0 => 0,
    else => @as(u32, @intCast(u16, 0x8000) * 2)
};

export const leading_zero_type: u001 = 1;

// lazy error: the conversion of comptime_int into the result type
// is done before computing the result
export const input_overflow = switch (0) {0 => 0,
    else => 0x10000 - @as(u16, 0xffff)
};

// lazy error: the non-wrapping, non-saturating arithmetic
// operations have undefined behavior on overflow,
// which must be caught during comptime
export const output_overflow = switch (0) {0 => 0,
    else => @as(u16, 0xffff) + 1
};

// allowed: peer type resolution assigns i16 for this, even though
// not all u16 values can be represented in it, but coercing
// comptime values is checked based on the value, not the type
// (see above for signed_to_unsigned)
export const peer_type_resolution = @as(i16, 1) + @as(u16, 1);

// allowed: switching on types. This particular example also
// confirms that indeed the above example has type i16
export const switch_on_types = switch (@TypeOf(@as(i16, 1) + @as(u16, 1))) {i16=> 0, else => 0/0};

// lazy error: range patterns are not allowed for types
export const type_range = switch (0) {0 => 0,
    else => switch (i16) {i15 ... i17 => 0, else => 0}
};

// as long as any signed integer is involved, the widest signed
// integer is the result
export const ptr2 = switch (@TypeOf(@as(i8, 1) + @as(u32, 1))) {i8 => 0, else => 0/0};

// lazy error: @TypeOf evaluates all comptime-known parts of the
// expression (which in this subset is all of it)
export const typeof_overflow = switch (0) {0 => 0,
    else => @TypeOf(@as(u16, 0xffff) + 1)
};

// allowed: if the input is comptime-known, other cases are not
// evaluated, so their type does not matter
export const comptime_differently_typed_cases = switch (0) {
    0 => @as(u1, 1), else => @as(i1, -1)
};

// allowed: since, as above, the inner switch is evaluated to
// @as(u1, 1) before @TypeOf is resolved, there is no type conflict
export const typeof_comptime_switch = switch (@TypeOf(switch (0) {
    0 => @as(u1, 1), else => @as(i1, -1)
})) {
    u1 => 0, else => 0/0
};

// === Feature Level 4 ===
// booleans, `if`, comparisons

// lazy error: integers do not coerce to booleans as they do in C
export const int_coerce_bool = if (false) @as(bool, 0) else 0;

// allowed: comparisons between integers of different signedness.
// There is no Peer Type Resolution going on here, as neither
// operand can be coerced into the other's type, and in fact their
// two's complement representation is the same.
export const diff_type_cmp = if (@as(i8, -0x80) < @as(u8, 0x80)) 0 else 0/0;

// allowed: this also works for equality test
export const diff_type_eq = if (@as(i8, -0x80) == @as(u8, 0x80)) 0/0 else 0;

// allowed: "and" and "or" do not evaluate their second operand
// if the first is "false" or "true" (respectively)
export const and_short_circuit = false and 0/0;
export const or_short_circuit = true or 0/0;

// lazy error: errors from the first operand abort compilation even
// if the second operand could short-circuit
export const and_no_rev_short_circuit = if (false) 0/0 and false else 0;
export const or_no_rev_short_circuit = if (false) 0/0 or true else 0;

// lazy error: only integers can be compared across type boundaries
const eq_different_types = 0 == true;

// ICE: variable-defining statement in unbraced else branch
//export const else_local = {if (false) 0 else const blah = 0;};

// === Feature Level 5 ===
// mutable (local) variables, assignment. Admittedly not very
// useful without loops.

// allowed: simple base example
export const simple = block: {
    var x = 0;
    x = 1;
    switch (x) {
        1 => break :block 0,
        else => 0/0
    }
};

// QUESTION: why is this not allowed?
//export const underscore: u32 = block: {
//   var @"_" =  42;
//   @"_" = 43;
//   break :block @"_";
//};

// lazy error: mutating var members at comptime.
var var_member: u32 = 0;
export const mutating_var_members = if (false) {var_member = 1;} else 0;

// lazy error: mutating const variables (local or member).
export const mutating_consts = if (false) {simple = 1;} else 0;

// lazy error: the value of a var member is not comptime-known
export const var_member_not_comptime = if (false) var_member else 0;

// allowed: assigning to _ discards the value
export const discard = {_ = 0; _ = true;};

// === Feature Level 6 ===
// while loops

// allowed: break/continue affecting outer loops
export const break_outer_loop_from_condition = switch (while (true) {
    while (break 0) {}
    break 1;
}) {0 => 0, else => 0/0};
export const continue_outer_loop_from_condition = switch (block: {
    var count = 0;
    break :block while (count < 0) : (count += 1) {
        while (continue) {}
        0/0;
    } else 0;
}) {0 => 0, else => 0/0};
export const break_outer_loop_from_continue_clause = switch (while (true) {
    while (true) : (break 0) {}
    break 1;
}) {0 => 0, else => 0/0};
export const continue_outer_loop_from_continue_clause = switch (block: {
    var count = 0;
    break :block while (count < 0) : (count += 1) {
        while (true) : (continue) {}
        0/0;
    } else 0;
}) {0 => 0, else => 0/0};

// https://github.com/ziglang/zig/issues/10591
export const defer_assign = switch (block: {
    var x = 0; defer x = 1; {break :block x;}
}) {
    1 => 0, else => 0/0
};

// === Feature Level 7 ===
// functions without comptime parameters

// lazy error: analysis rejects invalid coercions of runtime values
fn invalid_coercion(param: i32) u32 {return param;}

export fn signed_unsigned_cmp(a: i8, b: u32) bool {return a < b;}
export const bleh = if (signed_unsigned_cmp(-0x80, 0x80)) 0 else 0/0;

// lazy error: PTR assigns i8 as type for a + b, but the coercions
// are checked on the basis of their operand types, so this is
// rejected because u8 cannot generally be coerced to i8
export fn invalid_peer_type_res(a: i8, b: u8) i8 {return if (false) a + b else 0;}

// allowed: the coercion on the if expression is applied before PTR
// on the branches (which would fail because u8 cannot generally
// be coerced to i8). Same for switch, while and blocks.
export fn if_coerce(a: i8, b: u8, c: bool) i16 {
    return @as(i16, if (c) a else b);
}
export fn switch_coerce(a: i8, b: u8, c: bool) i16 {
    return @as(i16, switch (c) {true => a, else => b});
}
export fn while_coerce(a: i8, b: u8, c: bool) i16 {
    return @as(i16, while (c) break a else b);
}
export fn block_coerce(a: i8, b: u8, c: bool) i16 {
    return @as(i16, block: {if (c) break :block a else break :block b;});
}

export fn runtime_parts_in_inline_loop(a: u32) u32 {
    comptime var iter = 0;
    var x = a;
    return
    // allowed: this loop terminates because expansion stops if
    // the iteration produces a `noreturn` body.
    // The condition is comptime-evaluated though, so something
    // like `x < 32` would not have been allowed
    inline while (true)
    // allowed: the continue expression is not
    // comptime-evaluated
        : (x *= 2)
    {
        if (iter == 3)
            // allowed: neither the else clause nor the value of any
            // breaks targeting an inline loop are
            // comptime-evaluated. This makes sense since the
            // decision whether to use a particular break or fall back
            // to the else clause may depend on runtime branches
            // anyway, and if a result at comptime is desired, the
            // entire loop could have been wrapped in a comptime
            // expression, which has a different effect than marking
            // that loop inline.
            break inline while (false) {} else x;
        iter += 1;
    };
}
export const try_runtime_parts = if (runtime_parts_in_inline_loop(3) == 24) 0 else 0/0;

// allowed: performing a function return or break to a block
// containing runtime code, from comptime code. The effect is
// that the expression/statement at the runtime-comptime
// boundary is turned into the corresponding runtime exit
// expression.
// This means that any preceding runtime code is not discarded,
// in these cases the commented-out returns would actually
// determine the return value if uncommented.
export fn return_from_comptime() u32 {
    //if (true) return 1;
    comptime return 0;
}
export const test_return_from_comptime = if (0 == return_from_comptime()) 0 else 0/0;
export fn return_from_pattern() u32 {
    //if (true) return 1;
    switch (0) {
        return 0 => 0/0
    }
}
export const test_return_from_pattern = if (0 == return_from_pattern()) 0 else 0/0;
export fn return_from_annotation() u32 {
    //if (true) return 1;
    var x: (return 0) = 0;
    x;
}
export const test_return_from_annotation = if (0 == return_from_annotation()) 0 else 0/0;

export fn break_from_comptime() u32 {
    return block: {
        //if (true) return 1;
        comptime break :block 0;
    };
}
export const test_break_from_comptime = if (0 == break_from_comptime()) 0 else 0/0;
export fn break_from_pattern() u32 {
    return block: {
        //if (true) return 1;
        switch (0) {
            break :block 0 => 0/0
        }
    };
}
export const test_break_from_pattern = if (0 == break_from_pattern()) 0 else 0/0;
export fn break_from_annotation(a: bool) u32 {
    return block: {
        if (a) return 1;
        var x: (break :block 0) = 0;x;
    };
}
export const test_break_from_annotation = if (0 == break_from_annotation(false)) 0 else 0/0;

// arguably, this should make the block noreturn
export fn noreturn_defer() void {
    defer while (true) {};
}

fn runtime_int() u32 {return 0;}

export fn runtime_code_comptime_value(a: u32) void {
    if ({if (a == 0) {}} != {}) 0/0;
    if (b: {_ = runtime_int(); break :b false;}) 0/0;
    comptime var x = 0;
    (b: {_ = runtime_int(); break :b &x;}).* = 1;
    if (x != 1) 0/0;
    if (@as(b: {if (a == 0) while (true) {}; break :b u8;}, 1) << b: {_ = runtime_int(); break :b 7;} != 0x80) 0/0;
    const y: @TypeOf(b: {var b = a; break :b b;}) = 0;
    _ = y;
    // these only accept fully-comptime expressions
    //const x: if ({if (a == 0) {}} == {}) u32 = 0;
    //switch ({}) {{if (a == 0) {}} => {}}
    // arguably these should work too
    //if (b: {if (runtime_int() == 0) break :b false else break :b false;}) 0/0;
    //if (if (runtime_int() == 0) false else false) 0/0;
    //if (runtime_int() == 0 or true) {} else 0/0;
    //if (runtime_int() == 0 and false) 0/0;
    //if (runtime_int() * 0 != 0) 0/0;
    //if (runtime_int() & 0 != 0) 0/0;
}

fn invalid_param_type(p: u32, q: (0/0)) (0/0) {_= p * q; 0/0;}
const test_invalid_param_type = block: {invalid_param_type(break :block 0, 0/0);};

// === Feature Level 8 ===
// struct/union definitions
fn Struct(x: i2) type {return if (x < 0) struct {} else struct {};}
const t0 = struct {};
const t1 = struct {};
const t2 = Struct(-1);
const t3 = Struct(0);
const t4 = Struct(1);
const t5 = Struct(0);

export const struct_identity = if (t0 == t1) 0/0 else 0;
export const struct_identity2 = if (t3 != t5) 0/0 else 0;
// t3 and t4 are equal in the model, but distinct in the compiler
export const struct_identity3 = if (t3 == t4) 0/0 else 0;

const MyStruct = struct {
    a: u32,

    // Since members are evaluated lazily, we can use the name
    // of the member containing the type definition here without
    // circular dependencies.
    // Also: field names do not conflict with method names
    fn a() MyStruct {
        // anonymous compound literals take their type from the
        // coercion context if it exists.
        return .{.a = 0};
    }
};
export const mystruct_value = MyStruct.a();
export const mystruct_extract = mystruct_value.a;

const MethodVsFnPtr = struct {
    a: fn () u32,

    fn a(self: @This()) u32 {_ = self; return 0;}
};
fn return_1() u32 {return 1;}
export const fnptr_call = {
    const x = MethodVsFnPtr {.a = return_1};
    if ((x.a)() != 1) 0/0;
    //if (x.a() != 0) 0/0;
};

// not allowed: switch on struct
fn struct_switch(a: MyStruct) callconv(.Unspecified) u32 {
    switch (a) {
        .{.a = 0} => return 1,
        .{.a = 1} => return 0,
        else => return a.a
    }
}

// this works in Zig stage1, but not in the model or stage2
const InvalidDefault = struct {
  a: u32 = 0/0,
  b: u32 = 0
};
export const invalid_default = InvalidDefault {.a = 0};

export fn comptime_typeexpr(a: u32) void {
    _ = a;
    //_ = (b: {if (a == 0) while (true) {}; break :b MyStruct;}) {.a = 0};
}

const EmptyType = struct {a: noreturn};
fn make_empty_type() EmptyType {
    EmptyType {.a = if (true) while (true) {}};
    0/0;
}
export fn use_empty_type() void {
    _ = make_empty_type();
    //0/0;
}

export fn noreturn_initlist() void {
    //_ = noreturn {.a = while (true) {}};
}

const MyUnion = extern union {a: u32, b: u16};

// lazy error: cannot compare unions for equality
fn eq_union(a: MyUnion) bool {return a == MyUnion {.a = 0};}

const MyStruct2 = extern struct {a: u32};

// lazy error: cannot compare structs for equality
fn eq_struct(a: MyStruct2) bool {return a == MyStruct2 {.a = 0};}

// === Feature level 9 ===
// generic functions

fn generic(comptime p: u32, q: (0/0)) if (p == 0) u16 else u32 {_ = q;return 0;}
// this doesn't quite work because anytype is not allowed in
// return type position, but internally that's the type assigned.
// Arguably anytype as return type in function type expressions
// should be allowed (TODO discuss this)
//export const generic_fnptr = if (@TypeOf(generic) != fn(u32, anytype) anytype) 0/0;

fn comptime_param_sequencing(comptime always_type: type, comptime t: always_type, rt_param: t) t {return rt_param;}
export const try_comptime_param_sequencing = comptime_param_sequencing(type, u8, @as(u32, 0xff));

// ===
const MemberAtField = struct {const m : u32 = 0;};
export const member_atfield = @field(MemberAtField, "m");
const PointerMethod = struct {
  x: u32,
  fn m(self: *const @This()) void {_ = self;}
};
export const pointer_method = {
  const o = PointerMethod {.x = 0};
  o.m();
};
fn optional_else_payload(a: ?u32) u32 {return if (a) 0 else |p| if (p == null) 1 else 2;}
export const optional_else_nonnull = if (optional_else_payload(0) != 0) 0/0;
export const optional_else_null = if (optional_else_payload(null) != 0) 0/0;

fn bool_else_payload(a: bool) u32 {return if (a) 0 else |p| if (p == false) 1 else 2;}
export const bool_payload_else_true = if (bool_else_payload(true) != 0) 0/0;
export const bool_payload_else_false = if (bool_else_payload(false) != 0) 0/0;
fn give_zero(width: u16) u0 {
    const myzero = @as(@Type(.{.Int = .{.signedness = .unsigned, .bits = width}}), 0);
    return @intCast(u0, myzero);
}
const complicated_zero = give_zero(16);

// allowed: inline loop expansion leads to the creation of
// multiple instances of comptime variables defined in the body
fn comptime_const_identity() bool {
    comptime var i = 0;
    comptime var p: ?*const u32 = null;
    inline while(true) : (i += 1) {
        const x: u32 = 0;
        if (i == 0) {
            p = &x;
        } else {
            return (p orelse unreachable) == &(x);
        }
    }
}
export const test_comptime_const_identity = if (comptime_const_identity()) 0/0 else 0;

// according to my understanding, this function should always
// return the same value, since y is a comptime var, so its value
// should be substituted in at analysis time. Specifically because
// x is not comptime-known to be false, `y = true` should be
// executed before said substitution, so the return value should
// always be true. In practice though it behaves as if y were a
// runtime variable. Waiting on expert response.
fn comptime_assignment_in_runtime_branch(x: bool) bool {
    comptime var y = false;
    if (x) y = true;
    return y;
}
export const test1 = if (comptime_assignment_in_runtime_branch(true)) 0 else 0/0;
const test2 = if (comptime_assignment_in_runtime_branch(false)) 0 else 0/0;

export fn not_executable() noreturn {
    (if (true) unreachable) + {};
}
fn reflect_not_executable() u32 {
    return if (@TypeOf(not_executable()) == noreturn) @as(u32, 0) else @as(u16, 1);
}
const run_reflect_not_executable = if (reflect_not_executable() == 0) 0 else 0/0;
export fn infinite_loop() noreturn {while (true) {}}
fn reflect_infinite_loop() u32 {
    return if (@TypeOf(infinite_loop()) == noreturn) @as(u32, 0) else @as(i16, -1);
}
const run_reflect_infinite_loop = if (reflect_infinite_loop() == 0) 0 else 0/0;
const run_infinite_loop = if (@TypeOf(infinite_loop()) == noreturn) 0 else 0/0;
