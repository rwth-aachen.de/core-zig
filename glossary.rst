.. SPDX-License-Identifier: CC-BY-4.0

.. role:: gl(emphasis)

Glossary
--------

activation
    A program fragment (e. g. member variable initializer, function body) under evaluation, with local state (e. g. local variable addresses).
branch
    An expression that may be conditionally executed, as part of an ``if`` or :gl:`switch` expression.
case
    A construct in a :gl:`switch` expression that determines the set of values for which the :gl:`prong` applies.
    May be an ``else`` or a list of expressions and range constructs.
container
    A nominal data type (``struct``, ``union``, or, not modeled here, ``enum``) which can have variables and functions as (static) members.
    ``struct``\s and ``union``\s also define fields that make up the structure of instances of the type.
    The root level of a Zig program is a container.
member
    (Static) variables and functions declared in a :gl:`container`.
    In this document, the term is not used for (dynamic) fields of the type.
prong
    An alternative control flow path in a :gl:`switch`.
    Consists of a :gl:`case`, optionally a :gl:`capture clause` (not modeled here; used for destructuring tagged unions) and a :gl:`branch` expression, which may be executed if the case matches.
switch
    The basic conditional construct.
    Consists of an input expression and :gl:`prongs`.
update
    A binary operator (or expression thereof) taking a location and a value, which sets the location to the result of applying another binary operator to the previous value of the first operand and the second operand.
upvalue
    A local variable available when a :gl:`container` is created which can be used by expressions inside that container.
