.. SPDX-License-Identifier: CC-BY-4.0

===============================================================
A Formal Semantics for a Subset of the Zig Programming Language
===============================================================

:Author: Jona Stubbe
:Status: Draft

.. role:: m(code)
.. role:: gl(emphasis)
.. role:: zigdef(code)
   :language: zig
.. role:: zigexpr(code)
   :language: zig
.. default-role:: m

.. contents::

.. raw:: latex

    \mainmatter

Introduction
============

Since program code is generally written for the purpose of utility rather than as an artistic pursuit, the behavior of programs written in a given programming language is essential to software development.
The study of program semantics concerns itself with the expression of behaviors and methods of mapping programs to behaviors.
These mappings are used by programmers to construct programs implementing desired behavior (or verifying that a program implements the given requirements) and by language implementers to reason about whether certain transformations are correct.

Zig is an emerging imperative systems programming language.
Since many of its features are still in flux and its reference implementation has yet to reach version 1.0, there have not been any major scientific publications on this language.

This document is the result of a project attempting to formalize Zig, in order to provide a structured overview of the language and find edge cases where the guarantees of behavior need to be discussed.

Zig is a programming language with separate compilation and runtime phases.
Zig is unusual in that it has Turing-complete behavior during compilation, expressed in similar syntax to code running after compilation.

This document deals only with this "comptime" aspect of the language, and restricts the data types and syntactic constructs used.
See the `Further work`_ section for more details of what is omitted.

Since, as mentioned before, the Zig language is still in flux and under heavy development, a few areas have been identified where the reference compiler behaves inconsistently or erratically.
For these cases, this document proposes alternative semantics.
Known deviations from the behavior of the reference compiler:

- Static typing of function calls performed during runtime

  - ``type`` and ``comptime_int`` as call-time parameters, local variables and expressions
  - detection of runtime-only (currently only `var` members) or comptime-only features, separate analysis

- evaluation and matching order of ``switch`` patterns
- comptime-known value for blocks with multiple breaks (with same value)

Details of and arguments for/against these proposals are discussed in the sections dealing with these aspects.

.. include:: glossary.rst

Approach
--------

We define (our subset of) the language by means of an operational semantics.
The handling of the concrete syntax is not described formally in this document.
The definitions are based on an abstract syntax, with informal descriptions of how to abstract it.

The definitions in this document are structured in three layers:

1. Syntax tree.
   This layer recursively defines a structure in which programs are expressed, as well as certain queries on that structure.
2.  Configurations.
    This layer defines data structures to represent (comptime) program state from the perspective of a single :gl:`activation`.

    The approach used here is Structural Operational Semantics (SOS).
    This standard approach to program semantics was introduced by [SOS81]_ (re-published in 2004 as [SOS04]_; see [SOSHistory]_ for more context):
    Semantics are defined by giving a rule set that is recursive over the structure of a program.
    Structural induction [#structural-induction]_ can then be used to prove properties of the rule set or of programs.

    .. [#structural-induction] see [Burstall69]_

    In the rule set for this layer of the semantics, the rules define properties of configurations of the form `(e, env, ctx)`:

    `e ∈ GExpr` is the syntactic construct (expression, statement, or pattern) over whose structure the recursion takes place.

    Meanwhile `ctx ∈ Context` contains global state like type definitions (types are dynamically created during the comptime evaluation process) and variable memory (variables can be shared between function activations).

    Data flows down the recursion tree by the rules' choice of `env ∈ Environment` for the subconfigurations, and up using various sets, relations and functions defined on the configurations by the rules.
3.  Compiler state.
    This layer defines cross-activation behavior and dependency resolution using an activation stack and some additional state not contained in `Context`.

    While we use the same rule notation to define this layer, they are not structurally recursive and so this layer cannot be called a proper SOS.

Conventions
-----------

For a set `S`, `Pow(S)` denotes the power set of `S`.
`PowF(S)` is the set of all finite subsets of `S`.

The compilation process involves a lot of mappings of a finite set of keys to values (collections).
Given a (potentially infinite) key universe `U` and a set of potential values `V`, we write the set of all such mappings as `U →_⊥ V = {c: U → V ∪ {⊥} | ∃K ∈ PowF(U): (∀k ∈ U $/ K: c(k) = ⊥) ∧ (∀k ∈ K: c(k) ≠ ⊥)}`.
For any such `c: U →_⊥ V`, we define `Domain_⊥(c) = U $/ {k ∈ U | c(k) = ⊥}`.
We write an empty collection in `U →_⊥ V` as `[]_(U →_⊥ V)`.
Collections can be updated by applying substitutions of the form `c[k ↦ v | predicate]`, where the predicate may be omitted, and multiple mappings may be given at once.

Tuple components may be accessed using a 0-based subscript.
The expression `(f(k))_(k = 0)^n` produces a tuple `k`\th component is `f(k)`.
To fit better with 0-based indexing, the end value `n` in such tuple constructors is exclusive, i. e. the last index is `n - 1`.

In some cases, the order of tuple subscripts is not relevant.
In those cases, the element relation `∈` is used with a tuple `t` on the right-hand side as if it were `{t_i | 0 ≤ i < |t|}`.

Substitution notation can also be applied to tuples, where the left-hand side of each mapping is the index of the component to be replaced.

There are many sets of fixed-length tuples in the document, where each component has a very different meaning.
These sets are designated as named tuple sets and given names for each component.
The relevant component of such a tuple may be retrieved by suffixing it with the component name, and substitutions on named tuples use the name instead of the index of the component (e. g. `t[.componentFoo ↦ bar]`).
For brevity, multiple levels of named tuples may be substituted in one step, e. g. `t[.componentFoo.componentBar ↦ baz] = t[.componentFoo ↦ t.componentFoo[.componentBar ↦ baz]]`.

For clarity, we don't use unqualified `ℕ` in the definitions, and instead write `ℕ_0` for the set of non-negative integers, or `ℕ_(+)` for the set of strictly positive integers.

Abstract Syntax
===============

The Abstract Syntax Tree (AST) is a tree structure that roughly mirrors the parse tree of the Zig syntax, but is constructed in a way that makes it more convenient for the purpose of semantic reasoning.

Pre-abstraction checks
----------------------

There are a few syntactic requirements that are not expressible in our abstract syntax and are not enforced by the grammar, that have to be checked by the compiler:

- unescapability: quoted identifiers (and for that matter also string literals, but those are not in our language subset) may contain Unicode escapes over 0x1fffff, which are out of range for Unicode scalar values and cannot be transformed into UTF-8.
  These encodings must be rejected.
- label use: in the concrete syntax all defined labels must be used somewhere in their scope.
  This is not expressible in our AST because we require that all ``break``\s be labeled, while a loop targeted only by unlabeled ``break``/``continue`` is not allowed.

Definition
----------

Helper definitions

.. mlmath::

    MaybeComptime = {$comptime, ⊥}
    MaybeInline = {$inline, ⊥}
    Mutability = {$const, $var}
    VarAccess = Mutability ∪ {$comptime $var}
    MaybeExported = {$exported, ⊥}
    CompoundKind = {$struct, $union}
    IntBinOp = {+, +%, +$|, -, -%, -$|, *, *%, *$|, /, %, $|, &, $^}
    BinAssOps = {op= | op ∈ IntBinOp}
    BinBuiltins = {@mod, @rem, @divExact, @divTrunc, @divFloor}
    ShiftOp = {<<, <<$|, >>}
    ShiftAssOps = {<<=, <<$|=, >>=}
    AssignOps = BinAssOps ∪ ShiftAssOps
    ShiftBuiltins = {@shlExact, @shrExact}

.. mlmath::

    UnsignedInt = {$u i | i ∈ ℕ_0}
    SignedInt = {$i j | j ∈ ℕ_0}
    IntType = UnsignedInt ∪ SignedInt
    IntType_(+) = IntType ∪ {$comptime_int}

.. mlmath::

    BaseType = {$type, $void, $bool, $noreturn} ∪ IntType_(+)
    RawBool = {$true, $false}
    Primitive = BaseType ∪ RawBool ∪ {$unreachable, $_}

`stripAssign` maps in-place :gl:`update` operators to their respective out-of-place plain operators.

.. fndef::

    stripAssign: AssignOps → IntBinOp ∪ ShiftOp,
        op= ↦ op ∀op ∈ AssignOps

.. fndef::

    intWidth: IntType → ℕ_0,
        $u j ↦ j ∀j ∈ ℕ_0,
        $i j ↦ j ∀j ∈ ℕ_0

In the semantics, we will need to address a number of namespaces.
For this purpose we use the following countably infinite sets:

`Ident`
    An identifier standing for a variable or function, depending on the environment it is used in.
    Identifier expressions and definition identifiers are translated into this.
    Identifier expressions corresponding to elements of `Primitive` are translated to a member of that (disjoint) set instead.
`Label`
    An identifier naming a loop or block.
    We assume the existence of a `lab_return ∈ Label` which is fresh, in the sense that the transformation from concrete to abstract syntax will not use this element outside the `desugaring of return expressions <break-desugaring_>`_.
`CID`
    An address for a :gl:`container`.
    This is generated during the evaluation process and does not correspond to a concrete syntax construct.
`FID`
    An address for a typed function.
    This is generated during the evaluation process and does not correspond to a concrete syntax construct.
`LID`
    A lexical ID (in practice this might be a serial number or a file system path and offset).
    This is used to distinguish textually identical definitions occuring at different places in the source code.
    It is generated during the abstraction process and does not directly correspond to a concrete syntax construct.
`VID`
    An address for a variable.
    This is generated during the evaluation process and does not correspond to a concrete syntax construct.

The definitions do not make any assumptions about the representation of these sets' members.
Where, for illustration purposes, such members need to be written down, we use unquoted words for `Ident` and `Label` and ``V``/``C`` followed by a natural number for `VID` and `CID` respectively.

We assume the existence of functions `newLabel`, `newIdent`, `newVID`, `newCID`, and `newFID` that take a finite subset `S` of the respective set `U` and evaluate to an arbitrary element in `U $/ S`.
Particular strategies for computing these functions in a practical context depend on the representations of the sets and the data structures that define the input subsets.
These details are not relevant for the definitions in this document.

We define the following sets of abstract syntax contructs:

`AST`
    The set of all abstract syntax constructs.
`Parameter ⊂ AST`
    A single parameter declaration.
    This is a named tuple set with the following components:

    `.comptime`
        Whether this parameter is comptime.
    `.name`
        The name of the parameter.
    `.type`
        The type expression for the parameter.
`Parameters ⊂ AST`
    A sequence of parameter declarations.
`Decl ⊂ AST`
    A :gl:`member` declaration in a container.
`FnDecl ⊂ Decl`
    A function declaration.
`Field ⊂ AST`
    A field definition of a ``struct`` or ``union``.
    This is a named tuple set with the following components:

    `.name`
        The name of the field.
    `.type`
        The type expression.
    `.default`
        The default value expression.
`TypeBody ⊂ AST`
    The structural part of a ``struct`` or ``union`` definition.
`Expr ⊂ AST`
    The set of expressions.
`BlockExpr ⊂ Expr`
    The set of block expressions.
`CompoundLiteral ⊂ Expr`
    The set of compound literal expressions.
`Container ⊂ Expr`
    The body of a container definition.
    This is a named tuple set with the following components:

    `.lid`
        The lexical ID assigned to the definition.
        When abstracting, each `Container` is supposed to receive a distinct `LID`, but nothing in these semantic definitions requires this.
    `.members`
        A collection assigning declarations to member names.
    `.type`
        An object specifying the kind of type the container defines and its fields.

    An element of this set corresponds roughly to an instance of the ``Container`` symbol in the concrete grammar.
    This definition abstracts away details like the order of definition and identifier quoting.
`RangePattern ⊂ AST`
    The set of range :gl:`pattern`\s.
`Statement ⊂ AST`
    The set of statements, excluding expression statements.
    See below.
`LocalStmt ⊂ Statement`
    Local-defining statement.
`DeferStmt ⊂ Statement`
    Set of ``defer`` statements.

For convenience we define `GExpr = Expr ∪ Statement ∪ RangePattern`, `GStmt = Expr ∪ Statement` and `Pattern = Expr ∪ RangePattern`.

Based on these sets, we define the following functions:

`Children: AST → PowF(AST)`
    Lists all direct descendents.

The sets of syntax constructs are defined recursively.
They are the smallest sets satisfying these rules:

`(κ id: e_type) ∈ Parameter ∀κ ∈ MaybeComptime ∀id ∈ Ident ∀e_type ∈ Expr ∪ {$anytype}`
    A single parameter declaration.
    Directly corresponds to concrete syntax.

    .. mlmath::

        (κ id: e_type).comptime = κ
        (κ id: e_type).name = id
        (κ id: e_type).type = e_type
        Children((κ id: e_type)) = {e_type | e_type ≠ $anytype}

`Parameters = {p ∈ Parameter* | |p| = |{id | (κ, id, t) ∈ p}|}`
    Parameter declarations.
    Directly corresponds to the concrete syntax.

    For `params ∈ Parameters`:

    .. mlmath::

        Children(params) = {p ∈ params}

`(η μ: e_type = e_value) ∈ Decl ∀η ∈ MaybeExported ∀μ ∈ Mutability ∀e_type, e_value ∈ Expr`
    Member variable declaration

    .. mlmath::

        Children((η μ: e_type = e_value)) = {e_type, e_value}

`η $fn (params) e_return {stmts} ∈ FnDecl ∀η ∈ MaybeExported ∀params ∈ Parameters ∀e_return ∈ Expr ∀stmts ∈ GStmt*`
    Function declaration.

    .. mlmath::

        Children(η $fn (params) e_return {stmts}) = {params, e_return} ∪ {stmt ∈ stmts}

`Field = Ident × Expr × (Expr ∪ {⊥})`
    Field definitions, for ``struct`` and ``union``.

    For `(id, e_type, e_default) = f ∈ Field`:

    .. mlmath::

        Children(f) = {e_type, e_default}

`TypeBody = {(kind, fields) ∈ CompoundKind × Field* | |fields| = |{f.name | f ∈ fields}| ∧ (kind = $union ⇒ ∀f ∈ fields: f.default = ⊥)}`
    Type definition body.

    For `(kind, fields) = t ∈ TypeBody`

    .. mlmath::

        Children(t) = {f ∈ fields}

`(lid, m, t) ∈ Container ⊂ Expr ∀lid ∈ LID ∀m: Ident →_⊥ Decl ∀t ∈ TypeBody`
    Container definitions.

    .. mlmath::

        Children(lid, m, t) = {m(id) | id ∈ Domain_⊥(m)} ∪ {t}

`(e_lower ... e_upper) ∈ RangePattern ∀e_lower, e_upper ∈ Expr`
    Range patterns

    .. mlmath::

        Children((e_lower ... e_upper)) = {e_lower, e_upper}

`ℤ ⊂ Expr`
    Integer literals of all bases (binary, octal, decimal, hexadecimal) in the concrete parse tree are translated to integers in the AST.

    For `i ∈ ℤ`:

    .. mlmath::

        Children(i) = ∅

`Ident ⊂ Expr`
    Identifiers for variables.
    This is used for all identifiers that do not refer to primitives.
    If quoted identifiers that cannot be unescaped are encountered, compilation must fail.

    For `id ∈ Ident`:

    .. mlmath::

        Children(id) = ∅
`Primitive ⊂ Expr`
    Language-provided primitives.

    For `p ∈ Primitive`:

    .. mlmath::

        Children(p) = ∅

`CID ⊂ Expr`
    A resolved container reference.
    When a container is created, any members its definition closes over (both from itself and other containers in scope) are substituted with the CID-qualified version thereof.

    For `cid ∈ CID`:

    .. mlmath::

        Children(cid) = ∅

`FID ⊂ Expr`
    A resolved function reference.
    This does not correspond to a concrete grammar construct, it is only generated during evaluation.

    For `fid ∈ FID`:

    .. mlmath::

        Children(fid) = ∅

`VID ⊂ Expr`
    A resolved variable reference.
    This does not correspond to a concrete grammar construct, it is only generated during evaluation.

    For `vid ∈ VID`:

    .. mlmath::

        Children(vid) = ∅

`(e.id) ∈ Expr ∀e ∈ Expr ∀id ∈ Ident`
    Field/member access.

    .. mlmath::

        Children((e.id)) = {e}

`(op e) ∈ Expr ∀e ∈ Expr ∀op ∈ {~, -, -%, !}`
    Unary operators of all kinds.

    .. mlmath::

        Children((op e)) = {e}

`(e_a op e_b) ∈ Expr ∀e_a, e_b ∈ Expr ∀op ∈ {<, >, >=, <=, ==, !=, $and, $or, =} ∪ IntBinOp ∪ BinBuiltins ∪ AssignOps ∪ ShiftOp ∪ ShiftBuiltins`
    Binary operators of all kinds.
    The concrete syntax defined above displays the operands of operators of same precedence as an arbitrary-length list of peers.
    Transformation to the AST must translate this into binary expressions respecting associativity (all binary operators in Zig are left-associative).
    The listed builtin constructs are translated into this for 2 arguments.
    Any other number of arguments must make compilation fail.

    .. mlmath::

        Children((e_a op e_b)) = {e_a, e_b}

`($_ = e) ∈ Expr ∀e ∈ Expr`
    Discard operation.
    Disambiguated from "normal" assignment by `$_ /∈ Ident`

    .. mlmath::

        Children(($_ = e)) = {e}

`($if (e_cond) e_then $else e_else) ∈ Expr ∀e_cond, e_then, e_else ∈ Expr`
    ``if`` constructs at all levels (statement, ``Expr``, ``TypeExpr``) are translated into this form:

    - ``if`` *statements* are translated into an expression statement containing this form
    - if the concrete syntax construct does not have an ``else`` clause, set `e_else = {}` (the empty block)
    - if the ``else`` clause of an ``if`` statement is not an expression statement, it is wrapped in a block expression.

    .. mlmath::

        Children(($if (e_cond) e_then $else e_else)) = {e_cond, e_then, e_else}

`$switch (e_in) {patterns; branches} ∈ Expr ∀e_in ∈ Expr ∀patterns ∈ ((RangePattern ∪ Expr) × ℕ_0)* ∀branches ∈ Expr* ∀0 ≤ i < j < |patterns|: patterns_i.branch ≤ patterns_j.branch ∧ 0 ≤ i < |branches| ∧ |{0 ≤ i < |branches| | ¬(∃(pat, i) ∈ patterns)}| ≤ 1`
    The ``switch`` expression.
    The :gl:`patterns` and :gl:`branches` are listed separately for simpler rule definition.
    The number with each pattern indicates the branch to be taken.
    A branch not referenced by any patterns indicates an ``else`` branch.
    Compilation must fail if more than one ``else`` prong is encountered.

    .. mlmath::

        Children($switch (e_in) {patterns; branches}) = {e_in} ∪ {e | (e, i) ∈ patterns} ∪ {e ∈ branches}

`lab: {stmts} ∈ BlockExpr ⊂ Expr ∀stmts ∈ GStmt* ∀lab ∈ Label ∪ {⊥}`
    Both labeled and unlabeled block expressions are translated into this form.
    The compiler must enforce that in the concrete syntax no block not containing any ``break`` expressions targeting it has a label.
    If `lab = ⊥`, the block may be written as just `{stmts}`.
    Specifically, if `|stmts| = 0`, it can be written as `{}`, the ``void`` literal.

    .. mlmath::

        Children(lab: {stmts}) = {stmt ∈ stmts}

`($break :lab e) ∈ Expr ∀e ∈ Expr ∀lab ∈ Label`
    In Zig, ``break`` can be used to return a value from a block. [#break-block]_
    It can also be used to perform the familiar function of ending loops early.
    We simulate this here by syntactic expansion, see ``while`` below.

    .. [#break-block] see [zigdocs]_, section "blocks".

    All variants of ``break``, ``continue``, and ``return`` expressions are translated into this expression type.
    If no value expression is used in the concrete parse tree, set `e = {}` (the empty block expression).

    In the abstract syntax, all ``break`` expressions must be labeled.
    If any ``break`` expression in the concrete syntax are unlabeled, during the abstraction they must be labeled with the label of the innermost enclosing loop.

    .. _break-desugaring:

    For ``continue``, see the loop constructs below.
    ``return`` expressions with (transformed) value expression `e_val ∈ Expr` are transformed into `break :lab_return e_val`.

    .. mlmath::

        Children(($break :lab e)) = {e}

`((exprs_iter) ι $while ($true) e_body) ∈ Expr ∀ι ∈ MaybeInline ∀exprs_iter ∈ Expr* ∀e_body ∈ Expr`
    .. _while-definition:

    ``while`` loop.
    Forms with `|exprs_iter| > 0` are used as intermediate forms during evaluation.
    Accordingly, set `exprs_iter` to the empty expression list when transforming from concrete syntax.

    If the loop has ``continue``-expressions targeted at it, wrap the body in a block and replace them with ``break`` expressions (carrying a `{}` payload) targeting that block.
    If the body *is* a block, the generated ``break``\s may target that block instead and the wrapping can be omitted.

    After this step, if the loop has a continue clause, wrap the body in (potentially another) block and append the clause expression as a second statement.

    If the condition in the concrete syntax is not literally ``true``, wrap the loop in a block, and wrap the body in the 'then' branch of an ``if`` expression.
    Use the original loop condition for this ``if``, wrapped in a ``comptime`` expression if the loop is ``inline``.
    Set the ``else`` branch of the ``if`` to a ``break`` targeting the wrapping block, carrying the expression from the ``else`` branch from the loop (if any, otherwise `{}`) as payload.

    Any ``break`` expressions targeting the loop are retargeted to a block wrapped around the loop: if the loop originally had a non-``true`` condition, use the block created for that, otherwise a newly created wrapper block.

    The 'desugaring' of a fully-featured (non-inline) loop is this::

        loop_break: {
            while (true) (if (condition) {
                loop_continue: { body; };
                continue_clause;
            } else break :loop_break else_clause);
        }

    In the body, all ``continue``\s targeting the original loops are turned into ``break``\s targeting ``loop_continue``, all ``break``\s in the original loop expression now target ``loop_break``.
    ``loop_break`` and ``loop_continue`` may need to be replaced by other labels such that shadowing is avoided.

    .. mlmath::

        Children(((exprs_iter) ι $while ($true) e_body)) = {e ∈ exprs_iter} ∪ {e_body}

`($comptime e) ∈ Expr ∀e ∈ Expr`
    Comptime enforcement construct.
    This construct evaluates its argument during type analysis. [#comptime-expression]_
    It has a direct equivalent in the concrete syntax.

    .. [#comptime-expression] see [zigdocs]_, section "Compile-Time Expressions".

    .. mlmath::

        Children(($comptime e)) = {e}

`@This()`
    Produces the innermost enclosing container.

`@TypeOf(exprs) ∈ Expr ∀exprs ∈ Expr^(+)`
    Type extraction / Peer Type resolution operator.
    Instances in the concrete parse tree with no arguments must cause compilation to fail.

    .. mlmath::

        Children(@TypeOf(exprs)) = {e ∈ exprs}

`@as(e_type, e_value) ∈ Expr ∀e_type, e_value ∈ Expr`
    Coercion construct.
    Instances of ``@as`` in the parse tree with a number of arguments `nargs ≠ 2` are translated into `⊥`-expressions.

    .. mlmath::

        Children(@as(e_type, e_value)) = {e_type, e_value}

`($fn (exprs_type) e_return) ∈ Expr ∀exprs_type ∈ (Expr ∪ {$anytype})* ∀e_return ∈ Expr ∪ {$anytype}`
    Function type expression.
    Directly corresponds to the same construct in the concrete syntax.

    .. mlmath::

        Children(($fn (exprs_type) e_return)) = {e ∈ exprs_type} ∪ {e_return}

`(e_fn (exprs_args)) ∈ Expr ∀e_fn ∈ Expr ∀exprs_args ∈ Expr*`
    Function call expression.
    Directly corresponds to the same construct in the concrete syntax.

    .. mlmath::

        Children((e_fn (exprs_args))) = {e_fn} ∪ {e ∈ exprs_args}

`(e_type {fields}) ∈ CompoundLiteral ∀e_type ∈ Expr ∀fields ∈ (Ident × Expr)*: |{id | (id, $_) ∈ fields}| = |fields|`
    (Named) compound literal.
    Directly corresponds to the same construct in the concrete syntax.
    Repeated field names must be rejected.

    .. mlmath::

        Children((e_type {fields})) = {e_type} ∪ {e | (id, e) ∈ fields}

`. lid {fields} ∈ Expr ∀lid ∈ LID ∀fields ∈ (Ident × Expr)*: |{id | (id, $_) ∈ fields}| = |fields|`
    Anonymous compound literal.
    Directly corresponds to the same construct in the concrete syntax.
    Repeated field names must be rejected.

    .. mlmath::

        Children(. lid {fields}) = {e | (id, e) ∈ fields}

`(α id: t = e_val) ∈ LocalStmt ∀t ∈ Expr ∪ {$anytype} ∀e_val ∈ Expr`
    Local variable definition.
    If a local definition has no type annotation in the concrete syntax, use `$anytype` here.
    A local definition without initializer must be rejected. [#must-initialize]_

    .. [#must-initialize] see [zigdocs]_, section "Assignment".

    .. mlmath::

        Children((α id: t = e_val)) = {t, e_val} $/ {$anytype}

`(μ id: vid) ∈ LocalStmt ∀μ ∈ Mutability ∀id ∈ Ident ∀vid ∈ VID`
    Local variable marker.
    This construct does not correspond to anything in the concrete syntax and only exists in non-normal forms.
    It binds a name to a block and a VID, and establishes the mutability of the variable.

    .. mlmath::

        Children((μ id: vid)) = ∅

`($defer e) ∈ DeferStmt ∀e ∈ Expr`
    ``defer`` statements.

    .. mlmath::

        Children(($defer e)) = {e}

Many of these constructs are enclosed in parentheses to avoid ambiguities.
In the following we will omit these parentheses for better readability, unless needed to clarify nesting.

.. fndef::

    SubTree: AST → PowF(AST),
      ast ↦ {ast} ∪ ∪_(ast' ∈ Children(ast)) SubTree(ast') ∀ast ∈ AST

.. fndef::

    substComponent: Expr × Ident* × Expr → Expr ∪ {⊥},
        (e, path, e') ↦ ⊥ ∀e ∈ Expr $/ CompoundLiteral ∀path ∈ Ident* ∀e' ∈ Expr,
        (cid {fields}, path, e') ↦ cid {
          fields[i ↦ (path_0, e'')]
        } ∀cid {fields} ∈ CompoundLiteral ∀path ∈ Ident*
        ∀e' ∈ Expr ∀i ∈ ℤ: 0 ≤ i < |path| ∧ (fields_i)_0 = path_0
        ∧ e'' = substComponent((fields_i)_1, (path_(i - 1))_(k = 0)^(|path|), e') ≠ ⊥

.. fndef::

    DefinedLocals': AST → PowF(Ident),
        μ id: e_type = e_val ↦ {id} ∀(μ id: e_type = e_val) ∈ LocalStmt,
        μ id: vid ↦ {id} ∀(μ id: vid) ∈ LocalStmt,
        η $fn (params) e_return {stmts} ↦ {id |
            (id, e_type) ∈ params
        } ∀η $fn (params) e_return {stmts} ∈ FnDecl,
        ast ↦ ∅ ∀ast ∈ AST $/ LocalStmt $/ FnDecl
.. fndef::

    DefinedLocals: AST → PowF(Ident),
        ast ↦ ∪_(ast' ∈ SubTree(ast)) DefinedLocals'(ast') ∀ast ∈ AST
.. fndef::

    DefinedMembers: AST → PowF(Ident),
        cont ↦ Domain_⊥(cont.members) ∀cont ∈ Container,
        ast ↦ ∅ ∀ast ∈ AST $/ Container
.. fndef::

    DefinedMembers: AST → PowF(Ident),
        ast ↦ ∪_(ast' ∈ SubTree(ast)) DefinedMembers'(ast') ∀ast ∈ AST
.. fndef::

    FreeIdents: AST → PowF(Ident),
        ast ↦ (∪_(ast' ∈ Children(ast)) FreeIdents(ast')) $/ DefinedLocals'(ast) $/ DefinedMembers'(ast)
.. fndef::

    DefinedLabels: AST → PowF(Label),
        ast ↦ {lab ∈ Label | lab: {stmts} ∈ SubTree(ast)} ∀ast ∈ AST
.. fndef::

    exportStatus: Decl → MaybeExported,
        (η μ: e_type = e_value) ↦ η  ∀(η μ: e_type = e_value) ∈ Decl,
        (η $fn (params) e_return {stmts}) ↦ η ∀(η $fn (params) e_return {stmts}) ∈ Decl

AST invariants
--------------

These invariants must be verified at the end of abstraction: (TODO formalize)

Identifier resolution
    For any identifier expression (i. e. unqualified identifier), there is exactly 1 in-scope definition of said identifier. [#shadowing-rules]_
    Furthermore, local variables must not have a name already in scope at that point, or defined somewhere within their scope.

    .. [#shadowing-rules] see [ZigRelN0.9]_, section "Shadowing Declarations"; compare and contrast [zigdocs]_, section "Shadowing"

    .. mlmath::

        IdRes(ast, M, M', L) :⇔ (
            (ast ∈ Ident ⇒ ast ∈ M ∪ L)
            ∧ (
                (μ id: e_type = e_val) = ast ∈ LocalStmt
                ∨ (μ id: vid) = ast ∈ LocalStmt
                ⇒ id /∈ M ∪ M' ∪ L
            ) ∧ (lab: {stmts} = ast ∈ BlockExpr ⇒
                ∀i ∈ ℤ: 0 ≤ i < |stmts|
                ⇒ IdRes(ast, M, M', L ∪ {id | j ∈ ℤ ∧ 0 ≤ j < i ∧ (
                    (μ id: e_type = e_val) = stmts_j ∈ LocalStmt
                    ∨ (μ id: vid) = stmts_j ∈ LocalStmt
                )})
            ) ∧ (η $fn (params) e_return {stmts} = ast ∈ FnDecl ⇒
                L' = {id | (id, e_type) ∈ params}
                ∧ L' ∩ L = ∅
                ∧ (∀i ∈ ℤ:
                    0 ≤ i < |params|
                    ⇒ (κ id: t) = params_i
                    ∧ IdRes(t, M, M', L ∪ {t' |
                        j ∈ ℤ ∧ 0 ≤ j < i ∧ (κ' id': t') = params_j
                    })
                ) ∧ IdRes(e_return, M, M', L ∪ L')
                ∧ IdRes(lab_return: {stmts}, M, M', L ∪ L')
            ) ∧ (ast ∈ Container ⇒ ∀ast' ∈ Children(ast):
                IdRes(ast',
                    M Δ (Domain_⊥(ast.members) $/ M'),
                    M' ∪ (M ∩ Domain_⊥(ast.members)),
                    L
                )
            ) ∧ (ast /∈ BlockExpr ∪ FnDecl ∪ Container
                ⇒ ∀ast' ∈ Children(ast): IdRes(ast', M, M', L)
            )
        )

    At any node of the tree, `M` is the set of member names that can be used unqualified, `M'` is the set of shadowed member names, `L` is the set of locai variables.
    This invariant forbids shadowing except between members (which then cannot be used unqualified in the conflicting scope).

    This invariant holds for root construct `ast ∈ AST` iff `IdRes(ast, ∅, ∅, ∅)`.

Label resolution
    Any ``break`` expression is contained in a block with the target label, but not in a defer statement or container definition within the target block.
    Labels must not be shadowed.

    .. mlmath::

        LabRes(ast, L, L') :⇔ (
            ($break :lab e_val = ast ⇒ lab ∈ L)
            ∧ (
                (lab: {stmts} = ast ∈ BlockExpr ⇒ lab = ⊥)
                ∧ ast /∈ DeferStmt ∪ Container
                ⇒ ∀ast' ∈ Children(ast): LabRes(ast', L, L')
            ) ∧ (
                lab: {stmts} = ast ∈ BlockExpr ∧ lab ≠ ⊥
                ⇒ lab /∈ L ∪ L' ∧ ∀stmt ∈ stmts: LabRes(stmt, L ∪ {lab})
            ) ∧ ($defer e = ast ∨ ast ∈ Container ⇒ ∀ast' ∈ Children(ast):
                LabRes(ast, ∅, L ∪ L')
            )
        )

    At any node in the tree, `L` is the set of available labels and `L'` is the set of labels that is not available yet must not be shadowed.

    This invariant holds for root construct `ast ∈ AST` iff `LabRes(ast, ∅, ∅)`
Double-else
    A switch expression must not contain more than one ``else`` prong.
Assignment LHS
    The left-hand side (LHS) of any assignment expression must be an identifier, a VID or a member/field expression.
No default values for ``union``\s
    No field in a ``union`` definition may be assigned a value.

    .. note:: in the full language, this construct is used with tagged unions with inferred tag type. We do not model tagged unions here.

These invariants are upheld throughout evaluation (see `Evaluation invariants`_) and can be used to prove unreachability of `Corrupt` states.

Syntactic checks
----------------

In addition to the invariants listed above, the following constraints must be checked:

Identifier use
    The identifier of local variable definitions must be used unqualified at least once in the scope of the definition.
Label use
    A labeled expression contains at least one ``break`` using its label.
Trivially unreachable code
    Trivially ``noreturn`` expressions (e. g. ``break`` expressions, ``unreachable`` expressions) are rejected in certain positions.
    These include:

    - the first operand of binary operators
    - the condition of ``if`` expressions
    - the input of ``switch`` expressions
    - any statement except the last in a block


Semantics – Introduction
========================

Comptime concept
----------------

Compile-time (comptime) evaluation is a headline feature for Zig.
During compilation, types are first-class objects and type annotations are indeed normal expressions that are expected to produce such a type object.

Since such expressions must necessarily execute before type analysis is complete, they use dynamic typing. [#dynamic-typing]_
However the result of comptime evaluation is a statically-typed program in the runtime subset of the language.

.. [#dynamic-typing] see `E-Mail from Martin Wickham, 2022-02-09`_, third section.

Evaluation of an expression at comptime can result in one of 3 successful outcomes:

- a value, e. g. ``@as(u8, 5)``
- a runtime expression with comptime-known value, e. g. ``lab: {runtimeFn(); break :lab @as(u8, 5);}`` [#singlebreakexample]_
- a runtime expression without comptime-known value, e. g. the name of a runtime function parameter

.. [#singlebreakexample] see `E-Mail from Martin Wickham, 2022-02-09`_, first section

In the model described by this document, values are defined by the set `Value`, runtime expressions by `Typed`, where configurations that have a comptime-known value are also elements of `HasValue`.

A significant factor in whether an expression ends up as a typed runtime expression or is transformed into a value is the *evaluation mode*.
This is represented in the model by the `EvalMode` set.
The evaluation mode values can be grouped into 2 categories:

Execution mode: `$exec`
    In this mode, runtime expressions are not accepted and function calls are performed.
Analysis modes: `{$comptime, $runtime}`
    In these modes, function calls are considered runtime expressions.

For the root expression, the used evaluation mode depends on the type of activation being evaluated.
For instance, initializing expression of member variables are evaluated in Execution mode.
Function bodies for functions are evaluated, once per specialization, in the appropriate Analysis mode, and then in Execution mode for each comptime call.

Note that both stages of the reference compiler currently have different behavior for function bodies: in the implementation the bodies of functions called at comptime are directly executed without an analysis pass.[#onepassfn]_
The behavior described here thus has to be considered a proposal.
Expected advantages include:

- Consistency with runtime functions in terms of what has to be computable before any calls
- The purity enforcement scheme described in this document can be applied to function bodies.
- Performance:
  Since the resulting function body is statically typed, a high-performance compiler may choose to emit efficient native machine code to do perform any calls faster.

Expected concerns include:

- Additional complexity due to another analysis mode:
  The behavior of analysis for comptime execution is very similar to analysis for runtime code, so a single implementation of analysis should be able to perform both analysis modes.
- Degraded compiler performance due to additional analysis passes:
  Functions that work for both comptime and runtime will generally undergo the exact same transformation steps during analysis, so an implementation may share the transformed code unless and until a construct is encountered that discriminates between comptime and runtime analysis.
- Additional complexity due to another representation for analysed function bodies:
  The result of analysis as described in this document uses the same representation as untyped function bodies (though stricter rules apply on what constructs are used), so a compiler that prefers simplicity of implementation over high-performing comptime function calls can simply use the same code to interpret it.
- Loss of expressivity due to enforcement of static typing in all functions:
  Without static typing, functions can be written that, for instance, take or generate a type at call-time and then access members or use instances.
  Under the proposed semantics, these functions could still be used by way of comptime inline calls (which are out-of-scope for this document).

.. [#onepassfn] see `E-Mail from Martin Wickham, 2022-02-09`_, second and third section


Types of variables
------------------

During comptime, memory in Zig is allocated exclusively in terms of variables. There are two kinds of variables in Zig:

- Container-level variables: we call these 'members'. They are bound to a compound/container type. Their initial (and often final) value is computed lazily during comptime.
- Block-level variables: we call these 'locals'. They include function arguments and their values are computed eagerly.

Both members and locals may be of a compound (container) type.
These types consist of named *fields*.
An identifier can refer to either a member, local or field.
What sets field names apart from members and locals is that field names are never in scope, i. e. field accesses are always qualified with an object location such as a variable name, a pointer dereference or a slice indexing operation.

Scope
-----

Scope refers to the mechanism of resolving unqualified identifiers.
The Zig language allows shadowing only between member definitions, and unqualified references are not allowed in the code where both would be in scope.

The model of the language described in this document resolves unqualified identifiers using `Environment.localInfo`.
The information therein is collected by lexical context (from local variable markers) as well as traversing a chain of 'scope parents'.

At the start of this chain may be decreasingly specialized versions of a generic function.
Each specialization step may introduce a comptime parameter variable.

Following may be a sequence of containers, which correspond to the container expressions lexically enclosing the code being run.
Each container introduces the names of members into scope, removing those that are shadowing enclosing definitions.
A container also introduces the names of local variables that were available at its creation, so-called :gl:`upvalues`.

Activation records for function calls do not have a scope parent (since any identifiers in the function's source form would have been resolved during analysis), but their root environment contains variables for each parameter.

Consider this code::

    fn helper(a: u32) void {_ = a;}
    fn GenericType(comptime T: type) type {
        _ = T;
        return struct {
            fn init(a: u32) {
                var x: u32 = a;
                helper(x);
            }
        }
    }

When the callsite of ``helper`` is executed, the scope chain will have the following form:

- function scope for ``init``: introduces parameter variable ``a``
- container scope for the generic type: introduces upvalue variable ``T`` and member ``init``
- container scope for the root container: introduces members ``helper`` and ``GenericType``

In addition to that, the environment at the callsite will define the variable ``x`` through the successor environment mechanics.


Coercions and Peer Type Resolution
----------------------------------

Coercion is the process of implicitly converting a value based on its context.
In Zig, this happens in the following contexts:

- the @as builtin: our rules rewrite all other coercions into this  form, and it is also used to mark the type of integer literals.
- type annotations: the initializing expressions for variables are coerced to the corresponding type annotation, if present.
- switch patterns: the expressions in switch patterns are coerced to the type of the input.
- any coercions on a switch expression are applied to the selected :gl:`branch` expression.
- function call arguments are coerced to the type of the corresponding parameter.
- result expressions in ``return`` expressions are coerced to the return type of the function.
- any coercions on a block or loop expression are applied to the result expressions in break expressions targeting that block/loop.
- any coercions on an `$if` expression are applied to the :gl:`branch` expressions.
- the right-hand expression of an assignment expression is coerced to the type of (the location referred to by) the left-hand expression
- the second operand of :gl:`update`\s are coerced to a type based on the type of the first operand.

  - for arithmetic updates, the type of the first operand
  - for shift updates, the shortest type that can contain all valid shift amounts

Some expressions cause a new type to be created, unless they are coerced to a compatible type.
These are all expressions that begin with a dot, namely anonymous struct, union and, in the full language, enum literals.

Invalid coercions of runtime values are rejected at analysis time, based on types.
Coercions of comptime values are checked when they are evaluated, based on the values, not types, of the inputs.
This means that :zigdef:`fn func(param: u16) i16 {return param;}` is incorrect, but :zigexpr:`@as(i16, @as(u16, 0x7fff))` can be evaluated just fine.

Peer Type Resolution is the process of assigning a type to an expression with multiple inputs that must be converted to the same type. [#peer-type-resolution]
This is used in the following circumstances:

.. [#peer-type-resolution] see [zigdocs]_, section "Peer Type Resolution".

- the operands of binary arithmetic and bitwise operations
- the :gl:`branch`\es of conditional constructs (``if``, ``switch``), unless the conditional construct itself is being coerced
- the ``break`` payload expressions targeting blocks (and in the concrete syntax, loops; this is handled by the `desugaring described for loops <while-definition_>`_), unless the block itself is being coerced

Given a non-empty set of input types :m:`T ⊂_F TypeValue`, the rules are as follows:

- if :m:`T ∩ IntType_(+) ≠ ∅ ∧ T $/ IntType_(+) $/ {$noreturn} ≠ ∅` (mixed integer/non-integer): peer type resolution fails
- otherwise if :m:`T ∩ SignedInt ≠ ∅`: the widest signed integer type in :m:`T`
- otherwise if :m:`T ∩ UnsignedInt ≠ ∅`: the widest unsigned integer type in :m:`T`
- otherwise if `{e_type} = T $/ {$noreturn}`: `e_type`
- otherwise if `T $/ {$noreturn} = ∅`: `$noreturn`
- otherwise: peer type resolution fails

Consider the expression ``if (c) a else b`` in an environment where ``a`` and ``b`` are runtime variables of types ``u16`` and ``u32``, respectively, and ``c`` is a runtime variable of type ``bool``.
The expression will be transformed as follows:

- since ``c`` is a runtime expression of type ``bool``, but ``a`` and ``b`` have different types, which can be peer-type-resolved, (ifptr) is applied, rewriting the expression to ``@as(u32, if (c) a else b)``.
- (ifthensubexpr) propagates the coercion to the first branch,  such that (subexprcoerce) rewrites the expression to ``@as(u32, if (c) @as(u32, a) else b)``.
- both branches now have the same type ``u32``, so ``if (c) @as(u32, a) else b`` is given the type ``u32`` by (typeif), which allows (coercecollapse) to be applied, unwrapping ``if (c) @as(u32, a) else b`` from the coercion.

Evaluation-related definitions
------------------------------

Environment
...........

`AnalysisMode = {$comptime, $runtime, $typeOf}`
    Functions can be analysed for comptime or runtime execution.
    Many constructs are typed the same for either, so as long as no such constructs are encountered, the result of the analysis may be used for both runtime and comptime.
`EvalMode = {$exec} ∪ AnalysisMode`
    Evaluation encompasses analysis as well as direct execution, which only supports comptime features (runtime execution needs a different memory model, which is not described here).
`LocalInfo = (Mutability × Expr) ∪ VID ∪ CID ∪ {$comptime}`
    The meanings of an unqualified identifier are defined by elements of this set.
    For `info ∈ LocalInfo`:

    `info ∈ Mutability × Expr`
        The identifier stands for a future variable, for which the mutability and type is given.
        This is used for runtime parameters of functions.
    `info ∈ VID`
        The identifier stands for an allocated variable.
        The variable's mutability is looked up in `.mutables`.
    `info ∈ CID`
        The identifier stands for a member of the container with the given CID.
    `info = $comptime`
        The identifier stands for a comptime parameter whose value is not known yet.
`Environment = EvalMode × (Expr ∪ {$anytype}) × (Label →_⊥ Expr ∪ {$anytype}) × (Ident →_⊥ LocalInfo) × PowF(VID) × PowF(ℕ_0)`
    The environment is used to transfer information from enclosing expressions to subexpressions.
    It is stateless (i. e. not changed by evaluation steps), so it does not appear on the right side of the evaluation relation.

    This is a named tuple set with the following components:

    `.mode`
        Evaluation mode.
    `.coerce`
        Type to coerce this expression to.
        This is only informative, in the sense that the subexpression rules do not have to force an error when the result does not conform to it.
        It is mostly used to supply a type for anonymous enum/struct/union literals.
    `.labelInfo`
        Provides information on enclosing labeled expressions.
        `Domain_⊥(env.labelInfo)` is the set of enclosing labels.
        For each such label the value of the function gives the coercion info for the corresponding expression.
    `.localInfo`
        A collection mapping in-scope identifiers to their meaning.
    `.mutables`
        A set of mutable allocated variables.
        This set is propagated across function calls, since functions can write to caller memory by way of pointers, but it is reset on member evaluation frames (to enforce purity) and in the branches of runtime-dispatched conditionals (for side-effect hygiene).
    `.checkMTime`
        Used to enforce `Member definition purity`_.
        For each 'timestamp' value in this set, any member created before that timestamp must also have been modified before that timestamp.
    `.scopeParent`
        Used to set `.parent` on allocated containers.
        Note that this is never a FID, since the comptime parameters that the function specialization provides will already be added alongside other locals to `AContainer.upvalues`.

.. mlmath::

    defaultEnv = ($exec, ⊥, []_(Label →_⊥ Expr ∪ {$anytype}), []_(Ident →_⊥ LocalInfo), ∅, ∅, ⊥) ∈ Environment


Context
.......

`VarMember = {$var} × Expr`
    ``var`` members are stored with the expression containing their initial value.
`MaybeLocked = {$locked, $unlocked}`
    To enforce purity, variables closed over by member declarations are checked for modification after container creation and locked to prevent later modification.
`VarState = {($const, e, ctime, mtime, l) | e ∈ Expr ∧ ctime, mtime ∈ ℕ_0 ∧ l ∈ MaybeLocked}`
    .. _varstate:

    All variables except ``var`` members store the purity-tracking information in addition to their value.
`Function = Parameters × (Ident →_⊥ VID) × Expr × Expr × Expr × ℕ_0 × (CID ∪ FID) × PowF(ℕ_0 ∪ {$return} ∪ AnalysisMode) × PowF(ℕ_0 ∪ {$return})`
    Function object.
    This is a named tuple set with the following components:

    `.params`
        The parameter declarations
    `.comptimeArgs`
        A collection of VIDs for comptime arguments.
        When a function is specialized by a comptime parameter, that parameter is removed from the parameter list, and the argument passed is added here.
    `.retType`
        The return type expression.
    `.comptimeBody`
        The function body, analysed for comptime execution.
    `.runtimeBody`
        The function body; analysed for runtime execution.
    `.ctime`
        Creation time.
        Used to enforce purity.
    `.parent`
        Scope parent.
        Used to collect all upvalues and comptime parameter variables.
    `.evaluated`
        Contains the indices of evaluated parameter type expressions and tokens for return type and body expressions if they have respectively been evaluated.
    `.generic`
        Contains the indices of parameter type expressions that have been determined to be (transitively) dependent on a ``comptime`` or ``anytype`` parameter.
`AContainer = ℕ_0 × (Ident →_⊥ VID) × (CID ∪ FID ∪ {⊥}) × Container`
    Allocated Container.
    This is a named tuple set with the following components:

    `.creationTime`
        This number is compared to the mutation time of variables accessed in member definitions to ensure they have not been mutated after creation of the container.
    `.upvalues`
        Locals that were in scope when the container was created.
    `.parent`
        Scope parent.
        Used to collect all upvalues and comptime parameter variables.
    `.container`
        The definition of the container.
`Context = (VID →_⊥ VarState ∪ VarMember) × (CID →_⊥ AContainer) × (FID →_⊥ Function) × (Container × (Ident →_⊥ Expr) × CID →_⊥ CID) × (FID × Expr →_⊥ FID)`
    The context holds state global to all evaluations on a program.
    This is a named tuple set with the following components:

    `.vars`
        Storage of allocated variables.
    `.containers`
        Allocated containers.
    `.functions`
        Typed functions.
    `.containerIndex`
        Interning index for containers.
    `.specializationIndex`
        Interning index for function specialization.

Configuration
.............

`Configuration = (Expr ∪ RangePattern ∪ Statement) × Environment × Context`
    A configuration collects all the information used by the SOS rules to decide the next step.

Axiomatically defined sets of configurations

.. mlmath::

    Normal = Typed ∪ Value
    Location = TypedLocation ∪ LValue
    Typed = TypedValue ∪ TypedLocation
    Error = RuntimeError ∪ TypeError ∪ Corrupt
    EvalStopped = Normal ∪ Error ∪ Exit ∪ PushFrame

The following sets/relations/functions are defined by structural induction in `Configuration rule set`_.

Informational sets and query functions:

`HasType ⊂ Configuration`
    Configurations that have a type, in the sense that if coercion is removed or sustained, they will not change type to anything except ``noreturn``.
    Any configurations that would create a new type in the absence of coercion (e. g. anonymous init lists / enum variants) must not be `HasType`.
    However, configurations (e. g. conditionals, blocks) which would rewrite themselves in the presence of coercion to a different type, are allowed to be `HasType`.
    Even if a configuration is `HasType`, it might lead to an error condition or a suspend condition: if fully typed and normalized expressions are needed, look at `Normal` and its subsets, which are all subsets of `HasType`.

    This set mostly serves to define when coercion is not needed, to avoid situations where normalization would otherwise infinitely wrap the expression in coercions.
    As such, its minimal needed contents are all `Normal` configurations and configurations based on coercion expressions whose type is a `Value`.
    Wherever the rules become simpler when adding more configuration (as long as the requirements hold) we allow those as well.
`getType: HasType → Expr`
    Extracts the type of the given `HasType` configuration.
`HasValue ⊂ HasType`
    Configurations that have a value, but might not be fully evaluated yet.
`getValue: HasValue → Expr`
    Extracts the value of the given `HasValue` configuration.
`DeferredStmt ⊂ Configuration`
    This set only contains configurations based on statements.
    A configuration being in this set means the underlying statement is not done, but further execution is to be deferred to the cleanup phase of the block.
`succEnv: Configuration → Environment`
    Provides the environment under which the following statement is to be evaluated.

Shared rules states:

`LValue ⊂ HasValue`
    The expression represents a location that can be assigned to.
    `getValue` produces the value at the location.
`locationAccess: Location → Mutability`
    Indicates the mutability of the `Location`.
    Note that this includes both `LValue`\s and `TypedLocation`\s.
`modifyLValue: LValue × Ident* × Expr → Context ∪ {⊥}`
    Assigns a new value to given path the `LValue`, returning the modified `Context`, or `⊥` if the value cannot be assigned.
`Ready ⊂ Configuration`
    All subexpressions have been evaluated as far as they can.
`WaitSubExpr ⊂ Configuration`
    A subexpression needs to be evaluated before the top-level expression can be evaluated.
`subExprInfo: WaitSubExpr → GExpr × Environment × Expr*`
    For `cfg ∈ WaitSubExpr`, `(e, env, exprs) = subExprInfo(cfg)` means that sub-expression `e` is to be evaluated in environment `env`.
    If `e` turns out to be divergent, the expression is dismantled, preserving the expressions in `exprs` (see `Exit propagation`_).
`substExpr: WaitSubExpr × Expr → GExpr`
    Substitute the second argument into the first, in place of the current subexpression.
`substPattern: {(pat, env, ctx) ∈ WaitSubExpr | pat ∈ RangePattern} × RangePattern → GExpr`
    Like `substExpr`, for patterns.
`substStmt: {(stmt, env, ctx) ∈ WaitSubExpr | stmt ∈ Statement} × Statement → GExpr`
    Like `substExpr`, for statements.

Normal terminal/suspend states:

`PushFrame ⊂ Configuration`
    Used to detect that evaluation has halted because a dependency must be resolved or a call performed.
`frameReq: PushFrame → FrameReq`
    Returns information about the frame that needs to be created.
`frameResult: PushFrame × Expr → Expr`
    Substitutes the result of frame execution into the expression.
`Value ⊂ HasValue`
    Configurations whose expression represents a value.
    If the configuration is based on a statement, it means the statement is done and can be removed from the block.
`NormalPattern ⊂ HasType`
    Range patterns of a type are not values of that type, so this is another normal form for this case.
`Exit ⊂ HasType`
    Configurations whose expression exhibits divergent control flow, i. e. ``break`` (and its variants in the concrete syntax, ``continue`` or ``return``).
`TypedValue ⊂ HasType`
    The given (R)Value expression is fully typed and all parts that are comptime-evaluable are fully evaluated, but runtime evaluation is yet needed to produce a value.
`TypedLocation ⊂ HasType`
    The expression can be used to compute an `LValue` at runtime.
    Note that unlike `LValue`, `TypedLocation` is a set of terminal configurations.
`subCfgs: Normal ∪ LValue ∪ Ready ∪ NormalPattern → PowF(Normal ∪ Location)`
    Lists the (direct) subexpressions with their environments and the current context.
`Generic ⊂ Configuration`
    This terminal state behaves much like an error in that it propagates up the syntax tree in a single step.
    It signifies that the construct in question depends on the type and/or value of a ``comptime`` and/or ``anytype`` parameter that has not been resolved yet.

Error states:

`RuntimeError ⊂ Configuration`
    An error condition that can occur in any of member variable evaluation, function type analysis and function calls.
`TypeError ⊂ Configuration`
    An error condition that can occur during member variable evaluation and function type analysis, but not during function calls.
    See `Propositions`_
`Corrupt ⊂ Configuration`
    The configuration is in a state where no behavior can be specified, which is supposed to not be reachable from allowed syntax and initial context/environment.
    See `Propositions`_

Evaluation relation:

`(→) ⊂ (Configuration $/ EvalStopped) × (Expr × Context)`
    evaluation relation.

Activation stack
................

Frame requests

.. mlmath::

    MemberReq = CID × Ident
    CallReq = FID × PowF(VID) × PowF(ℕ_0) × Expr*
    TypeReq = CID
    PrototypeReq = FID
    FrameReq = MemberReq ∪ CallReq ∪ TypeReq ∪ PrototypeReq

Frame headers

.. mlmath::

    MemberVarHeader = CID × Ident
    FieldHeader = CID × ℕ_0
    FnParamHeader = FID × ℕ_0
    FnRetTypeHeader = FID
    FnBodyHeader = FID × AnalysisMode
    FnCallHeader = (Ident →_⊥ VID) × PowF(VID) × PowF(ℕ_0) × FID
    FrameHeader = MemberVarHeader ∪ FieldHeader ∪ FnParamHeader ∪ FnRetTypeHeader ∪ FnBodyHeader ∪ FnCallHeader

.. fndef::

    headerFID: FnParamHeader ∪ FnRetTypeHeader ∪ FnBodyHeader → FID,
        (fid, i) ↦ fid ∀(fid, i) ∈ FnParamHeader,
        fid ↦ fid ∀fid ∈ FnRetTypeHeader,
        (fid, mode) ↦ fid ∀(fid, mode) ∈ FnBodyHeader

.. mlmath::

    DepKey = MemberReq ∪ TypeReq ∪ PrototypeReq ∪ FnBodyHeader

.. fndef::
    depKey: FrameHeader → DepKey ∪ {⊥},
        (cid, id) ↦ (cid, id) ∀(cid, id) ∈ MemberVarHeader,
        (cid, i) ↦ cid ∀(cid, i) ∈ FieldHeader,
        fid ↦ fid ∀fid ∈ FnRetTypeHeader,
        (fid, i) ↦ fid ∀fid ∈ FnParamHeader,
        (fid, mode) ↦ (fid, mode) ∀(fid, mode) ∈ FnBodyHeader,
        h ↦ ⊥ ∀h ∈ FnCallHeader


`StackFrame = FrameHeader × Expr`
    Frames on the evaluation stack.
    This is a named tuple set with the following components:

    `.header`
        A description for why this frame was created.
        Indicates the root `environment` to be used and what must be done when evaluation finishes.
    `.expr`
        The current root expression.
`CompilerState = StackFrame* × Context × (Ident →_⊥ VID ∪ FID) × (CID × Ident →_⊥ VID ∪ FID)`
    Full evaluation state.
    This is a named tuple set with the following components:

    `.stack`
        The activation stack.
    `.context`
        The current context.
    `.exportedSymbols`
        The index of exported symbols.
    `.members`
        A mapping of member names to variables/functions.

The following sets and functions on `CompilerState` are defined below by the top-level rules:

`StateCorrupt ⊂ CompilerState`
    States that should be unreachable as a matter of rule soundness.
`CompilationFailed ⊂ CompilerState`
    The compilation has failed, the input program has no semantics associated.
`CompilationFinish ⊂ CompilerState`
    Compile-time evaluation has finished.
`nextState: CompilerState $/ StateCorrupt $/ CompilationFailed $/ CompilationFinish → CompilerState`
    Gives the next state for the compiler.
`CreateFrame ⊂ CompilerState`
    Shared rule state for pushing activation frames.
`stateFrameReq: CreateFrame → FrameReq`
    Info to pass to frame-pushing shared rules.
`FrameReturn ⊂ CompilerState`
    Shared rule state for returning a result from a frame to the previous one.
`stateReturn: FrameReturn → Expr × State`
    Gives the result expression and the state to base the successor on.

Evaluation invariants
---------------------

The `AST invariants`_ are upheld for the root expressions of stack frames throughout evaluation.

In addition, the following invariants are upheld (TODO formalize):

CID/FID/VID existence
    Any CID/FID/VID referred to in any stack frame or heap object is defined on the heap.
Variable value form
    For every variable on the heap with value expression `e_val`, `(e_val, defaultEnv, ctx) ∈ Value` holds.
Function normal form
    For every function on the heap, any parameter type and return type expression forms a `Value`, and for `FnTyped`, the body expression is `Normal`.

Changes to the heap obey the following constraints:

Variable type persistence
    Modifying the value of a variable never changes its type.

Exit propagation
----------------

Zig explicitly models the concept of divergent control flow using the ``noreturn`` type.
There are four types of divergent control flow in Zig:

- immediate process termination (normal or abnormal)
- nontermination
- labeled jump (``break`` and, in the concrete syntax, ``return`` and ``continue``); since the value exits the local expression context, we call the corresponding normal form `Exit`
- occurence of undefined behavior (e. g. executing parts of the program marked as ``unreachable``)

Since detecting all diverging expressions would require solving the halting problem generally, the ``noreturn`` type is a conservative estimate: not all divergent expressions are ``noreturn``, but all ``noreturn`` expressions are divergent.

In most situations, subexpressions following divergent expressions cannot be evaluated and the operation of the enclosing expressions cannot be performed anymore.

To reflect this, the expression is turned into a block that contains the preceding subexpressions (since they might contain runtime code) and the divergent subexpression.

The resulting expression will then be ``noreturn`` as well, which propagates up the syntax tree.
Therefore we call this mechanism 'exit propagation'.

In a block, if a statement is ``noreturn``, any later statements are discarded.
Unless there are ``break``\s targeting the block, the block itself becomes a ``noreturn`` expression.

Consider the body of this function (in concrete syntax) if specialized for ``c = true``::

    fn foo(comptime c: bool) u32 {
        if (b: {runtimeOperation(); break :b c;}) return 0;
        doSomething();
        return computeSomething();
    }

In this scenario, the condition of the ``if`` statement is comptime-known to be ``true``.
As a result, the early return becomes unconditional, causing the subsequent calls to be discarded before analysis.
The call to ``runtimeOperation`` is retained however. [#preserveruntime]_

.. [#preserveruntime] see `E-Mail from Martin Wickham, 2022-02-09`_, first section

Exit propagation is implemented in the `WaitSubExpr` shared rules to simplify the rules for most syntax constructs.
This rule is disabled in the following situations:

- For branch expressions in ``if`` and ``switch``:
  Only one of the branches will run, and (if the condition could not be evaluated during analysis) it is not known which one that is.
  Thus whether one of the branches diverges should not affect the typing of the other(s).
- The second operand of ``and`` and ``or``:
  The short-circuiting effect of the operators make them behave as conditionals, see above.
- For the body of ``defer``:
  Defer runs out-of-order with respect to the surrounding statements.
  Thus a divergent deferred statement will not prevent the subsequent statements from running.
  Labeled jumps out of ``defer`` statements are forbidden, for soundness of the cleanup mechanism, eliminating a large source of divergent expressions.
- for the body in ``while`` loops: TODO is this needed?
- Blocks.
  Exit propagation is not used for blocks for many reasons:

  - Blocks can be targeted by ``break`` expressions. This would be discarded by exit propagation.
  - Blocks have a cleanup process that has to run before exits proceed.
  - Exit propagation turns the expressions into blocks.
    If the normal exit propagation process were used here, it would make the compiler get stuck, replacing the block with a new block ad infinitum.
  - The statements of blocks are not all expressions, while exit propagation only deals with expressions. Specifically local variable definitions also affect the variables in scope for subsequent statements, which would be lost if exit propagation were applied.

Side effect hygiene
-------------------

The language constrains the mutability of variables in order to prevent side-effects from certain scopes to affect outer scopes. [#sideeffecthygiene]_
We call this constraint "side effect hygiene". [#sehname]_
Situations where it is used include:

.. [#sideeffecthygiene] see [issue1470]_.
.. [#sehname] see `E-Mail from Martin Wickham, 2022-03-02`_, third section.

- in functions, situations where runtime decisions affect if/when the code is run

  Rationale: allowing this would mean that some part (comptime side-effects) of the contained code always takes effect while another (runtime code) doesn't, which increases the difficulty of intuitively reasoning about code.
  It also excludes a class of code that would behave differently between single-pass execution and a separate typing pass followed by an execution pass.

  - branches of conditionals that cannot be dispatched when the function is compiled.
  - the body of defer statements [#deferseh]_
  - the body of loops

  .. [#deferseh] see `E-Mail from Martin Wickham, 2022-02-09`_, last section.

  These situations also disable exit propagation for the side-effect-isolated subexpressions.

- in member definitions (see `Member definition purity`_)

Note that function calls can still modify any variable that could be modified at their callsites.

We model this restriction by passing a set of mutable variables through the `Environment`, which is passed to call frames via the `FrameReq`.
It is empty in the root environment of any member definition frames.


Member definition purity
------------------------

The language enforces that the result of evaluating a member is fixed at the time the containing type is created, and that the time of its evaluation does not affect any other computation.
The purpose of this restriction is a guarantee that all member definitions can be evaluated independently, as long as dependencies are respected.

The mechanism of side effect hygiene is used to enforce that member evaluation does not affect any other computation. [#upvalueseh]_

In addition to that, it must be guaranteed that any read variables that were mutable in some running context when the type was created were and are not modified after the creation of the type.

An additional difficulty exists with specializations of generic functions:
Any memory made reachable through the specialization arguments must also have its referential transparency verified.

One difficulty with this is that because of lazy evaluation, any locals referenced by members of the type will necessarily be created after the type is defined.
To deal with this, we only perform the check on variables created before the type or function.

Since all directly addressible variables were either created before the type or are members of a type created no later than the type containing the member in question, it would take mutation (which is forbidden) to make any variable created after the type, but not created by any member definition of the type or any other type created before it, reachable.

By inducing over the dependency order of member definitions it can thus be shown that the value of a member is fixed when the containing type is defined.

The model uses a scheme similar to the one described in `E-Mail from Martin Wickham, 2021-10-14`_.
"Time" is kept for a `Context` by counting the number of created containers and functions, which monotonically increases (see `the time function <time_>`_).
The `Context` stores for each variable a creation and modification time as well as a locking flag (which is set whenever the value is observed in a way that should prevent later modification), see `its definition <varstate_>`_.

`Environment.checkMTime`, which is passed down through function calls, stores the creation times of the container whose member is being evaluated.
If the member is a function under analysis, it also stores the times when any specializations were created.
This set of numbers is collected in the `function baseEnv <base-env_>`__.

.. [#upvalueseh] see `E-Mail from Martin Wickham, 2022-03-02`_, third section.

Variable identity
-----------------

Since containers can hold members that are mutable (at runtime) they have identity beyond their contents at comptime.
It is thus relevant to define under which circumstances ``type`` values must be identical and under which they must not be.

The current Zig reference implementation creates a new type identity for every evaluation of a container expression.
To allow for generic types (which are implemented using functions returning types) to be identical even if named in separate places, function calls are memoized by their arguments. [#memo]_

This memoization causes problems if the function is supposed to also cause side effects however, so this strategy is not yet final. [#memonotfinal]_
As an alternative, this document proposes a scheme where a container expression can return a previously existing container, if that container is determined to be equivalent.

A complication for this is that members are lazily evaluated, so their contents cannot be required to be available for the decision of identity.
If instead of the evaluated members, the upvalues are used to make this decision, the problem becomes defining what variables in scope to use, since the exact set of used variables is again not known until the evaluation is done. [#upvaluesnotknown]_
This problem is exacerbated with the inclusion of pointers (which are not defined by the model in this document). [#upvaluepointers]_

For the model in this document, the key that decides the identity of the container consists of:

- the valuation of free identifiers in the container definition,
- the lexical ID (position of the definition in the source text) and
- the parent container.

.. [#memo] see [zigdocs]_, section "struct", test "linked list".
.. [#memonotfinal] see `E-Mail from Martin Wickham, 2021-09-21`_, `E-Mail from Martin Wickham, 2022-02-28`_
.. [#upvaluesnotknown] see `E-Mail from Martin Wickham, 2022-02-28`_
.. [#upvaluepointers] see `E-Mail from Martin Wickham, 2022-03-02`_, second section

Function specialization
-----------------------

Zig functions can have parameters marked as ``comptime``. [#comptime-parameter]_
This means that the argument given for this parameter is evaluated at comptime and the function specialized on its value.
In addition, parameters (``comptime`` or not) can, instead of giving a proper type expression, be designated as being ``anytype``, meaning that the function can be specialized on the type of that parameter. [#anytype-parameter]_
Using any of these mechanisms makes the function generic over that feature of the type signature.

.. [#comptime-parameter] see [zigdocs]_, section "Compile-Time Parameters".
.. [#anytype-parameter] see [zigdocs]_, section "Function Parameter Type Inference".

Parameter and return type expressions can refer to the type of any preceding parameters (mostly interesting if that parameter is ``anytype``), and to the value of preceding ``comptime`` parameters.
Since for any function reference, the type signature can be extracted as a ``type`` value, it is relevant to define what the type signature of a function with ``comptime`` or ``anytype`` parameters looks like.

In this model, a parameter whose type expression (transitively) depends on a parameter that is ``comptime`` or ``anytype``, is given as ``anytype`` in the type signature.
(Note that in the reference implementation, this type expression cannot be given as concrete syntax for technical reasons. [#returnnotanytype]_)

This dependency is detected by the terminal state `Generic` that is reachable whenever a variable is set to `$comptime` in `Environment.localInfo`, which happens if the parameter is marked ``comptime`` or ``anytype``, or if it is in `Function.generic`.
A parameter with `Generic` type expression is added to `Function.generic` to make this information available to the rules that determine the type signature as well as subsequent type expressions.
This strategy matches the idea described in `E-Mail from Martin Wickham, 2022-04-14`_, third to sixth section.

Since an argument cannot be evaluated without knowing the destination type (since the argument expression may depend on coercion context), but parameter type expressions can depend on previous type expressions, functions must be specialized iteratively, argument by argument.
In this model, a new function object is created in the `Context` for each such specialization step.

On specialization, the specialization value (argument value for ``comptime``, argument type for non-``comptime`` ``anytype`` parameters) is added to the specialized function object and the `.generic` set is cleared, since now any type expressions depending on this parameter may be able to evaluate further.
For ``comptime`` parameters, specialization involves removing the affected parameter and making it available as a (``const``) local by adding a variable to the specialization's `.comptimeArgs`.

Callsites are rewritten to point to the specialization (and for ``comptime`` parameters, the corresponding argument is removed).
When analysis finishes, callsites will point to a fully specialized function (i. e. without any ``comptime`` or ``anytype`` parameters) whose return type is known (so the call expression can be typed) and whose body can be analysed.

.. [#returnnotanytype] see `E-Mail from Martin Wickham, 2022-04-05`_, third section.

Comptime evaluation
-------------------

The compilation process begins by reading the root file of the compilation unit and parsing it according to the concrete grammar, performing the checks on the parse tree described above, then transforming it into the abstract syntax and performing the checks described for the abstract syntax tree.
If any of these fail, the compilation as a whole must fail.

After this stage, comptime evaluation begins. It is essentially a graph search for the reachable members of the compilation unit.
The starting points for this search are the exported members of the root container and other objects that end up in the compiled image.
Edges are discovered by evaluating the initializer expressions and resolving members that are found in used parts of the expression.
New nodes are added whenever a new container type is defined.

The definition of the 'used' part of an expression is quite important for the Zig programming language:
semantically ill-formed program parts, such as :zigexpr:`0/0`, are considered okay, as long as they are not used in the comptime evaluation process.
This is used as a form of conditional compilation by users of the language:
some parts of a program might be uncompilable on some platforms, and comptime conditionals are written such that these parts are avoided on such platforms.

Summary of identifier conflicts
-------------------------------

Identifiers can collide with another in multiple ways, i. e. a named object may be in multiple namespaces where name resolution must be unique, lest it cause compilation to fail.
These are the namespace types that have been identified:

- lexical scope: at any point in the source code, the names of members in all enclosing container definitions (including the root) and all locals defined previously in enclosing blocks (including conditional/loop captures and function parameters)
- exported members of all allocated containers

Rule set introduction
=====================

This document attempts to define a semantics for a real-world programming language with a comprehensive inventory of types and control flow structures.
Thus there are many rules with long premise conjunctions.
To get to grips with this we use special notation.

Rule notation
-------------

Each of the two rule sets (configuration rules, activation stack rules) is built on different data structures defined above.

Each rule set is based on a set of root elements. For the configuration rule set this is `Configuration`, for the activation stack rules it is `CompilerState`.

The majority of rules directly defines if a next step exists and how to construct the relevant state.
We call these rules "sequence rules".
To make it obvious by structure that the rule set is complete with respect to the set of root elements, the rules can be considered to be organized as a decision diagram.
Since most nodes in this decision diagram have a single predecessor, a tree-based representation was chosen.

Nodes with multiple predecessors are handled as follows:
For each such node, a set of "shared rules states" is defined (e. g. `WaitSubExpr`, `CreateFrame`).
Nodes with multiple predecessors form the root of a rule tree, with the root element being in this set as the premise.
Incoming arcs to multi-predecessor nodes are represented as rules asserting the root element to be in the shared rule state.

Since decision diagrams are acyclic, the rule set is well-defined despite using a predicate on the root element as a premise.

Premises often match on or otherwise select certain values, which are then used to define the conclusion.
The variables carrying these values can be used in the subtree under the premise.
To transfer this kind of information from one rule tree to a shared rule tree, the rule sets define query functions on the shared rule state (e. g. `subExprInfo`, `stateFrameReq`).
The value of these functions for the respective root element is defined in the conclusion of the rule asserting the element to be in the shared rule state.
They can then be applied to the root element in the shared rule tree to extract the needed information.

Rule trees are given as nested lists, where each item (usually) starts with a premise, followed by either a sub-list (optionally prefixed with some name bindings) or a rule name in parenthesis followed by the conclusion.

For sequence rules, completeness and unambiguity can be established by checking each list (independently) for uniqueness and coverage under the assumption of the ancestor premises.
To reduce redundancy, we abbreviate the conjunct negations of all preceding premises in the respective list by prefixing the premise with "otherwise if", or just "otherwise" if there is no further condition for this subtree.

Common helper definitions
-------------------------

_IntRange gives the set of integers in range for a given integer type:

.. fndef::

    IntRange: IntType_(+) → Pow(ℤ),
        $comptime_int ↦ ℤ,
        ∀t ∈ UnsignedInt ∪ {i0}: t ↦ {i ∈ ℤ | 0 ≤ i < 2^(width(t))},
        ∀t ∈ SignedInt $/ {i0}: {i ∈ ℤ | -2^(width(t) - 1) ≤ 0 < 2^(width(t) - 1)}`

_extrInt extracts the raw integer value from a value expression for the integer types; _makeInt does the inverse, packing an integer into an expression of the given integer type

.. mlmath::

    IntValue = {@as(t, i) | t ∈ IntType ∧ i ∈ IntRange(t)} ∪ ℤ

.. fndef::

    extrInt: IntValue → ℤ, i ↦ i,
        @as(t, i) ↦ i ∀t ∈ IntType ∀i ∈ ℤ

.. fndef::

    makeInt: IntType_(+) × ℤ → Expr,
        ($comptime_int, i) ↦ i ∀i ∈ ℤ,
        (t, i) ↦ @as(t, i) ∀t ∈ IntType

_shAmtType gives the smallest integer type that can fit the largest possible shift amount for a given integer type:

.. fndef::

    shAmtType: IntType_(+) → IntType_(+),
        $comptime_int ↦ $comptime_int,
        t ↦ makeUnsignedType(ceil(log_2(intWidth(t)))) ∀t ∈ IntType

_commonType is used to detect when Peer Type Resolution has completed:

.. fndef::

    commonType: PowF(Expr) → Expr ∪ {⊥},
        S ↦ $noreturn ∀S ∈ PowF(Expr): S $/ {$noreturn} = ∅,
        S ↦ e_type ∀S ∈ PowF(Expr): {e_type} = S $/ {$noreturn},
        S ↦ ⊥ ∀S ∈ PowF(Expr): |S $/ {$noreturn}| > 1

.. fndef::

    subEnv: Environment → Environment,
        env ↦ env[.coerce ↦ $anytype] ∀env ∈ Environment

Shorthands for sets of normalized configurations:

.. fndef::

    ValueOf: Expr → Pow(Configuration),
        t ↦ {cfg ∈ Value | getType(cfg) = t} ∀t ∈ Expr

.. mlmath::

    ValueT = {(e, env, ctx) ∈ Value | getType(e, env, ctx) = env.coerce ∨ env.coerce = $anytype}
    NormalT = {(e, env, ctx) ∈ Normal | getType(e, env, ctx) = env.coerce ∨ env.coerce = $anytype}
    GExit = {cfg ∈ Typed ∪ Exit | getType(cfg) = $noreturn}

.. _time:

`time` is used to set modification times of variables in relation to creation of containers and function specializations:

.. fndef::

    time: Context → ℕ_0,
        ctx ↦ |ctx.containers| + |ctx.functions| ∀ctx ∈ Context

_preserve produces a block expression with a label that discards the given list of expressions and then returns another expression:

.. fndef::

    preserve: Configuration × Expr* × Expr → Expr × Context,
        (cfg, exprs_discard, e) ↦ (lab: {
            (_ = (exprs_discard)_k)_(k = 0)^(|exprs_discard|)
            ++ (break :lab e,)
        }, ctx) ∀cfg = (_, env, ctx) ∈ Configuration
        ∀exprs_discard ∈ Expr* ∀e ∈ Expr: lab = newLabel(
            Domain_⊥(env.labelInfo)
            ∪ DefinedLabels(e)
            ∪ ∪_(e' ∈ exprs_discard) DefinedLabels(e')
        )

This function is used to dismantle expressions in exit propagation while preserving needed runtime subexpressions.
In this case, the ``break`` will be removed by the next step of exit propagation and the label removed again.
It is also used to split off runtime subexpressions so that the expression, substituted with the comptime-known values of the subexpressions, can be declared a `Value`.

_Coercibility produces the coercibility relation for the types available with the context:

.. fndef::

    Coercibility: Context → Pow(Expr × Expr),
        ctx ↦ {(a, b) ∈ IntType_(+) × IntType(+) |
            IntRange(a) ⊂= IntRange(b)
        } ∀ctx ∈ Context

Note that with the types in our language subset, there are no coercions using user-defined types.
This would change with the addition of pointers and slices.

Configuration rule set
======================

Notation
--------

The rule tree rooted at the root of the decision diagram is very large for the configuration rule set.
Certain subtrees dealing with specific syntactic constructs have therefore been split off into their own sections.

The sequence rules define the configuration to be in exactly one of the following states:

- Incomplete computation:
  These rules define the sucessor relation `→`.
- Error:
  These rules define the configuration to be in `TypeError` or `RuntimeError`, meaning that the program is incorrect.
- Corrupt:
  These rules assert `Corrupt`, meaning that the configuration is inconsistent.
- Suspend:
  These rules assert `PushFrame` or `Generic`, meaning that computation cannot make progress without externally added information.
- Normalized:
  These rules assert `Value`, `TypedValue`, `TypedLocation` or `NormalPattern`, indicating that computation has successfully finished.

`WaitSubExpr`, `Ready`, and `LValue` are used as shared rule states.

In addition to the sequence rules, the configuration rule set also has "auxiliary rules".
These are given as a separate rule tree in each section below the sequence rules.

Auxiliary rules provide the following information to other rules:

- Type.
  The typing rules assert `HasType` and accordingly define `getType` for the configuration.
  This information is used in various places throughout the ruleset.
- Value.
  The value computation rules assert `HasValue` and accordingly define `getValue` for the configuration.
  This information is mostly used by other `HasValue` rules, and for the `Ready` shared rules.
  However it is also used in some sequence rules dealing with subexpressions that have special semantics if comptime-known.
- Successor environment.
  These rules define `succEnv`, which is used by the block rules.

In premises, any free variable is automatically universally quantified (with respect to the full rule, not just the premise).
The following variable names (and primed/subscripted versions thereof) are matched to objects from the specified sets

.. mlmath::

    α ∈ VarAccess
    ι ∈ MaybeInline
    μ ∈ Mutability
    cfg ∈ Configuration
    cid ∈ CID
    cont ∈ Container
    ctx ∈ Context
    e ∈ Expr ∪ {⊥}
    exprs ∈ Expr*
    i ∈ ℤ
    lab ∈ Label ∪ {⊥}
    lid ∈ LID
    stmt ∈ Expr ∪ Statement
    stmts ∈ (Expr ∪ Statements)*
    pat ∈ Expr ∪ RangePattern
    vid ∈ VID

Root
----

The rules are organized based on syntactic construct.
This section refers to the relevant sections for any construct whose rules are not defined here.

Let `cfg = (e, env, ctx) ∈ Configuration ∧ env' = subEnv(env)`.

- `e ∈ $unreachable`:
    - .. rule::

        env.mode = $exec
        (unreachablereached) cfg ∈ RuntimeError
    - .. rule::

        env.mode ∈ AnalysisMode
        (unreachabletyped) cfg ∈ Ready
        subCfgs(cfg) = ∅
- .. rule::

    e ∈ ℤ`
    (intliteral) cfg ∈ Value
    subCfgs(cfg) = ∅
- .. rule::

    e ∈ RawBool
    (boolliteral) cfg ∈ Value
    subCfgs(cfg) = ∅
- .. rule::

    e ∈ BaseType
    (basetype) cfg ∈ Value
    subCfgs(cfg) = ∅
- `e ∈ Ident`: see `Unqualified identifiers`_
- `e_comp.id = e`: see `Field access`_
- .. rule::

    e ∈ CID
    (cidvalue) cfg ∈ Value
    subCfgs(cfg) = ∅
- `e ∈ FID`: see `FID expression`_
- `e ∈ VID`:

  - `($const, e_val, ctime, mtime, l) = ctx.vars(e) ∈ VarState`:

    - .. rule::

        ∃i ∈ env.checkMTime: ctime ≤ i < mtime
        (varimpure) cfg ∈ RuntimeError
    - .. rule:: otherwise if

        env.checkMTime ≠ ∅
        ∧ ctime ≤ max env.checkMTime
        ∧ l = $unlocked
        (varlock) cfg → (e, ctx[.vars ↦ ctx.vars[
                e ↦ (e_val, ctime, mtime, $locked, $const)
        ]])
    - otherwise: (varlvalue) let `μ = $var` if `e ∈ env.mutables ∧ l = $unlocked` else `μ = $const` in::

        cfg ∈ LValue
        subCfgs(cfg) = ∅
        getValue(cfg) = e_val
        locationAccess(cfg) = μ
        ∀path ∈ Ident* ∀e':
          e'_val = substComponent(e_val, path, e')
          ∧ (μ = $var ∧ e'_val ≠ ⊥ ⇒ modifyLValue(cfg, path, e') = ctx[.vars ↦ ctx.vars[e_loc ↦ (e_val, ctime, time(ctx), $unlocked, $const)])
          ∧ (μ = $const ∨ e'_val = ⊥ ⇒ modifyLValue(cfg, path, e') = ⊥)`

      .. TODO better format

  - `($var, e_val) = ctx.vars(e) ∈ VarMember`:

    - .. rule::

        env.mode ∈ {$runtime, $typeOf}
        (varmembertyped) cfg ∈ TypedLocation
        getType(cfg) = getType(e_val, defaultEnv, ctx)
        locationAccess(cfg) = $var, subCfgs(cfg) = ∅
    - .. rule:: otherwise

        (varmemberatcomptime) cfg ∈ TypeError

  - .. rule:: otherwise

      (vidnotdefined) cfg ∈ Corrupt

- `$comptime e_inner = e`: let `cfg_inner = (e_inner, env[.mode ↦ $exec], ctx)` in

  - .. rule::

      cfg_inner /∈ ValueT
      (comptimesubexpr) cfg ∈ WaitSubExpr
      subExprInfo(cfg) = (e_inner, env[.mode ↦ $exec], ())
      ∀e' ∈ Expr: substExpr(cfg, e') = $comptime e'
  - .. rule:: otherwise

      (comptimedone) cfg → (e_inner, ctx)

- `e = @TypeOf(exprs)`:

  - .. rule::

      NotDone = {i ∈ ℤ |
          0 ≤ i < |exprs|
          ∧ (exprs_i, env'[.mode ↦ $typeOf], ctx) /∈ Normal
      } ≠ ∅
      let i = min NotDone
      (typeofsubexpr) cfg ∈ WaitSubExpr
      subExprInfo(cfg) = (exprs_i, env', (exprs_k)_(k=0)^i)
      ∀e' ∈ Expr: substExpr(cfg, e') = @TypeOf(exprs[i ↦ e'])
  - otherwise: let `t = peerTypeResolve({getType(e_in, env', ctx) | e_in ∈ exprs}, ctx)`

    - .. rule::

        t ≠ ⊥
        (typeof) cfg → (t, ctx)
    - .. rule::

        t = ⊥
        (typeofptrfailed) cfg ∈ TypeError
- .. rule::

    @This() = e
    (thisready) cfg ∈ Ready
    subCfgs(cfg) = ∅

  This should always take the `HasValue` route since all stack frames have a (non-empty) scope chain.
- `e = @as(e_type, e_value)`: see `Coercion`_
- `e = $if (e_cond) e_then $else e_else`: see `If`_
- `e = $switch (e_in) {cases}`: see `Switch`_
- `e = lab: (e_cur) ι $while (e_cond) e_body $else e_else`: see `while`_
- `e = break :lab`: see `Break`_
- `e = lab: {stmts}`: see `Block`_
- `e = defer e'`: see `Defer statement`_
- `e = μ id: e_type = e_val`: see `Local variable definition`_
- `!e' = e`: see `Boolean complement`_
- `op ∈ {~, -, -%} ∧ op e' = e`: see `Integer complements`_
- `e = e_a op e_b`:
    - `op ∈ {==, !=}`: see `Equality`_
    - `op ∈ IntBinOp ∪ BinBuiltins`: see `Binary operators on integers`_
    - `op ∈ {<, >, >=, <=}`: see `Integer order relations`_
    - `op ∈ {$and, $or}`: see `Binary operators on booleans`_
    - `op ∈ {=} ∪ AssignOps`: see `Assignment`_
    - `op ∈ ShiftOp ∪ ShiftBuiltins`: see `Shift`_
- `($_ = e') = e`: let `cfg' = (e', env', ctx)`

  - .. rule::

      cfg_b /∈ Normal
      (discardsubexpr) cfg ∈ WaitSubExpr
      subExprInfo(cfg) = (e_b, env', ())
      ∀e' ∈ Expr: substExpr(cfg, e') = ($_ = e')
  - .. rule:: otherwise

      (discardready) cfg ∈ Ready
      subCfgs(cfg) = {(e_b, env', ctx)}

- `$fn (exprs_params) e_return = e`: see `Function type expression`_
- `e_fn (exprs_args) = e`: see `Function call expression`_
- `e ∈ Container`: see `Container expression`_
- `e_type {fields} ∧ fields ∈ (Ident × Expr)*`: see `Compound literal`_
- `. lid {fields} ∧ fields ∈ (Ident × Expr)*`: see `Anonymous compound literal`_

Auxiliary rules:

- .. rule::

    e ∈ ℤ
    (typeintliteral) cfg ∈ HasType
    getType(cfg) = $comptime_int
- .. rule::

    e ∈ RawBool
    (typeboolliteral) cfg ∈ HasType
    getType(cfg) = $bool
- .. rule::

    e ∈ BaseType
    (typebasetype) cfg ∈ HasType
    getType(cfg) = $type
- .. rule::

    e ∈ CID
    (typecid) cfg ∈ HasType
    getType(cfg) = $type
- .. rule::

    e ∈ VID ∧ (e_val, _, _, _, _) = ctx.vars(e)
    ∧ cfg_val = (e_val, defaultEnv, ctx) ∈ Value
    (typevid) cfg ∈ HasType
    getType(cfg) = getType(cfg_val)
- .. rule::

    $comptime e_inner = e
    ∧ cfg_inner = (e_inner, env[.mode ↦ $exec], ctx) ∈ HasType
    (typecomptime) cfg ∈ HasType
    getType(cfg) = getType(cfg_inner)
- .. rule::

    @This() = e
    (typethis) cfg ∈ HasType
    getType(cfg) = $type
- .. rule::

    @This() = e ∧ env.scopeParent ∈ CID
    (valuethis) cfg ∈ HasValue
    getValue(cfg) = env.scopeParent
- .. rule::

    e = @TypeOf(exprs)
    (typetypeof) cfg ∈ HasType
    getType(cfg) = $type
- .. rule::

    e = ($_ = e_b)
    (typevaluediscard) cfg ∈ HasType ∩ HasValue
    getType(cfg) = $void
    getValue(cfg) = {}
- .. rule::

    cfg ∈ Value
    (valuevalue) cfg ∈ HasValue
    getValue(cfg) = e
- .. rule::

    e ∈ Pattern
    (succenvnoop) succEnv(cfg) = env

Sub-expression shared rules
---------------------------

Let `cfg ∈ WaitSubExpr ∧ (e_sub, env', exprs_preserve) = subExprInfo(cfg) ∧ cfg' = (e_sub, env', ctx)`.

- `cfg' → (e', ctx')`:

  - .. rule::

      e' ∈ Expr
      (subexpr) cfg → (substExpr(cfg, e'), ctx')
  - .. rule::

      e', cfg.expr ∈ RangePattern
      (subexprpattern) cfg → (substPattern(cfg, e'), ctx')
  - .. rule::

      e', cfg.expr ∈ Statement
      (subexprstatement) cfg → (substStmt(cfg, e'), ctx')
  - .. rule:: otherwise

      (subexprsuccessorwrong) cfg ∈ Corrupt

    This is unreachable because all successors that are range patterns or statements originate in configurations on a range pattern or statement, respectively.

- .. rule::

    cfg' ∈ RuntimeError
    (subexprrterror) cfg ∈ RuntimeError
- .. rule::

    cfg' ∈ TypeError
    (subexprtypeerror) cfg ∈ TypeError
- .. rule::

    cfg' ∈ Corrupt
    (subexprcorrupt) cfg ∈ Corrupt
- .. rule::

    cfg' ∈ Generic
    (subexprgeneric) cfg ∈ Generic
- .. rule::

    cfg' ∈ PushFrame`
    (subexprframereq) cfg ∈ PushFrame
    frameReq(cfg) = frameReq(e_sub, env', ctx)
    ∀e': frameResult(cfg, e') = substExpr(cfg, e')
- .. rule::

    cfg' ∈ GExit
    (subexprdivergent) cfg → preserve(cfg, exprs_preserve, e_sub)

  This performs `exit propagation`_.
  This rule is disabled for the relevant constructs by not asserting `WaitSubExpr` for `GExit` subconfigurations.
- `cfg' ∈ Normal $/ GExit`

  - .. rule::

      e ∈ Expr ∧ env'.coerce ≠ $anytype ∧ getType(cfg') ≠ env'.coerce
      (subexprcoerce) cfg → (substExpr(cfg, @as(env'.coerce)), ctx)

    Note that this rule only gets used if the premise of the rule asserting `cfg ∈ WaitSubExpr` requires `getType(cfg') = env'.coerce` (`ValueT` and `NormalT` have been defined as a shorthand for this).
  - .. rule:: otherwise

      (subexprwrongcategory) cfg ∈ TypeError

    This rule is supposed to catch all normal forms that are not preempted by the premise of the rule asserting `cfg ∈ WaitSubExpr`.
    Rules for normal expressions will mask this by requiring `cfg' /∈ NormalT` (or plain `Normal` if coercion is disabled for the sub-expression), but sub-expressions that are required to be comptime may use `cfg' /∈ Value`, leaving this rule active for any normal forms containing runtime code.
    Subexpressions for which the address needs to be preserved (e. g. field access, assignment LHS) may specify `cfg /∈ Normal ∪ Location`.

Other shared rules
------------------

Let `cfg = (e, env, ctx) ∈ Configuration`.

- .. rule::

    cfg ∈ LValue
    (lvalueread) cfg → (getValue(cfg), ctx)
- `cfg ∈ Ready`:

  .. note:: the sequence rule defining this premise must also define subCfgs

  - .. rule::

      cfg ∈ HasValue ∧ subCfgs(cfg) ∩ Value = subCfgs(cfg)
      (readyeval) cfg → (getValue(cfg), ctx)
  - .. rule:: otherwise if

      env.mode = $exec
      (readyunknownvalue) cfg ∈ Corrupt
  - .. rule:: otherwise

      (readytyped) cfg ∈ TypedValue

Unqualified identifiers
-----------------------

Let `cfg = (id, env, ctx) ∈ Configuration`.

- .. rule::

    (μ, e_type) = env.localInfo(id)
    (identfuture) cfg ∈ TypedLocation
    locationAccess(cfg) = μ
    subCfgs(cfg) = ∅
- .. rule::

    vid = env.localInfo(id) ∈ VID
    (identlocal) cfg → (vid, ctx)
- .. rule::

    cid = env.localInfo(id) ∈ CID
    (identmember) cfg → (cid.id, ctx)
- .. rule::

    $comptime = env.localInfo(id)
    (identgeneric) cfg ∈ Generic
- .. rule:: otherwise

    (localscopeerror) cfg ∈ Corrupt

Typing rule:

- .. rule::

    e ∈ Ident ∧ (μ, e_type) = env.localInfo(e)
    (typevarname) cfg ∈ HasType
    getType(cfg) = e_type

FID expression
--------------

.. fndef::

    anytypeIfNotElement: (Expr ∪ {$anytype}) × (ℕ_0 ∪ {$return}) × PowF(ℕ_0 ∪ {$return} ∪ AnalysisMode) → Expr ∪ {$anytype},
        (t, token, S) ↦ $anytype ∀t ∈ Expr ∪ {$anytype} ∀S ∈ PowF(ℕ_0 ∪ {$return} ∪ AnalysisMode) ∀token ∈ (ℕ_0 ∪ {$return}) $/ S
        (t, token, S) ↦ t ∀t ∈ Expr ∪ {$anytype} ∀S ∈ PowF(ℕ_0 ∪ {$return} ∪ AnalysisMode) ∀token ∈ S $/ AnalysisMode

Let `cfg = (fid, env, ctx) ∈ Configuration`.

- `fn = ctx.functions(e) ≠ ⊥`:

  - .. rule::

      NotDone = {i ∈ ℤ |
          0 ≤ i < |fn.params| ∧ i /∈ fn.evaluated ∪ fn.generic
          ∧ fn.params_i.type ≠ $anytype
      } ≠ ∅
      ∨ $return /∈ fn.evaluated ∪ fn.generic
      (fitprotoreq) cfg ∈ PushFrame
      frameReq(cfg) = e,
      ∀e': frameResult(cfg, e') = e
  - .. rule:: otherwise

      (fidvalue) cfg ∈ Value
      subCfgs(cfg) = ∅

- .. rule:: otherwise

    (fidnotdefined) cfg ∈ Corrupt

Typing rule:

- .. rule::

    e ∈ FID ∧ fn = ctx.functions(e) ≠ ⊥
    ∧ (∀i ∈ ℤ: 0 ≤ i < |fn.params| ∧ fn.params_i ≠ $anytype ⇒
      i ∈ fn.evaluated ∪ fn.generic
    ) ∧ $return ∈ fn.evaluated ∪ fn.generic
    (typefid) cfg ∈ HasType
    getType(cfg) = $fn ((
        anytypeIfNotElement(fn.params_k.type, k, fn.evaluated)
    )_(k = 0)^(|fn.params|)) anytypeIfNotElement(
        fn.retType, $return, fn.evaluated
    )

Field access
------------

Let `cfg = (e_comp.id, env, ctx) ∈ Configuration ∧ cfg_comp = (e_comp, subEnv(env), ctx)`.

- .. rule::

    cfg_comp /∈ Normal ∪ Location
    (fieldcompsubexpr) cfg ∈ WaitSubExpr
    subExprInfo(cfg) = (e_comp, subEnv(env), ())
    ∀e' ∈ Expr: substExpr(cfg, e') = e'.id
- otherwise if: `getType(cfg_comp) = $type`:

  - .. rule::

      env' = env[.mode ↦ $exec, .coerce ↦ $type]
      ∧ cfg'_comp = (e_comp, env') /∈ ValueT
      (fieldtypesubexpr) cfg ∈ WaitSubExpr
      subExprInfo(cfg) = (e_comp, env', ())
      ∀e' ∈ Expr: substExpr(cfg, e') = e'.id
  - .. rule::

      e_comp ∈ CID
      (memberreq) cfg ∈ PushFrame
      frameReq(cfg) = (e_comp, id)
      ∀e': frameResult(cfg, e') = e'
  - .. rule:: otherwise

      (fieldtypenotcontainer) cfg ∈ TypeError

- otherwise if `cid = getType(cfg_comp) ∈ CID ∧ (kind, contfields) = ctx.containers(cid).type`:

  - .. rule::

      ¬∃(id, _) ∈ contfields
      (fieldnotdefined) cfg ∈ TypeError
  - `cfg_comp ∈ HasValue ∧ cid {fields} = getValue(e_comp)`:

    - .. rule::

        (id, e_val) /∈ fields
        (fieldinactive) cfg ∈ RuntimeError
    - .. rule::

        cfg_comp ∈ LValue
        (fieldlvalue) cfg ∈ LValue
        locationAccess(cfg) = locationAccess(cfg_comp)
        ∀path ∈ Ident* ∀e': modifyLValue(cfg, path, e') = modifyLValue(cfg_comp, path ++ (id,), e')

    - .. rule:: otherwise

        (fieldready) cfg ∈ Ready
        subCfgs(cfg) = {cfg_comp}

  - .. rule::

      cfg_comp ∈ TypedLocation
      (fieldtyped) cfg ∈ TypedLocation

- .. rule:: otherwise

    (fieldnotcompound) cfg ∈ TypeError

Auxiliary rules:

- .. mlmath::

    cfg_comp ∈ HasType ∧ cid = getType(cfg_comp) ∈ CID
    ∧ (kind, contfields) = ctx.containers(cid).type
    ∧ (id, e_type) ∈ contfields
    ∧ (e_type, defaultEnv, ctx) ∈ Value

  - .. rule::

      (typefield) cfg ∈ HasType
      getType(cfg) = e_type
  - .. rule::

      cfg_comp ∈ HasValue
      ∧ cid {fields} = getValue(cfg_comp)
      ∧ (id, e_val) ∈ fields
      (valuefield) cfg ∈ HasValue
      getValue(cfg) = e_val


Assignment
----------

If the location is an `LValue`, the second operand is evaluated at comptime.
In essence, this means a value assigned to a ``comptime var`` is evaluated as if wrapped in a ``comptime`` expression.

.. fndef::

    assignEvalMode: Location → EvalMode,
        cfg ↦ $exec ∀cfg ∈ LValue,
        cfg ↦ env.mode ∀cfg = (_, env, _) ∈ TypedLocation

.. fndef::

    assignType: ({=} ∪ AssignOps) × Expr → Expr,
        (op, t) ↦ t ∀op ∈ {=} ∪ BinAssOps ∀t ∈ Expr
        (op, t) ↦ shAmtType(t) ∀t ∈ IntType_(+) ∀op ∈ ShiftAssOps

Let `op ∈ {=} ∪ AssignOps ∧ cfg = (e_loc op e_val, env, ctx) ∈ Configuration ∧ cfg_loc = (e_loc, subEnv(env), ctx)`.

- .. rule::

    cfg_loc /∈ Normal ∪ Location
    (assignlsubexpr) cfg ∈ WaitSubExpr
    subExprInfo(cfg) = (e_loc, subEnv(env), ())
    ∀e' ∈ Expr: substExpr(cfg, e') = e' op e_val
- otherwise: let `t_loc = getType(e_loc) ∧ t_val = assignType(op, t_loc) ∧ env_val = env[.mode ↦ assignEvalMode(cfg_loc), .coerce ↦ t_val] ∧ cfg_val = (e_val, env_val, ctx)` in

  - .. rule::

      locationAccess(cfg_loc) ≠ $var
      (assignimmutable) cfg ∈ TypeError
  - .. rule:: otherwise if

      op ∈ AssignOps ∧ t /∈ IntType_(+)
      (updatenotint) cfg ∈ TypeError
  - .. rule:: otherwise if

      cfg_val /∈ NormalT
      (assignrsubexpr) cfg ∈ WaitSubExpr
      subExprInfo(cfg) = (e_val, env_val, (e_loc,))
      ∀e' ∈ Expr: substExpr(cfg, e') = e_loc = e'
  - .. rule:: otherwise if

      op ∈ {<<=, >>=} ∧ cfg_val /∈ Value
      (shiftupdnotcomptime) cfg ∈ TypeError
  - .. rule:: otherwise if

      op ∈ ShiftAssOps
      ∧ t ≠ $comptime_int
      ∧ cfg_val ∈ Value
      ∧ extrInt(e_val) ≥ intWidth(t)
      (shiftupdtoomuch) cfg ∈ TypeError
  - otherwise if `cfg_loc ∈ LValue`:

    - .. rule::

        cfg_val /∈ Value
        (assignfuturevalue) cfg ∈ TypeError
    - otherwise if `op = (=)`:

      - .. rule::

          ctx' = modifyLValue(cfg_loc, (), e_val) ≠ ⊥
          (assignsuccess) cfg → ({}, ctx')
      - .. rule:: otherwise

          (assignerror) cfg ∈ RuntimeError

    - .. rule:: otherwise

        (extendedassign) cfg → (e_loc = e_loc stripAssign(op) e_val, ctx)

  - .. rule:: otherwise

      (assignready) cfg ∈ Ready
      subCfgs(cfg) = {cfg_loc, cfg_val}

Typing rules:

- .. rule::

    (typevalueassign) cfg ∈ HasType ∩ HasValue
    getType(cfg) = $void
    getValue(cfg) = {}

Boolean complement
------------------

Let `cfg = (!e, env, ctx) ∈ Configuration ∧ env' = env[.coerce ↦ $bool] ∧ cfg' = (e, env', ctx)`.

- .. rule::

    cfg' /∈ NormalT
    (notsubexpr) cfg ∈ WaitSubExpr
    subExprInfo(cfg) = (e, env', ())
    ∀e' ∈ Expr: substExpr(cfg, e') = !e'
- .. rule:: otherwise

    (notready) cfg ∈ Ready
    subCfgs(cfg) = {cfg'}

Auxiliary rules:

- .. rule::

    (typenot) cfg ∈ HasType
    getType(cfg) = $bool
- .. rule::

    cfg' ∈ HasValue ∧ $true = getValue(cfg')
    (valuenottrue) cfg ∈ HasValue
    getValue(cfg) = $false
- .. rule::

    cfg' ∈ HasValue ∧ $false = getValue(cfg')
    (valuenotfalse) cfg ∈ HasValue
    getValue(cfg) = $true

Integer complements
-------------------

Let `op ∈ {~, -, -%} ∧ cfg = (op e, env, ctx) ∈ Configuration ∧ cfg' = (e, subEnv(env), ctx)`.

- .. rule::

    cfg' /∈ Normal
    (negsubexpr) cfg ∈ WaitSubExpr
    subExprInfo(cfg) = (e, subEnv(env), ())
    ∀e' ∈ Expr: substExpr(cfg, e') = op e'
- otherwise: let `t = getType(cfg')` in

  - .. rule::

      t /∈ IntType_(+)
      (negnoint) cfg ∈ TypeError
  - .. rule:: otherwise if

      op = (-) ∧ t ∈ SignedInt
      ∧ cfg_in ∈ HasValue ∧ -extrInt(getValue(cfg_in)) /∈ IntRange(t)
      (negoverflow) cfg ∈ RuntimeError
  - .. rule:: otherwise

      (negready) cfg ∈ Ready
      subCfgs(cfg) = {cfg_in}

Auxiliary rules:

- .. rule::

    cfg' ∈ HasType
    (typeneg) cfg ∈ HasType, getType(cfg) = getType(cfg')
- .. mlmath::

    cfg_in ∈ HasValue
    ∧ t = getType(cfg_in) ∈ IntType_(+)
    ∧ i = extrInt(getValue(cfg_in))

  - .. rule::

      op = ~
      (valueinvert) cfg ∈ HasValue
      getValue(cfg) = makeInt(t, -i - 1)
  - `op ∈ {-, -%}`:

    - .. rule::

        -i ∈ IntRange(t)
        (valueneg) cfg ∈ HasValue
        getValue(cfg) = makeInt(t, -i)
    - .. rule::

        -i /∈ IntRange(t)
        (valuenegwrap) cfg ∈ HasValue
        getValue(cfg) = getValue(cfg_in)

Binary operators on integers
----------------------------

Token sets

.. mlmath::

    SatRingOp = {+$|, -$|, *$|}
    RingOp = {+, +%, -, -%, *, *%} ∪ SatRingOp
    DivOp = {/, %, @divExact, @divFloor, @divTrunc, @mod, @rem}

Two types of wrapping:

.. fndef::

  modulo: ℤ × ℕ_(+) → ℕ_0,
      (i, j) ↦ i - j * floor(i / j)
          ∀i ∈ ℤ ∀j ∈ ℕ_(+)

.. fndef::

  remainder: ℤ × ℕ_(+) → ℤ,
      (i, j) ↦ i - j * trunc(i / j)
          ∀i ∈ ℤ ∀j ∈ ℕ_(+)

_modulo always produces positive results, _remainder produces results where the sign matches that of the numerator.

_bitLen gives the index (counted from the least significant bit) in the two's-complement binary representation of the given integer after which the last bit value (i. e. the sign value) repeats:

.. fndef::

  bitLen: ℤ → ℕ_(+),
      i ↦ ceil(log_2 (i + 1)) + 1 ∀i ∈ ℕ_0
      -i ↦ ceil(log_2 i) + 1 ∀i ∈ ℕ_(+)

_bitAt extracts the bit at the given LSB-first index of the two's-complement binary representation of the given integer (sign bit is extended ad infinitum):

.. fndef::

    bitAt: ℤ × ℕ_0 → {0, 1},
        (i, n) ↦ modulo(floor(i / 2^n), 2) ∀i ∈ ℤ ∀n ∈ ℕ_0

_bitRaw computes a binary bit operation on a single pair of bits:

.. fndef::

    bitRaw: {&, $|, $^} × {0, 1} × {0, 1} → {0, 1},
        (&, i, j) ↦ i * j ∀i, j ∈ {0, 1},
        ($|, i, j) ↦ i + j - i * j ∀i, j ∈ {0, 1},
        ($^, i, j) ↦ i + j - 2 * i * j ∀i, j ∈ {0, 1}

_bitStrToInt creates an integer from a two's complement signed binary representation to an integer:

.. fndef::

    bitStrToInt: {0, 1}* → ℤ,
        bits ↦ Σ_(k = 0)^(|bits| - 1) bits_k * 2^k - bits_(|bits| - 1) * 2^(|bits| - 1) ∀bits ∈ {0, 1}*

_binRaw computes the result of a binary integer operation, disregarding wrapping/saturation:

.. fndef::

    binRaw: IntBinOp × ℤ × ℤ → ℤ ∪ {⊥},
        (op, i, j) ↦ i + j ∀op ∈ {+, +%, +$|} ∀i, j ∈ ℤ,
        (op, i, j) ↦ i - j ∀op ∈ {-, -%, -$|} ∀i, j ∈ ℤ,
        (op, i, j) ↦ i * j ∀op ∈ {*, *%, *$|} ∀i, j ∈ ℤ,
        (op, i, j) ↦ bitStrToInt(
            (bitRaw(op, bitAt(i, k), bitAt(j, k)))_(k = 0)^(max {bitLen(i), bitLen(j)})
        ) ∀op ∈ {&, $|, $^} ∀i, j ∈ ℤ,
        (op, i, j) ↦ trunc(i / j) ∀op ∈ {/, @divExact, @divTrunc} ∀i, j ∈ ℤ: j ≠ 0,
        (@divFloor, i, j) ↦ floor(i / j) ∀i, j ∈ ℤ: j ≠ 0,
        (op, i, 0) ↦ ⊥ ∀op ∈ DivOp ∀i ∈ ℤ,
        (op, i, j) ↦ modulo(i, j) ∀op ∈ {%, @mod} ∀i ∈ ℤ ∀j ∈ ℕ_(+),
        (@rem, i, j) ↦ modulo(i, j) ∀i ∈ ℤ ∀j ∈ ℕ_(+),
        (op, i, -j) ↦ ⊥ ∀op ∈ {%, @mod, @rem} ∀i ∈ ℤ ∀j ∈ ℕ_(+)

_applyWrapSat saturates the given integer to the given type if the given operation is a saturating variant, otherwise wraps into the range:

.. fndef::

  applyWrapSat: IntBinOp × IntType_(+) × ℤ → ℤ,
    (op, t, i) ↦ i ∀op ∈ IntBinOp ∀t ∈ IntType_(+) ∀i ∈ IntRange(t),
    (op, t, i) ↦ min IntRange(t) ∀op ∈ SatRingOp ∀i ∈ ℤ ∀t ∈ SignedInt ∪ UnsignedInt: i < min IntRange(t),
    (op, t, i) ↦ max IntRange(t) ∀op ∈ SatRingOp ∀i ∈ ℤ ∀t ∈ SignedInt ∪ UnsignedInt: i > max IntRange(t),
    (op, t, i) ↦ i + j * 2^(intWidth(t)) ∀op ∈ IntBinOp $/ {+$|, -$|, *$|} ∀t ∈ SignedInt ∪ UnsignedInt ∀i ∈ ℤ $/ IntRange(t) ∀j ∈ ℤ: i - j * 2^(intWidth(t)) ∈ IntRange(t)

Let `op ∈ IntBinOp ∪ BinBuiltins ∧ cfg = (e_a op e_b, env, ctx) ∈ Configuration ∧ env' = subEnv(env) ∧ cfg_a = (e_a, env', ctx) ∧ cfg_b = (e_b, env', ctx)`.

- .. rule::

    cfg_a /∈ Normal
    (binlsubexpr) cfg ∈ WaitSubExpr
    subExprInfo(cfg) = (e_a, env', ())
    ∀e' ∈ Expr: substExpr(cfg, e') = e' op e_b
- .. rule:: otherwise if

    getType(cfg_a) /∈ IntType_(+)
    (binlnotint) cfg ∈ TypeError
- .. rule:: otherwise if

    cfg_b /∈ Normal
    (binrsubexpr) cfg ∈ WaitSubExpr
    subExprInfo(cfg) = (e_b, env', (e_a,))
    ∀e' ∈ Expr: substExpr(cfg, e') = e_a op e'
- .. rule:: otherwise if

    getType(cfg_b) /∈ IntType_(+)
    (binrnotint) cfg ∈ TypeError
- otherwise: let `Types = {getType(cfg_a), getType(cfg_b)}` in

  - `t = commonType(Types) ≠ ⊥`:

    - .. rule::

        op ∈ {/, %} ∧ t ∈ SignedInt ∪ {$comptime_int} ∧ (
            cfg_a, cfg_b ∈ HasValue
            ⇒ extrInt(getValue(cfg_a)) < 0
            ∨ extrInt(getValue(cfg_b)) < 0
        )
        (bindivsignerror) cfg ∈ TypeError
    - .. rule::

        op = @divExact
        ∧ cfg_a, cfg_b ∈ HasValue
        ∧ ¬(∃i ∈ ℤ:
          extrInt(getValue(cfg_b)) * i = extrInt(getValue(cfg_a))
        )
        (bindivinexact) cfg ∈ RuntimeError
    - .. rule::

        op ∈ {+, -, *} ∪ DivOp ∧ binRaw(
          op, extrInt(getValue(cfg_a)), extrInt(getValue(cfg_b))
        ) /∈ IntRange(t)
        (bininvalid) cfg ∈ RuntimeError

        This rule covers division by zero and negative moduli in addition to overflow, by virtue of `⊥` not being an instance of any integer type.
    - .. rule:: otherwise

        (binready) cfg ∈ Ready
        subCfgs(cfg) = {cfg_a, cfg_b}

  - .. rule:: otherwise if

      t = peerTypeResolve(Types) ≠ ⊥
      (binptr) cfg → (@as(t, i_a) op @as(t, i_b), ctx)
  - .. rule:: otherwise

      (bintypeerror) cfg ∈ TypeError

Auxiliary rules:

- .. rule::

    cfg_a, cfg_b ∈ HasType ∧ getType(cfg_a) = getType(cfg_b)
    (typebin) cfg ∈ HasType
    getType(cfg) = getType(cfg_a)
- .. rule::

    cfg_a, cfg_b ∈ HasValue
    ∧ t = commonType({
        getType(cfg_a), getType(cfg_b)
    }) ∈ IntType_(+)
    ∧ i = binRaw(
        op, extrInt(getValue(cfg_a)), extrInt(getValue(cfg_b))
    ) ∧ (op ∈ {+, -, *} ∪ DivOp ⇒ i ∈ IntRange(t))
    (valuebin) cfg ∈ HasValue
    getValue(cfg) = makeInt(t, applyWrapSat(op, i))

Integer order relations
-----------------------

.. mlmath::

    OrdRel = {<, >, >=, <=}

.. fndef::

    compareInt: OrdRel × ℤ × ℤ → RawBool,
        (rel, i, i) ↦ $false ∀i ∈ ℤ ∀rel ∈ {<, >},
        (rel, i, i) ↦ $true ∀i ∈ ℤ ∀rel ∈ {<=, >=},
        (rel, i, j) ↦ $false ∀i, j ∈ ℤ ∀rel ∈ {<, <=}: i > j,
        (rel, i, j) ↦ $true ∀i, j ∈ ℤ ∀rel ∈ {<, <=}: i < j
        (rel, i, j) ↦ $false ∀i, j ∈ ℤ ∀rel ∈ {>, >=}: i < j,
        (rel, i, j) ↦ $true ∀i, j ∈ ℤ ∀rel ∈ {>, >=}: i > j

Let `rel ∈ ArithRel ∧ cfg = (e_a rel e_b, env, ctx) ∈ Configuration ∧ env' = subEnv(env) ∧ cfg_a = (e_a, env', ctx) ∧ cfg_b = (e_b, env', ctx)`.

- .. rule::

    cfg_a /∈ Normal
    (ordlsubexpr) cfg ∈ WaitSubExpr
    subExprInfo(cfg) = (e_a, env', ())
    ∀e' ∈ Expr: substExpr(cfg, e') = e' op e_b
- .. rule:: otherwise if

    getType(cfg_a) /∈ IntType_(+)
    (ordlnotint) cfg ∈ TypeError
- .. rule:: otherwise if

    cfg_b /∈ Normal
    (ordrsubexpr) cfg ∈ WaitSubExpr
    subExprInfo(cfg) = (e_b, env', ())
    ∀e' ∈ Expr: substExpr(cfg, e') = e_a op e'
- .. rule:: otherwise if

    getType(cfg_b) /∈ IntType_(+)
    (ordrnotint) cfg ∈ TypeError
- .. rule:: otherwise

    (ordready) cfg ∈ Ready
    subCfgs(cfg) = {cfg_a, cfg_b}

Auxiliary rules:

- .. rule::

    op ∈ BinRel
    (typeord) cfg ∈ HasType
    getType(cfg) = $bool
- .. rule::

    rel ∈ BinRel ∧ cfg_a, cfg_b ∈ HasValue
    ∧ getType(cfg_a), getType(cfg_b) ∈ IntType_(+)
    (valueord) cfg ∈ HasValue
    getValue(cfg) = compareInt(
        rel, extrInt(getValue(cfg_a)), extrInt(getValue(cfg_b))
    )

Equality
--------

These differ from the integer relations above in that they do not require the operands to be integers.
They can be used on any type in our language subset except container types.
If they are used on integers, they behave much the same as the arithmetic comparisons in that they don't perform Peer Type Resolution.

Let `rel ∈ {==, !=} ∧ cfg = (e_a rel e_b, env, ctx) ∈ Configuration ∧ env' = subEnv(env) ∧ cfg_a = (e_a, env', ctx) ∧ cfg_b = (e_b, env', ctx)`.

- .. rule::

    cfg_a /∈ Normal
    (eqlsubexpr) cfg ∈ WaitSubExpr
    subExprInfo(cfg) = (e_a, env')
    ∀e' ∈ Expr: substExpr(cfg, e') = e' rel e_b
- .. rule:: otherwise if

    getType(cfg_a) ∈ CID
    (eqlcontainer) cfg ∈ TypeError
- .. rule:: otherwise if

    cfg_b /∈ Normal
    (eqrsubexpr) cfg ∈ WaitSubExpr
    subExprInfo(cfg) = (e_b, env', (e_a,))
    ∀e' ∈ Expr: substExpr(cfg, e') = e_a rel e'
- otherwise: let `t_a = getType(cfg_a) ∧ t_b = getType(cfg_b)` in

  - .. rule::

      t_b ∈ CID
      (eqrcontainer) cfg ∈ TypeError
  - .. rule:: otherwise if

      t_a ≠ t_b ∧ {t_a, t_b} $/ IntType_(+) ≠ ∅
      (eqtypeerr) cfg ∈ TypeError
  - .. rule:: otherwise

      (eqready) cfg ∈ Ready
      subCfgs(cfg) = {cfg_a, cfg_b}

Auxiliary rules:

- .. rule::

    (typeeq) cfg ∈ HasType
    getType(cfg) = $bool
- `cfg_a, cfg_b ∈ HasValue`:

  - `getType(cfg_a), getType(cfg_b) ∈ IntType_(+)`:

    - .. rule::

        rel = (==) ⇔ extrInt(e_a) = extrInt(e_b)
        (valueeqintsame) cfg ∈ HasValue
        getValue(cfg) = $true
    - .. rule::

        rel = (==) ⇔ extrInt(e_a) ≠ extrInt(e_b)
        (valueeqintdiff) cfg ∈ HasValue
        getValue(cfg) = $false

  - `{getType(cfg_a), getType(cfg_b)} $/ IntType_(+) ≠ ∅`:

    - .. rule::

        rel = (==) ⇔ e_a = e_b
        (valueeqsame) cfg ∈ HasValue
        getValue(cfg) = $true
    - .. rule::

        rel = (==) ⇔ e_a ≠ e_b
        (valueeqdiff) cfg ∈ HasValue
        getValue(cfg) = $false

Binary operators on booleans
----------------------------

.. mlmath::

    ShortCircuit = {($and, $false), ($or, $true)}

Since no other type coerces into ``bool``, Peer Type Resolution is not used.
However, these operators do exhibit "short-circuit" behavior, i. e. the second operand is not evaluated if the value of the first is sufficient for determining the result (``false`` for ``and``, ``true`` for ``or``).

Let `op ∈ {$and, $or} ∧ cfg = (e_a op e_b, env, ctx) ∈ Configuration ∧ env' = env[.coerce ↦ $bool] ∧ cfg_a = (e_a, env', ctx) ∧ cfg_b = (e_b, env'[.mutables ↦ ­∅], ctx)`.

- .. rule::

    cfg_a /∈ NormalT
    (loglsubexpr) cfg ∈ WaitSubExpr
    subExprInfo(cfg) = (e_a, env', ())
    ∀e' ∈ Expr: substExpr(cfg, e') = e' op e_b
- .. rule:: otherwise if

    cfg_a ∈ HasValue ∧ (op, getValue(cfg_a)) ∈ ShortCircuit
    (logshortcircuit) cfg → (e_a, ctx)
- .. rule:: otherwise if

    cfg_b /∈ NormalT ∪ GExit
    (logrsubexpr) cfg ∈ WaitSubExpr
    subExprInfo(cfg) = (e_b, env'[.mutables ↦ ∅], ())
    ∀e' ∈ Expr: substExpr(cfg, e') = e_a op e'
- .. rule:: otherwise

    (logtyped) cfg ∈ Ready
    subCfgs(cfg) = {cfg_a, cfg_b}

Auxiliary rules:

- .. rule::

    (typelog) cfg ∈ HasType
    getType(cfg) = $bool
- .. rule::

    cfg_a ∈ HasType ∧ cfg_b ∈ HasValue
    ∧ getType(cfg_a) = getType(cfg_b) = $bool
    ∧ (op, getValue(cfg_b)) ∈ ShortCircuit
    (valuelog) cfg ∈ HasValue
    getValue(cfg) = getValue(cfg_b)

  This is effectively a reverse short-circuit rule. [#revshortcircuit]_

.. [#revshortcircuit] see `E-Mail from Martin Wickham, 2022-03-03`_, third section.

Shift
-----

These operators coerce their second operand (the shift width) to the smallest unsigned type that can contain all valid shift amounts (`ceil(log_2(width))` bits).

Let `op ∈ ShiftOp ∪ ShiftBuiltins ∧ cfg = (e_in op e_shamt, env, ctx) ∈ Configuration ∧ cfg_in = (e_in, subEnv(env), ctx)`.

- .. rule::

    cfg_in /∈ Normal
    (shiftinsubexpr) cfg ∈ WaitSubExpr
    subExprInfo(cfg) = (e_in, subEnv(env), ())
    ∀e' ∈ Expr: substExpr(cfg, e') = e' op e_shamt
- otherwise: let `t = getType(cfg_in) ∧ env_shamt = env[.coerce ↦ shAmtType(t)] ∧ cfg_shamt = (e_shamt, env_shamt, ctx)` in

  - .. rule:: otherwise if

      t /∈ IntType_(+)
      (shiftnoint) cfg ∈ TypeError
  - .. rule:: otherwise if

      cfg_shamt /∈ NormalT
      (shiftamountsubexpr) cfg ∈ WaitSubExpr
      subExprInfo(cfg) = (e_shamt, env_shamt, (e_in,))
      ∀e' ∈ Expr: substExpr(cfg, e') = e_in op e'
  - .. rule:: otherwise if

      cfg_shamt /∈ HasValue ∧ op ≠ (<<$|)
      (shiftamountnotcomptime) cfg ∈ TypeError
  - .. rule:: otherwise if

      cfg_shamt ∈ HasValue ∧ op ≠ (<<$|)
      ∧ t ≠ $comptime_int
      ∧ extrInt(getValue(cfg_shamt)) ≥ intWidth(t)
      (shifttoomuch) cfg ∈ TypeError
  - .. rule:: otherwise if

      cfg_in, cfg_shamt ∈ HasValue ∧ op = @shrExact
      ∧ modulo(
          extrInt(getValue(cfg_in)), extrInt(getValue(cfg_shamt))
      ) ≠ 0
      (shiftrightinexact) cfg ∈ RuntimeError
  - .. rule:: otherwise if

      cfg_in, cfg_shamt ∈ HasValue ∧ op = @shlExact
      ∧ extrInt(getValue(cfg_in)) * 2^(extrInt(getValue(cfg_shamt))) /∈ IntRange
      (shiftleftinexact) cfg ∈ RuntimeError
  - .. rule:: otherwise

      (shiftready) cfg ∈ Ready
      subCfgs(cfg) = {cfg_in, cfg_shamt}

Auxiliary rules:

- .. rule::

    cfg_in ∈ HasType
    (typeshift) cfg ∈ HasType
    getType(cfg) = getType(cfg_in)
- .. mlmath::

    cfg_in ∈ HasValue ∧ t = getType(cfg_in) ∈ IntType_(+)
    ∧ cfg_shamt = (e_shamt, env[.coerce ↦ shAmtType(t)], ctx) ∈ HasValue
    ∧ i_in = extrInt(getValue(cfg_in))
    ∧ i_shamt = extrInt(getValue(cfg_shamt))

  - .. rule::

      op ∈ {>>, @shrExact}
      ∧ (t ≠ $comptime_int ⇒ i_shamt < intWidth(t))
      (valueshiftright) cfg ∈ HasValue
      getValue(cfg) = makeInt(t, floor(i_in / 2^(i_shamt)))
  - `op = {<<, <<$|, @shlExact}`: let `i_shifted = i_in * 2^(i_shamt)` in

    - .. rule::

        i_shifted ∈ IntRange(t)
        (valueshiftleft) cfg ∈ HasValue
        getValue(cfg) = makeInt(t, i_shifted)
    - `i_shifted /∈ IntRange(t)`:

      - .. rule::

          op = (<<$|)
          (valueshiftsaturate) cfg ∈ HasValue
          getValue(cfg) = makeInt(t, max IntRange(t))
        .. note:: this is a non-negative value, even if `i_in` is negative.
      - .. rule::

          op = (<<)
          let i_modulus = 2^(intWidth(t))
          ∧ i_wrapped = i_shifted - i_modulus * floor(i_shifted / i_modulus)
          ∧ (i_wrapped ∈ IntRange(t) ⇒ i_out = i_wrapped)
          ∧ (i_wrapped /∈ IntRange(t) ⇒ i_out = i_wrapped - i_modulus)
          (valueshiftlossy) cfg ∈ HasValue
          getValue(cfg) = makeInt(t, i_out)

        .. note:: `i_wrapped` is the unsigned result, the case distinction re-establishes two's complement sign representation for `i_out` for signed types.

Switch
------

.. note:: The following description is a proposal. The compiler as is labels ranges before single-value patterns and prefers ranges over single-value patterns.

.. fndef::

    MatchedValues: ValueT ∪ NormalPattern → Pow(Expr),
        (e, env, ctx) ↦ e ∀(e, env, ctx) ∈ ValueT,
        (e_lower ... e_upper, env, ctx) ↦ {makeInt(t, i) |
            t = getType(e_lower, env, ctx)
            ∧ i ∈ ℤ
            ∧ extrInt(e_lower) ≤ i ≤ extrInt(e_upper)
        } ∀(e_lower ... e_upper, env, ctx) ∈ NormalPattern

:gl:`Switch` expressions first evaluate their input expression.
If this results in a value (which is always the case in lazy or function evaluation, in the absence of errors), the patterns are evaluated in-order until one matches, at which point the entire switch is substuted for the selected value expression.
Further patterns are not evaluated and no type analysis is done on unused branches. [#switch-comptime-dispatch]_

.. [#switch-comptime-dispatch] see [zigdocs]_, section "switch", test "switch inside function".

If the input value evaluates to a non-value normal form (which only occurs during type analysis), the patterns are all evaluated and all branches are analysed for type.
Peer Type Resolution is used to form the type of the branches.
At runtime of the function, the value of the input is computed and the appropriate branch taken.

Like for ``if``, the :gl:`branch` expressions receive the coercion context of the switch itself.

Let `cfg = ($switch (e_in) {patterns; branches}, env, ctx) ∈ Configuration ∧ cfg_in = (e_in, subEnv(env), ctx) ∧ env_branch = env[.mutables ↦ ∅]`.

- .. rule::

    cfg_in /∈ Normal
    (switchinsubexpr) cfg ∈ WaitSubExpr
    subExprInfo(cfg) = (e_in, subEnv(env), ())
    ∀e' ∈ Expr: substExpr(cfg, e') = $switch (e') {patterns; branches}
- otherwise: let `t = getType(cfg_in) ∧ env_pattern = env[.mode ↦ $exec, .coerce ↦ t]` in

  - .. rule::

      t ∈ CID
      (switchoncompound) cfg ∈ TypeError
  - .. rule:: otherwise if

      NotLabeled = {i ∈ ℤ |
          0 ≤ i < |patterns| ∧ (pat, i_branch) = patterns_i
          ∧ cfg_pat = (pat, env_pattern, ctx) /∈ ValueT ∪ NormalPattern
      } ≠ ∅
      let i_pat = min NotLabeled
      ∧ (pat, i_branch) = patterns_(i_pat)
      ∧ cfg_pat = (pat, env_pattern, ctx)
      (switchpatsubexpr) cfg ∈ WaitSubExpr
      subExprInfo(cfg) = (pat, env_pattern, (e_in,))
      ∀e' ∈ Expr: substExpr(cfg, e') = $switch (e_in) {
          patterns[i_pat ↦ (e', i_branch)]; branches
      }
      pat ∈ RangePattern ⇒ (∀pat' ∈ RangePattern:
          substPattern(cfg, pat') = $switch (e_in) {
              patterns[i_pat ↦ (pat', i_branch)]; branches
          }
      )

  - .. rule:: otherwise if

      (∀i ∈ ℤ: 0 ≤ i < |branches| ⇒ (
          ∃(_, i_branch) ∈ patterns: i_branch = i
      )) ∧ (∃(e_val, env_pattern, ctx) ∈ Value:
          e ∈ Expr
          ∧ getType(cfg_unmatched) = t
          ∧ ¬(∃(pat, _) ∈ patterns:
              e_val ∈ MatchedValues(pat, env_pattern, ctx)
          )
      )
      (switchnotexhaustive) cfg ∈ TypeError
  - otherwise if `e_in ∈ HasValue`:

    - .. rule::

        Matched = {i_branch ∈ ℤ |
            ∃i ∈ ℤ: 0 ≤ i < |patterns| ∧ (pat, i_branch) = patterns_i
            ∧ getValue(e_in) ∈ MatchedValues(pat, env_pattern, ctx)
        } ≠ ∅
        (switchdispatch) cfg → preserve(
            cfg, (e_in,), branches_(min Matched)
        )
    - .. rule:: otherwise

        let i ∈ ℤ ∧ ∀(_, j) ∈ patterns: i ≠ j
        (switchelse) cfg → preserve(cfg, (e_in,), branches_i)

  - .. rule:: otherwise if

      NotTyped = {i ∈ ℤ |
          0 ≤ i < |branches|
          ∧ (branches_i, env_branch, ctx) /∈ NormalT ∪ GExit
      } ≠ ∅
      let i_branch = min NotTyped
      ∧ e_branch = branches_(i_branch)
      ∧ cfg_branch = (e_branch, env_branch, ctx)
      (switchbranchsubexpr)  cfg ∈ WaitSubExpr
      subExprInfo(cfg) = (e_branch, env_branch, ())
      ∀e' ∈ Expr: substExpr(cfg, e') = $switch (e_in) {
          patterns; branches[i_branch ↦ e']
      }
  - otherwise: let `BranchTypes = {getType(branches_i, env_branch, ctx) | 0 ≤ i < |branches|}` in

    - .. rule::

        commonType(BranchTypes) ≠ ⊥
        (switchready) cfg ∈ Ready
        subCfgs(cfg) = ({cfg_in}
            ∪ {(pat, env_pattern, ctx) | (pat, i) ∈ patterns}
            ∪ {(e_branch, env_branch, ctx) | e_branch ∈ branches}
        )
    - .. rule:: otherwise if

        t_ptr = peerTypeResolve(BranchTypes) ≠ ⊥
        (switchptr) cfg → (@as(t_ptr, $switch (e_in) {
            patterns; branches
        }))
    - .. rule:: otherwise

        (switchptrfailed) cfg ∈ TypeError

Typing rule:

- .. rule::

    ∀case ∈ cases: (extrValueExpr(case), env, ctx) ∈ HasType
    ∧ e_type = commonType(
      {getType(extrValueExpr(case), env, ctx) | case ∈ cases}
    ) ≠ ­⊥
    (typeswitch) cfg ∈ HasType
    getType(cfg) = e_type

Range pattern
.............

Let `cfg = (e_lower ... e_upper, env, ctx) ∈ Configuration ∧ cfg_lower = (e_lower, env, ctx) ∧ cfg_upper = (e_upper, env, ctx)`.

- .. rule::

    env.coerce /∈ IntType_(+)
    (rangenoint) cfg ∈ TypeError
- .. rule:: otherwise if

    cfg_lower /∈ ValueT
    (rangelsubexpr) cfg ∈ WaitSubExpr
    subExprInfo(cfg) = (e_lower, env, ())
    ∀e' ∈ Expr: substExpr(cfg, e') = e' ... e_upper
- .. rule:: otherwise if

    cfg_upper /∈ ValueT
    (rangeusubexpr) cfg ∈ WaitSubExpr
    subExprInfo(cfg) = (e_upper, env, ())
    ∀e' ∈ Expr: substExpr(cfg, e') = e_lower ... e'
- .. rule:: otherwise if

    extrInt(e_lower) > extrInt(e_upper)
    (rangeorder) cfg ∈ TypeError
- .. rule:: otherwise

    (rangedone) cfg ∈ NormalPattern
    subCfgs(cfg) = {cfg_upper, cfg_lower}

Typing rule:

- .. rule::

    cfg_lower, cfg_upper ∈ Value
    (typerange) cfg ∈ HasType
    getType(cfg) = getType(cfg_lower)

If
--

Like ``switch``, an ``if`` expression is dispatched during analysis if the condition is comptime-known, discarding the untaken branch and preventing any errors therein from surfacing. [#if-comptime-dispatch]_

.. [#if-comptime-dispatch] see [zigdocs]_, section "Compile-Time Parameters".

Let `cfg = ($if (e_cond) e_true $else e_false, env, ctx) ∈ Configuration ∧ cfg_cond = (e_cond, subEnv(env), ctx) ∧ env' = env[.mutables ↦ ∅] ∧ cfg_then = (e_true, env', ctx) ∧ cfg_else = (e_false, env', ctx)`

- .. rule::

    cfg_cond /∈ Normal
    (ifcondsubexpr) cfg ∈ WaitSubExpr
    subExprInfo(cfg) = (e_cond, subEnv(env), ())
    ∀e' ∈ Expr: substExpr(cfg, e') = $if (e') e_true $else e_false
- .. rule:: otherwise if

    getType(cfg_cond) ≠ $bool
    (ifcondnotbool) cfg ∈ TypeError

  The condition expression is not coerced to bool since ``if`` supports optionals and error unions as well in the full language.

- otherwise if `cfg_cond ∈ HasValue`:

  - .. rule::

      getValue(cfg_cond) = $true
      (iftrue) cfg → preserve(cfg, (e_cond,), e_true)
  - .. rule::

      getValue(cfg_cond) = $false
      (iffalse) cfg → preserve(cfg, (e_cond,), e_false)

- .. rule:: otherwise if

    cfg_then /∈ NormalT ∪ GExit
    (ifthensubexpr) cfg ∈ WaitSubExpr
    subExprInfo(cfg) = (e_true, env', ())
    ∀e' ∈ Expr: substExpr(cfg, e') = $if (e_cond) e' $else e_false
- .. rule:: otherwise if

    cfg_else /∈ NormalT ∪ GExit
    (ifelsesubexpr) cfg ∈ WaitSubExpr
    subExprInfo(cfg) = (e_false, env', ())
    ∀e' ∈ Expr: substExpr(cfg, e') = $if (e_cond) e_true $else e'
- otherwise: let `BranchTypes = {getType(cfg_then), getType(cfg_else)}` in

  - .. rule::

      commonType(BranchTypes) ≠ ⊥
      (ifready) cfg ∈ Ready
      subCfgs(cfg) = {cfg_cond, cfg_then, cfg_else}
  - .. rule:: otherwise if

      e_type = peerTypeResolve(
          {getType(cfg_then), getType(cfg_else)}
      ) ≠ ⊥
      (ifptr) cfg → ($if (e_cond) @as(e_type, e_true) $else @as(e_type, e_false), ctx)
  - .. rule:: otherwise

      (ifvaluetypeerror) cfg ∈ TypeError

Typing rule:

- .. rule::

    cfg_then, cfg_else ∈ HasType ∧ e_type = commonType(
        {getType(cfg_then), getType(cfg_else)}
    ) ≠ ⊥
    (typeif) cfg ∈ HasType
    getType(cfg) = e_type

While
-----

A ``while (true)`` loop can only terminate by diverging (this is why it always has type ``noreturn``).
Specifically, an ``inline while (true)`` stops unrolling when the body in an iteration is normalized to a ``noreturn`` expression.
The desugaring process causes this to happen for general ``inline while``\s when the condition evaluates to ``false``, since it introduces an unconditional ``break`` at the start of the body.
Since the branch on the original loop condition is decided at comptime for ``inline while``, any types defined in the ``else`` branch are not replicated across iterations during the unrolling.

Let `cfg = ((exprs) ι $while ($true) e_body, env, ctx) ∈ Configuration`.

- `env.mode = $exec ∨ ι = $inline`: let `env_body = env[.coerce ↦ $void]`

  - .. rule::

      NotTyped = {i ∈ ℤ |
          0 ≤ i < |exprs|
          ∧ cfg_iter = (exprs_i, env_body, ctx) /∈ NormalT
      } ≠ ∅
      let i = min NotTyped
      (whileitersubexpr) cfg ∈ WaitSubExpr
      subExprInfo(cfg) = (exprs_i, env_body, (exprs_k)_(k = 0)^i)
      ∀e' ∈ Expr: substExpr(cfg, e') = (exprs[i_iter ↦ e']) ι $while ($true) e_body

    The normal noreturn propagation makes loops terminate, preserving the previous iterations.
  - .. rule:: otherwise

      (whileexpand) cfg → ((exprs ++ (e_body,)) ι $while ($true) e_body, ctx)

- .. rule:: otherwise if

    |exprs| > 0
    (whileshouldntberunning) cfg ∈ Corrupt
- otherwise: let `env_body = env[.coerce ↦ $void, .mutables ↦ ∅] ∧ cfg_body = (e_body, env_body, ctx)` in

  - .. rule::

      cfg_body /∈ NormalT ∪ GExit
      (whilebodysubexpr) cfg ∈ WaitSubExpr
      subExprInfo(cfg) = (e_body, env_body, ())
      ∀e' ∈ Expr: substExpr(cfg, e') = () ι $while ($true) e'
  - .. rule:: otherwise

      (whileready) cfg ∈ Ready
      subCfgs(cfg) = {cfg_body}

Typing rule:

- .. rule::

    (typewhile) cfg ∈ HasType
    getType(cfg) = $noreturn

Break
-----

Let `cfg = ($break :lab e_val, env, ctx) ∈ Configuration`.

- .. rule::

    env.labelInfo(lab) = ⊥
    (breaknolabel) cfg ∈ Corrupt
- otherwise: let `env_val = env[.coerce ↦ env.labelInfo(lab)] ∧ cfg_val = (e_val, env_val, ctx)` in

  - .. rule:: otherwise if

      cfg_val /∈ NormalT
      (breaksubexpr) cfg ∈ WaitSubExpr
      subExprInfo(cfg) = (e_val, env_val, ())
      ∀e' ∈ Expr: substExpr(cfg, e') = $break :lab e'
  - .. rule:: otherwise if

      env.mode = $exec
      (exitbreak) cfg ∈ Exit
  - .. rule:: otherwise

      (breakready) cfg ∈ Ready
      subCfgs(cfg) = {cfg_val}

Typing rule:

- .. rule::

    (typebreak) cfg ∈ HasType
    getType(cfg) = $noreturn

Block
-----

.. fndef::

    blockEnv: (BlockExpr × Environment × Context) × ℕ_0 → Environment,
        (({stmts}, env, ctx), i) ↦ env[.coerce ↦ $void] ∀({stmts}, env, ctx) ∈ Configuration ∀i ∈ ℤ: i ≤ 0 ∨ i ≥ |stmts|,
        ((lab: {stmts}, env, ctx), i) ↦ env[.coerce ↦ $void, .labelInfo ↦ env.labelInfo[lab ↦ env.coerce]] ∀(lab: {stmts}) ∈ BlockExpr ∀env ∈ Environment ∀ctx ∈ Context ∀i ∈ ℤ: i ≤ 0 ∨ i ≥ |stmts| ∧ lab ≠ ⊥,
        ((lab: {stmts}, env, ctx), i) ↦ succEnv(blockSubCfgs(lab: {stmts, env, ctx})_(i - 1)) ∀(lab: {stmts}) ∈ BlockExpr ∀env ∈ Environment ∀ctx ∈ Context ∀i ∈ ℕ: 0 < i ≤ |stmts|

.. fndef::

    blockSubCfgs: BlockExpr × Environment × Context → Configuration*,
        (lab: {stmts}, env, ctx) ↦ (stmts_(i - 1), blockEnv((lab: {stmts}, env, ctx), i - 1), ctx)_(i = 0)^(|stmts|) ∀(lab: {stmts}) ∈ BlockExpr ∀env ∈ Environment ∀ctx ∈ Context

.. mlmath::
    SecondPassDone = {cfg ∈ Typed | getType(cfg) = $void}
    FirstPassDone = DeferredStmt ∪ SecondPassDone

.. fndef::

    subCfgs*: Normal → PowF(Normal),
        cfg ↦ {cfg} ∪ \∪cfg' ∈ subCfgs(cfg): subCfgs*(cfg') ∀cfg ∈ Normal

Let `cfg = (lab: {stmts}, env, ctx) ∈ Configuration ∧ cfgs = blockSubCfgs(cfg)`.

- .. rule::

    |stmts| = 0
    (voidliteral) cfg ∈ Value
    subCfgs(cfg) = ∅
- .. mlmath::

    NotDeferred = {i ∈ ℤ |
        0 ≤ i < |stmts| ∧ cfg_stmt = cfgs_i /∈ FirstPassDone
        ∧ (i = |stmts| - 1 ⇒ cfg_stmt /∈ GExit)
    } ≠ ∅

  Let `i = min NotDeferred ∧ cfg_stmt = (stmt, env_stmt, _) = cfgs_i` in

  - .. rule::

      cfg_stmt ∈ ValueT
      (ignorevoid) cfg → (lab: {
          (stmts_k)_(k = 0)^i
          ++ (stmts_(k + i - 1))_(k = 0)^(|stmts| - i - 1)
      }, ctx)

    This rule is used for cleaning up finished statements in both the forward execution and the backward cleanup phases.
  - .. rule::

      cfg_stmt ∈ GExit
      (blockdropdeadcode) cfg → (lab: {(stmts_k)_(k = 0)^(i + 1)}, ctx)

    The premise of this rule implies `stmts_i` is of type ``noreturn``.
    Since `NotDeferred` does not include the last element if it is ``noreturn``, the second phase (if necessary) will start after this rule is used.
  - .. rule:: otherwise

      (blockforwardsubexpr) cfg ∈ WaitSubExpr
      subExprInfo(cfg) = (stmt, env_stmt, ())
      ∀e' ∈ Expr: substExpr(cfg, e') = lab: {stmts[i ↦ e']}
      stmt ∈ Statement ⇒ ∀stmt': substStmt(cfg, stmt') = lab: {stmts[i ↦ stmt']}

    The check for ignored results is automatically done by the shared sub-expression rules (since we coerce statements to ``void``).
    The normal rules for exit propagation are not used because they would create a new block expression as a result, which would lead to evaluation getting stuck applying the rule ad infinitum, as well as losing any label the block might have, which is used to detect ``break``\s targeting the block in (blockbreak) below.
    Exit propagation would also preclude running cleanup at the end of the block.
    Instead (blockdropdeadcode) above is used if the statement is not the last in the block otherwise (blockbreak) or (blockexit) below.

- .. rule:: otherwise if

    NotCleaned = {i ∈ ℤ |
        0 ≤ i < |stmts| ∧ cfg_stmt = cfgs_i /∈ SecondPassDone
        ∧ (i = |stmts| - 1 ⇒ cfg_stmt /∈ GExit)
    } ≠ ∅
    let i = max NotCleaned
    ∧ cfg_stmt = (stmt, env_stmt, _) = stmts_i
    (blockcleanupsubexpr) cfg ∈ WaitSubExpr
    subExprInfo(cfg) = (stmt, env_stmt, ())
    ∀e' ∈ Expr: substExpr(cfg, e') = lab: {stmts[i ↦ e']}
    stmt ∈ Statement ⇒ ∀stmt': substStmt(cfg, stmt') = lab: {stmts[i ↦ stmt']}
- otherwise if `cfg_end = cfgs_(|stmts| - 1) ∈ Exit`: TODO what happens if `|stmts| > 1`?

  - .. rule::

      $break :lab e_val = cfg_end.expr
      (blockbreak) cfg → (e_val, ctx)
  - .. rule:: otherwise

      (blockexit) cfg → (cfg_end.expr, ctx)

- .. rule:: otherwise if

    {stmts'} = stmt_(|stmts| - 1)
    (blocktailcollapse) cfg → (lab: {(stmts_k)_(k = 0)^(|stmts| - 1) ++ stmts'})

  This rule only applies to blocks that are not labeled.
  (blocknottargeted) below will strip the label off untargeted blocks.
- otherwise: let `BreakTypes = {getType((e, env', ctx)) | cfg_stmt ∈ cfgs ∧ (break :lab e, env', ctx) ∈ subCfgs*(cfg_stmt)} ∧ ResultTypes = BreakTypes ∪ {getType(cfgs_k) | 0 ≤ k = |stmts| - 1}` in

  - .. rule::

      BreakTypes = ∅ ∧ lab ≠ ⊥
      (blocknottargeted) cfg → ({stmts}, ctx)
  - .. rule::

      t = commonType(ResultTypes) ≠ ⊥
      (blockready) cfg ∈ Ready
      subCfgs(cfg) = {cfg' ∈ cfgs}
  - .. rule:: otherwise if

      t = peerTypeResolve(ResultTypes) ≠ ⊥
      (blockptr) cfg → (@as(t, lab: {stmts}), ctx)
  - .. rule:: otherwise

      (blocktypeconflict) cfg ∈ TypeError


Auxiliary rules:

- .. rule::

    ∀cfg_stmt ∈ cfgs: cfg_stmt ∈ NormalT
    (typevalueblockvoid) cfg ∈ HasType ∩ HasValue
    getType(cfg) = $void
    getValue(cfg) = {}
- `|stmts| > 0 ∧ (∀0 ≤ i < |stmts| - 1: cfg_stmt = (stmts_i, blockEnv(env, lab, stmts, i), ctx) ∈ NormalT) ∧ cfgs_(|stmts| - 1) ∈ GExit`:

  - .. rule::

      t = commonType({getType(e', env', ctx) |
          (break :lab e', env', ctx) ∈ subCfgs*(cfg)
      }) ≠ ⊥
      (typeblock) cfg ∈ HasType
      getType(cfg) = t
  - .. rule::

      breaks = {cfg_break |
          cfg_stmt ∈ cfgs
          ∧ cfg_break = ($break :lab e_val, _, _) ∈ subCfgs*(cfg_stmt)
      } ∧ (∀cfg_break ∈ breaks:
          {cfg_val} = subCfgs(cfg_break)
          ∧ cfg_val ∈ HasValue
      ) ∧ {e_val} = {getValue(cfg_val) |
          cfg_break ∈ breaks ∧ {cfg_val} = subCfgs(cfg_break)
      }
      (valueblock) cfg ∈ HasValue
      getValue(cfg) = e_val

    This rule also covers multiple ``break``\s (as long as they have the same comptime-known value), in addition to the single-break case.
    This is a proposed generalization; the current reference implementation does not assign a comptime-known value to blocks with a multiple ``break``\s, even if all of them are comptime-known to have the same value. [#singlebreak]_

.. [#singlebreak] see `E-Mail from Martin Wickham, 2022-03-03`_, first section and `E-Mail from Martin Wickham, 2022-02-09`_, first section; compare and contrast `E-Mail from Martin Wickham, 2022-03-18`_. The generalization was included in the model because distinguishing same-value ``break``\s would require significant changes.

Defer statement
---------------

In `$exec` mode, the body of a ``defer`` statement is executed after the forward execution of the other statements has finished, in the cleanup phase, in reverse order.

By contrast, in analysis mode, when the forward execution will end is not known.
Therefore, the body is analysed in order with respect to the surrounding statements, with side effect hygiene to prevent state modifications that would be in a different order if executed without analysis. [#deferorder]_

Let `cfg = (defer e, env, ctx) ∈ Configuration ∧ (env.mode = $exec ⇒ env' = env[.coerce ↦ $void]) ∧ (env.mode ∈ AnalysisMode ⇒ env' = env[.coerce ↦ $void, .mutables ↦ ∅]) ∧ cfg_body = (e, env', ctx)`.

- .. rule::

    cfg_body ∈ Exit
    (deferexit) cfg ∈ Corrupt

  This should not be reachable since no ``break`` in `e_body` is allowed to target a block not in `e_body`.
  Note that other ``noreturn`` expressions (e. g. infinite loops, panics) are allowed, without making the block ``noreturn``. TODO citation
- .. rule::

    cfg_body /∈ NormalT ∪ GExit
    (defersubexpr) cfg ∈ WaitSubExpr
    subExprInfo(cfg) = (e, env', ())
    ∀e' ∈ Expr: substExpr(cfg, e') = defer e'
- .. rule:: otherwise

    (defertyped) cfg ∈ Ready
    subCfgs(cfg) = {cfg_body}

Auxiliary rules:

- .. rule::

    env.mode = $exec
    (deferreddefer) cfg ∈ DeferredStmt
- .. rule::

    (typevaluedefer) cfg ∈ HasType ∪ HasValue
    getType(cfg) = $void
    getValue(cfg) = {}
- .. rule::

    (succenvdefer) succEnv(cfg) = env

.. [#deferorder] see `E-Mail from Martin Wickham, 2022-02-09`_, fourth section.

Local variable definition
-------------------------

``const`` definitions whose initializer evaluates to a `Value` (instead of `Typed`) allocate and initialize the value right then and there (even in analysis mode), i. e. comptime-known ``const`` variables exist at comptime, just like ``comptime var`` variables.
``comptime var`` initializers are evaluated in `$exec` mode.

.. fndef::

    localMutability: VarAccess → Mutability,
        $comptime $var ↦ $var, $var ↦ $var,
        $const ↦ $const

.. fndef::

    localValueMode: EvalMode × VarAccess → EvalMode,
        ($exec, α) ↦ $exec ∀α ∈ VarAccess
        (mode, $comptime $var) ↦ $exec ∀mode ∈ AnalysisMode,
        (mode, μ) ↦ mode ∀mode ∈ AnalysisMode ∀μ ∈ Mutability

Let `cfg = (α id: t = e_val, env, ctx) ∈ Configuration ∧ env_type = env[.mode ↦ $exec, .coerce ↦ $type] ∧ cfg_type = (t, env_type, ctx) ∧ env_val = env[.mode ↦ localValueMode(env.mode, α), .coerce ↦ t] ∧ cfg_val = (e_val, env_val, ctx)`.

- .. rule::

    t ≠ $anytype ∧ cfg_type /∈ ValueT
    (localtypesubexpr) cfg ∈ WaitSubExpr
    subExprInfo(cfg) = (t, env_type, ())
    ∀e' ∈ Expr: substExpr(cfg, e') = α id: e' = e_val
- .. rule:: otherwise if

    cfg_val /∈ NormalT
    (localvaluesubexpr) cfg ∈ WaitSubExpr
    subExprInfo(cfg) = (e_val, env_val, ())
    ∀e' ∈ Expr: substExpr(cfg, e') = α id: t = e'
- .. rule:: otherwise if

    `(α = $var ⇒ env.mode = $exec) ∧ cfg_val ∈ Value
    let vid = newVID(Domain_⊥(ctx.vars))
    ∧ μ = localMutability(α)
    (localinit) cfg → (μ id: vid, ctx[.vars ↦ ctx.vars[
        vid ↦ (α', e_val)
    ]])
- .. rule:: otherwise if

    t = $anytype
    let t' = getType(cfg_val)
    (localextracttype) cfg → (α id: t' = e_val, ctx)
- .. rule:: otherwise

    (localready) cfg ∈ Ready
    subCfgs(cfg) = {(e', env', ctx) ∈ {cfg_type, cfg_val} | e' ≠ $anytype}

Auxiliary rules:

- .. rule::

    (typelocal) cfg ∈ HasType
    getType(cfg) = $void
- .. rule::

    t = $anytype
    (succenvlocaluntyped) succEnv(cfg) = env

  This exists only for completeness.
  The successor configurations of a local definition without type annotation should never be used, since the type will either be extracted by (localextracttype) or the statement rewritten to a `Local variable marker`_ by (localinit).
- .. rule::

    t ≠ $anytype
    (succenvlocaltyped) succEnv(cfg) = env[
        .localInfo ↦ env.localInfo[id ↦ (localMutability(α), t)]
    ]

Local variable marker
---------------------

Variables during compilation are never notionally destroyed, as long as a reference to them exists, they can be read.

.. TODO citation

.. note:: Since we don't model pointers/slices here, the VID could actually remove the VID from `ctx.vars`, but for the sake of easier extension to a larger language, we leave them be.

Let `cfg = (μ id: vid, env, ctx) ∈ Configuration`.

- .. rule::

    env.mode = $exec
    (varmarkerdelete) cfg ∈ DeferredStmt
    cfg → ({}, ctx)
- .. rule:: otherwise if

    μ = $var
    (varmarkerconst)  cfg ∈ DeferredStmt
    cfg → ($const id: vid, ctx)
- .. rule::

    (varmarkertyped) cfg ∈ TypedValue
    subCfgs(cfg) = ∅

  Variable markers are not removed in analysis mode to preserve the available locals, in case the block includes a container expression that will only run at call-time (so it can be forwarded as an upvalue).
  In all other cases, the relevant identifier expression should be resolved to the VID during analysis.

Auxiliary rules:

- .. rule::

    (typevarmarker) cfg ∈ HasType
    getType(cfg) = $void
- .. rule::

    μ = $var
    (succenvvarmarkervar) succEnv(cfg) = env[
        .localInfo ↦ env.localInfo[id ↦ vid],
        .mutables ↦ env.mutables ∪ {vid}
    ]
- .. rule::

    μ = $const
    (succenvvarmarkerconst) succEnv(cfg) = env[
        .localInfo ↦ env.localInfo[id ↦ vid]
    ]

Coercion
--------

Let `cfg = (@as(t, e_value), env, ctx) ∈ Configuration ∧ env_type = env[.mode ↦ $exec, .coerce ↦ $type] ∧ cfg_type = (t, env_type, ctx) ∧ env_val = env[.coerce ↦ t] ∧ cfg_val = (e_val, env_val, ctx)`.

- .. rule::

    cfg_type /∈ ValueT
    (coercetypesubexpr) cfg ∈ WaitSubExpr
    subExprInfo(cfg) = (t, env_type, ())
    ∀e' ∈ Expr: substExpr(cfg, e') = @as(e', e_value)
- .. rule::

    t ∈ IntType ∧ e_value ∈ IntRange(t)
    (valueint) cfg ∈ Value
    subCfgs(cfg) = {cfg_type, cfg_value}
- .. rule:: otherwise if

    cfg_val /∈ Normal
    (coercesubexpr) cfg ∈ WaitSubExpr
    subExprInfo(cfg) = (e_value, env_val, ())
    ∀e' ∈ Expr: substExpr(cfg, e') = @as(t, e')

  Since `WaitSubExpr` would otherwise wrap the value expression into coercions ad infinitum, we only wait until `cfg_val ∈ Normal`, and not `NormalT`, despite indicating coercion in `env_val`.
- .. rule:: otherwise if

    cfg_val ∈ HasValue $/ Value
    (coercesplit) cfg → preserve(cfg, (e_value,), @as(t, getValue(cfg_value)))
- .. rule:: otherwise if

    getType(cfg_val) = t
    (coercecollapse) cfg → (e_value, ctx)
- .. rule:: otherwise if

    cfg_val ∈ Value ∧ t ∈ IntType_(+)
    ∧ extrInt(getValue(cfg_val)) /∈ IntRange(t)
    (coerceintinvalid) cfg ∈ TypeError
- .. rule:: otherwise if

    cfg_val ∈ Value ∧ t /∈ IntType_(+)
    (coercevaluenotint) cfg ∈ TypeError
- .. rule::

    cfg_val /∈ Value ∧ (t, getType(cfg_val)) /∈ Coercibility(ctx)
    (coerceinvalid) cfg ∈ TypeError
- otherwise: (coerceready) `cfg ∈ Ready, subCfgs(cfg) = {cfg_type, cfg_val}`

Typing rule:

- .. rule::

    cfg_type ∈ ValueT
    (typecoerce) cfg ∈ HasType
    getType(cfg) = t
- .. rule::

    cfg_type ∈ ValueT ∧ cfg_val ∈ NormalT ∩ HasValue
    ∧ t ∈ IntType_(+)
    ∧ i = extrInt(getValue(cfg_val)) ∈ IntRange(t)
    (valuecoerceint) cfg ∈ HasValue
    getValue(cfg) = makeInt(t, i)

Function type expression
------------------------

Let

.. mlmath::

    cfg = ($fn (exprs_params) e_return, env, ctx) ∈ Configuration
    ∧ env' = env[.coerce ↦ $type]
    ∧ cfg_return = (e_return, env', ctx)
    ∧ cfgs = {((exprs_params)_i, env', ctx) |
        0 ≤ i < |exprs_params| ∧ (exprs_params)_i ≠ $anytype
    } ∪ {cfg_return}

- .. rule::

    ParamsNotNormal = {i ∈ ℤ |
        0 ≤ i < |exprs_params| ∧ (exprs_params)_i ≠ $anytype
        ∧ ((exprs_params)_i, env', ctx) /∈ NormalT
    } ≠ ∅
    let i = min ParamsNotNormal
    (fntypeparamsubexpr) cfg ∈ WaitSubExpr
    subExprInfo(cfg) = ((exprs_params)_i, env', ((exprs_params)_k)_(k = 0)^i)
    ∀e' ∈ Expr: substExpr(cfg, e') = $fn (exprs_params[i ↦ e']) e_return
- .. rule:: otherwise if

    cfg_return /∈ NormalT
    (fntypereturnsubexpr) cfg ∈ WaitSubExpr
    subExprInfo(cfg) = (e_return, env', exprs_params)
    ∀e' ∈ Expr: substExpr(cfg, e') = $fn (exprs_params) e'
- .. rule:: otherwise if

    cfgs ∩ Value = cfgs
    (fntypevalue) cfg ∈ Value
    subCfgs(cfg) = cfgs
- .. rule:: otherwise if

    cfgs ∩ HasValue = cfgs
    (fntypesplit) cfg → preserve(cfg,
        exprs_params ++ (e_return,),
        $fn((getValue(
            (exprs_params)_k))_(k = 0)^(|exprs_params|)
        ) getValue(e_return)
    )
- .. rule:: otherwise

    (fntypetyped) cfg ∈ Ready
    subCfgs(cfg) = cfgs

Typing rule:

- .. rule::

    (typefntype) cfg ∈ HasType
    getType(cfg) = $type

Function call expression
------------------------

.. mlmath::

    SigParam = MaybeComptime × (Expr ∪ {$anytype})
.. fndef::

    paramSignature: Normal → SigParam* ∪ {⊥},
        (fid, env, ctx) ↦ (
            (fn.params_k.comptime, fn.params_k.type)
        )_(k = 0)^(|fn.params|) ∀(fid, env, ctx) ∈ Normal: (
            fid ∈ FID
            ∧ fn = ctx.functions(fid)
        ),
        cfg ↦ ((⊥, tparams_k))_(k = 0)^(|tparams|) ∀cfg ∈ Typed: $fn (tparams) e_ret = getType(cfg),
        cfg ↦ ⊥ ∀cfg ∈ Normal: ¬(
            ∃tparams ∈ (Expr ∪ {$anytype})*
            ∃e_ret:
            $fn (tparams) e_ret = getType(cfg)
        )

.. fndef::
    argEnv: Environment × SigParam → Environment,
      (env, ($comptime, t)) ↦ env[.mode ↦ $exec, .coerce ↦ t]
      ∀env ∈ Environment ∀($comptime, t) ∈ SigParam,
      (env, (⊥, t)) ↦ env[.coerce ↦ t]
      ∀env ∈ Environment ∀(⊥, t) ∈ SigParam

Let `cfg = (e_fn (exprs_args), env, ctx) ∈ Configuration ∧ cfg_fn = (e_fn, subEnv(env), ctx)`.

- .. rule::

    cfg_fn /∈ Normal
    (callfnsubexpr) cfg ∈ WaitSubExpr
    subExprInfo(cfg) = (e_fn, subEnv(env), ())
    ∀e' ∈ Expr: substExpr(cfg, e') = e' (exprs_args)
- `sig = paramSignature(cfg_fn) ≠ ⊥` in:

  - .. rule::

      |exprs_args| ≠ |sig|
      (fncallwrongnumargs) cfg ∈ TypeError
  - otherwise if

    .. mlmath::

      NotDone = {i ∈ ℤ |
          0 ≤ i < |sig| ∧ (κ, t) = sig_i ∧ (
              κ = $comptime
              ∨ t = $anytype
              ∨ ((exprs_args)_i, argEnv(env, (κ, t)), ctx) /∈ NormalT
          )
      } ≠ ∅

    Let

    .. mlmath::

        i = min NotDone
        e_arg = (exprs_args)_i
        (κ, t) = sig_i
        env_arg = argEnv(env, (κ, t))
        cfg_arg = (e_arg, env_arg, ctx)

    - .. rule::

        cfg_arg /∈ NormalT
        (callargsubexpr) cfg ∈ WaitSubExpr
        subExprInfo(cfg) = (
            e_arg, env_arg, (e_fn,) ++ (exprs_args)_(k = 0)^(i_args)
        )
        ∀e' ∈ Expr: substEnv(cfg, e') = e_fn (exprs_args[i ↦ e'])`
    - .. rule:: otherwise if

        e_fn /∈ FID
        (callgenericnotcomptime) cfg ∈ TypeError
    - .. rule:: otherwise if

        ctx.functions(e_fn) = ⊥
        (callgenericdoesntexist) cfg ∈ Corrupt
    - otherwise: let `e_spec = e_arg` if `κ = $comptime` else `e_spec = getType(cfg_arg)` in

      - .. rule::

          fid = ctx.specializationIndex(e_fn, e_spec) ≠ ⊥
          (callspecializereuse) cfg → (fid (exprs_args), ctx)
      - otherwise: let

        .. mlmath::

          fn = ctx.functions(e_fn)
          fid = newFID(Domain_⊥(ctx.functions))
          ctx' = ctx[.specializationIndex ↦ ctx.specializationIndex[
              (e_fn, e_spec) ↦ fid
          ]]

        - .. rule::

            κ = $comptime
            let vid = newVID(Domain_⊥(ctx.variables))
            (callspecializecomptimeparam) cfg → (
              fid (
                ((exprs_args)_k)_(k = 0)^i
                ++ ((exprs_args)_(k + i + 1))_(k = 0)^(|fn.params| - i - 1)
              ), ctx'[
                  .functions ↦ ctx.functions[fid ↦ fn[
                      .params ↦ (
                          (fn.params_k)_(k = 0)^i
                          ++ (fn.params_(k + i + 1))_(k = 0)^(|fn.params| - i - 1)
                      ), .comptimeArgs ↦ fn.comptimeArgs[
                          protoParams_i.name ↦ vid
                      ], .ctime ↦ time(ctx),
                      .evaluated ↦ {j ∈ fn.evaluated |
                          j ∈ ℤ ⇒ 0 ≤ j < i
                      } ∪ {j - 1 | j ∈ fn.evaluated ∩ ℤ ∧ i < j},
                      .generic ↦ ∅
                  ]],
                  .variables ↦ ctx.variables[vid ↦ e_spec]
              ])
        - .. rule:: otherwise

            (callspecializeanytype) cfg → (fid (exprs_args), ctx'[
                .functions ↦ ctx.functions[fid ↦ fn[
                    .params ↦ fn.params[i ↦ e_spec],
                    .ctime ↦ time(ctx)
                    .generic ↦ ∅
                ]]
            ])

  - .. rule:: otherwise if

      env.mode = $exec ∧ e_fn ∈ FID
      ∧ ∀e_arg ∈ exprs_args: (e_arg, subEnv(env), ctx) ∈ Value
      (call) cfg ∈ PushFrame
      frameReq(cfg) = (e_fn, env.mutables, exprs_args)
      ∀e': frameResult(cfg, e') = e'
  - .. rule:: otherwise

      (fncalltyped) cfg ∈ Ready
      subCfgs(cfg) = {((exprs_args)_i, argEnv(env, sig_i), ctx) |
          0 ≤ i < |exprs_args|
      }

- .. rule:: otherwise

    (callfnnotfunction) cfg ∈ TypeError

Typing rule:

- .. rule::

    cfg_fn ∈ HasType ∧ $fn (params) e_ret = getType(cfg_fn)
    ∧ e_ret ≠ $anytype
    (typecall) cfg ∈ HasType
    getType(cfg) = e_ret

Container expression
--------------------

Let `cfg = (cont, env, ctx) ∈ Configuration`.

- .. rule::

    ∃id ∈ FreeIdents(cont): env.localInfo(id) = ⊥
    (containerundeflocal) cfg ∈ Corrupt
- .. rule:: otherwise if

    ∃id ∈ FreeIdents(cont): env.localInfo(id) = $comptime
    (containergeneric) cfg ∈ Generic
- .. rule:: otherwise if

    ∃id ∈ FreeIdents(cont): env.localInfo(id) ∈ Mutability × Expr
    (containerrt) cfg ∈ Ready
    subCfgs(cfg) = ∅
- otherwise: let

  .. mlmath::

      key = (cont, []_(Ident →_⊥ Expr)[id ↦ e |
          id ∈ FreeIdents(cont)
          ∧ vid = env.localInfo(id) ∈ VID
          ∧ (e, ctime, mtime, l, μ) = ctx.vars(vid)
      ], env.scopeParent)

  - .. rule::

      ctx.containerIndex(key) ≠ ⊥
      (containerexists) cfg → (ctx.containerIndex(key), ctx)
  - .. rule:: otherwise

      let cid = newCID(Domain_⊥(ctx.containers))
      (containercreate) cfg → (cid, ctx[.containers ↦ ctx.containers[
          cid ↦ (time(ctx), []_(Ident →_⊥ VID)[id ↦ vid |
              id ∈ FreeIdents(cont)
              ∧ vid = env.localInfo(id) ∈ VID
          ], env.scopeParent, cont)
      ]])

Typing rule:

- .. rule::

    (typecontainer) cfg ∈ HasType
    getType(cfg) = $type

Compound literal
----------------

After the type subexpression is evaluated, the list of fields can be checked (if necessary, after evaluating the field definitions): a compound literal must not contain field not found in the type definition, a ``struct`` literal must contain all fields of the type which don't have a default value, a ``union`` literal exactly one of them.

The expression for each field is coerced to the type given in the type definition.

A compound literal only represents a value if all given fields are values.
For ``union`` literals this is the single active field.

For ``struct`` literals, all fields of the type definition must be present and in definition order for the literal to be considered a value.
If all fields contain values but this condition is not fulfilled, this is established by reordering given fields and adding default values for absent fields.

.. fndef::

    joinFields: (Ident × Expr)* × TypeBody → (Ident × Expr × Expr ∪ {$anytype})
        ((), (kind, contfields)) ↦ () ∀(kind, contfields) ∈ Type,
        (fields, (kind, contfields)) ↦ joinFields(
            (fields_k)_(k = 0)^(|fields|), (kind, contfields)
        ) ++ ((id, e, t),) ∀fields ∈ (Ident × Expr)^(+)
        ∀(kind, contfields) ∈ Type
        ∃t ∈ Expr ∃e_default ∈ Expr ∪ {⊥}:
        (id, e) = fields_(|fields| - 1)
        ∧ (id, t, e_default) ∈ contfields,
        (fields, (kind, contfields)) ↦ joinFields(
            (fields_k)_(k = 0)^(|fields|), (kind, contfields)
        ) ++ ((id, e, $anytype),) ∀fields ∈ (Ident × Expr)^(+)
        ∀(kind, contfields) ∈ Type
        ∀t ∈ Expr ∀e_default ∈ Expr ∪ {⊥}:
        (id, e) = fields_(|fields| - 1)
        ∧ (id, t, e_default) /∈ contfields

Let `fields ∈ (Ident × Expr)* ∧ cfg = (t {fields}, env, ctx) ∈ Configuration ∧ env_type = env[.mode ↦ $exec, .coerce ↦ $type]`.

- .. rule::

    (t, env_type, ctx) /∈ ValueT
    (compoundtypesubexpr) cfg ∈ WaitSubExpr
    subExprInfo(cfg) = (t, env_type, ())
    ∀e' ∈ Expr: substExpr(cfg, e') = e' {fields}
- `t ∈ CID ∧ (kind, contfields) = ctx.containers(t).type`:

  - .. rule::

      kind = $union ∧ |fields| ≠ 1
      (compoundunionfieldnum) cfg ∈ TypeError
  - .. rule:: otherwise if

      `∃(id, e_field) ∈ fields: ¬∃f ∈ contfields: f.name = id
      (compoundunknownfield) cfg ∈ TypeError
  - .. rule:: otherwise if

      ∃(id, e_ftype, e_default) ∈ contfields: (
          (e_ftype, defaultEnv, ctx) /∈ ValueOf($type)
          ∨ (e_default = ⊥ ⇒ (e_default, defaultEnv, ctx) /∈ ValueOf($type))
      )
      (compoundneedtypeinfo) cfg ∈ PushFrame
      frameReq(cfg) = cid
      ∀e' ∈ Expr: frameResult(cfg, e') = t {fields}

    This also requests the default value expressions to be evaluated, in accordance with the behavior of stage2 of the reference compiler. [#typeinfodefaults]_
  - .. rule:: otherwise if

      kind = $struct
      ∧ ∃(id, e_ftype, ⊥) ∈ contfields: ¬∃e_field: (id, e_field) ∈ fields
      (compoundstructmissingfield) cfg ∈ TypeError
  - otherwise: let

    .. mlmath::

        joined = joinFields(fields, (kind, contfields))
        cfgs = {(t, env_type, ctx)} ∪ {(e, env[.coerce ↦ t'], ctx) | (id, e, t') ∈ joined}

    - .. rule::

        NotDone = {i ∈ ℤ |
            0 ≤ i < |fields| ∧ (id, e, t') = joined_i
            ∧ (e, env[.coerce ↦ t'], ctx) /∈ NormalT
        } ≠ ∅
        let i = min NotDone
        ∧ (id, e_field, t_field) = joined_i
        ∧ env_field = env[.coerce ↦ t_field]
        (compoundfieldsubexpr) cfg ∈ WaitSubExpr
        subExprInfo(cfg) = (e_field, env_field, (fields_k)_(k = 0)^(i_field))
        ∃e': substEnv(cfg, e') = t {fields[i ↦ (id, e')]}
    - otherwise if `cfgs ∩ Value = cfgs`:

      - .. rule::

          kind = $struct ∧ 0 ≤ i < |contfields|
          ∧ (fields_i)_0 ≠ (contfields_i).name
          (compoundnormalize) cfg → (t {((id, e_value) |
              (id, e_ftype, e_default) = contfields_k
              ∧ (
                  ¬((∃e_field: (id, e_field) ∈ fields) ∧ e_field = e_default)
                  ∨ (id, e_value) ∈ fields
              )
          )_(k = 0)^(|contfields|)})
      - .. rule:: otherwise

          (compoundvalue) cfg ∈ Value
          subCfgs(cfg) = cfgs

    - .. rule:: otherwise if

        cfgs ∩ HasValue = cfgs
        (compoundsplit) cfg → preserve(
            cfg,
            (e_comp,) ++ ((fields_k)_1)_(k = 0)^(|fields|),
            e_comp {(
                fields_k[1 ↦ getValue((fields_k)_1, subEnv(env), ctx)]
            )_(k = 0)^(|fields|)})
    - .. rule:: otherwise

        (compoundtyped) cfg ∈ Ready
        subCfgs(cfg) = cfgs

- .. rule:: otherwise

    (compoundwrongtype) cfg ∈ TypeError

Typing rule:

- .. rule::

    t ∈ CID
    (typecompound) cfg ∈ HasType
    getType(cfg) = t

.. [#typeinfodefaults] see `E-Mail from Martin Wickham, 2022-03-21`_.

Anonymous compound literal
---------------------------

Let `fields ∈ (Ident × Expr)* ∧ cfg = (. lid {fields}, env, ctx) ∈ Configuration`

- .. rule::

    env.coerce ∈ CID
    (anoncompcoerce) cfg → (env.coerce {fields}, ctx)
- .. rule:: otherwise if

    NotDone = {i ∈ ℤ |
        0 ≤ i < |fields|
        ∧ ((fields_i)_1, subEnv(env), ctx) /∈ Normal
    } ≠ ∅ ∧ i = min NotDone ∧ (id, e_field) = fields_i
    (anoncompsubexpr) cfg ∈ WaitSubExpr
    subExprInfo(cfg) = (e_field, subEnv(env), (fields_k)_(k = 0)^i)
    ∀e' ∈ Expr: substExpr(cfg, e') = . lid {fields[i ↦ (id, e')]}
- .. rule:: otherwise

    let cont = (lid, []_(Ident →_⊥ Decl), ($struct, (
        ((fields_k)_0, getType((fields_k)_1, subEnv(env), ctx), ⊥)
    )_(k = 0)^(|fields|), ()))
    (anoncompcreate) cfg → (comp {fields}, ctx)

Propositions
============

Some propositions that might be interesting to prove:

- `Corrupt` is not reachable for 'sane' configurations
- function calls using arguments with appropriate names and types will not reach a `TypeError` configuration.
- `Value ∪ Exit` classification is independent of environment (but not necessarily heap)
- a `Normal` configuration will not become not-`Normal` by making a variable mutable
- full evaluation never leads to a `Typed` configuration (for expressions)
- (valid) changes to the heap never invalidate `EvalStopped` classification
- purity of member evaluation
- `∀cfg, cfg' ∈ Configuration: cfg ∈ HasType ∧ cfg → cfg' ⇒ cfg' ∈ HasType ∧ getType(cfg') ∈ {getType(cfg), $noreturn}`
- `∀cfg ∈ Configuration: cfg ∈ Typed ⇒ getType(cfg) ≠ $comptime_int`
- `Normal ∪ Location ⊂ HasType`
- `(e, env, ctx) ∈ HasType ⇒ ∀t ∈ AExpr: (e, env[.coerce ↦ t], ctx) ∈ HasType ∧ getType(e, env, ctx) = getType(e, env[.coerce ↦ t], ctx)` (used by (coercecollapse) and others)
- for a well-typed expression, if the used variables are allocated and assigned with an appropriate value, no type-related error can occur
- `cfg ∈ Value ⇒ getType(cfg) ≠ $noreturn`
- `∀cfg ∈ Configuration: cfg.env.mode ≠ $exec: cfg /∈ Exit`
- `∀cfg ∈ Configuration: (cfg.expr /∈ Expr ⇒ cfg /∈ Location) ∧ (cfg.expr /∈ Statement ⇒ cfg /∈ DeferredStmt)`
- `∀cfg, cfg' ∈ Configuration: cfg → cfg' ∧ cfg'.expr ∈ Statement ⇒ cfg.expr ∈ Statement`
- `∀(pat, env, ctx) ∈ Configuration: pat ∈ Pattern ⇒ succEnv(pat, env, ctx) = env`
- `∀cfg = (_, _, ctx) ∈ HasValue: getType(cfg) = getType(getValue(cfg), defaultEnv, ctx)`
- `∀cfg ∈ Value: getValue(cfg) = cfg.expr`

Activation stack ruleset
========================

.. fndef::

    fillOutField: Field × Value → Field,
        ((id, e_type, ⊥), (e, env, ctx)) ↦ (id, e, ⊥) ∀(id, e_type, ⊥) ∈ Field ∀(e, env, ctx) ∈ Value,
        ((id, e_type, e_default), (e, env, ctx)) ↦ (id, getType(e, env, ctx), e) ∀(id, e_type, e_default) ∈ Field ∀(e, env, ctx) ∈ Value: e_default ≠ ⊥

.. fndef::

    RuntimeReferences: Context × PowF(FID ∪ VID) → PowF(FID ∪ VID),
        (ctx, S) ↦ (∪_(fid ∈ S ∩ Domain_⊥(ctx.functions))
            (∪_((id, e_type) ∈ ctx.functions(fid).params)
                SubTree(e_type)
                ∪ SubTree(ctx.functions(fid).retType)
            ) ∪ (∪_(fid ∈ S ∩ Domain_⊥(ctx.functions): $runtime ∈ ctx.functions(fid).evaluated)
                SubTree(ctx.functions(fid).runtimeBody)
                ∪ ∪_(vid ∈ S ∩ Domain_⊥(ctx.vars)) SubTree(ctx.vars(vid)_1)
            )
        ) ∩ (FID ∪ VID)

Root environment helpers
------------------------

.. fndef::

    CFIDs: Context → PowF(FID ∪ CID),
        ctx ↦ Domain_⊥(ctx.containers) ∪ Domain_⊥(ctx.functions) ∀ctx ∈ Context

.. _base-env:

`baseEnv` collects creation times, specialization variable information and upvalues for the given scope (the third argument exists for well-definedness reasons; it is used to detect cycles in the scope chain which are forbidden by invariants):

.. fndef::

    baseEnv: (FID ∪ CID ∪ {⊥}) × Context × PowF(FID ∪ CID) → Environment,
        (⊥, ctx, S) ↦ defaultEnv ∀ctx ∈ Context ∀S ⊂= CFIDs(ctx),
        (fid, ctx, S) ↦ env[
          .checkMTime ↦ env.checkMTime ∪ {fn.ctime},
          .localInfo ↦ env.localInfo[
            id ↦ fn.comptimeArgs | id ∈ Domain_⊥(fn.comptimeArgs)
          ][
            id ↦ $comptime | (κ id: t) ∈ fn.params ∧ (κ = $comptime ∨ t = $anytype)
          ]
        ] ∀ctx ∈ Context ∀S ⊂= CFIDs(ctx) ∀fid ∈ FID ∩ (CFIDs(ctx) $/ S): fn = ctx.functions(fid) ≠ ⊥ ∧ env = baseEnv(fn.parent, ctx),
        (cid, ctx, S) ↦ env[
          .scopeParent ↦ cid,
          .checkMTime ↦ env.checkMTime ∪ {cont.creationTime},
          .localInfo ↦ env.localInfo[
            id ↦ cid | id ∈ Domain_⊥(cont.members)
          ][
            id ↦ ⊥ | id ∈ Domain_⊥(env.localInfo) ∩ Domain_⊥(cont.members)
          ][
            id ↦ cont.upvalues(id) | id ∈ Domain_⊥(cont.upvalues)
          ]
        ] ∀ctx ∈ Context ∀S ⊂= CFIDs(ctx) ∀cid ∈ CID ∩ (CFIDs(ctx) $/ S): cont = ctx.containers(cid) ≠ ⊥ ∧ env = baseEnv(cont.parent, ctx),
        (xid, ctx, S) ↦ defaultEnv ∀ctx ∈ Context ∀S ∈ PowF(CID ∪ FID) $/ PowF(CFIDs(ctx)) ∀xid ∈ CID ∪ FID,
        (xid, ctx, S) ↦ defaultEnv ∀ctx ∈ Context ∀S ⊂= CFIDs(ctx) ∀xid ∈ S,
        (fid, ctx) ↦ defaultEnv ∀ctx ∈ Context ∀S ⊂= CFIDs(ctx) ∀fid ∈ (FID $/ Domain_⊥(ctx.functions)) ∩ (CFIDs(ctx) $/ S)
        (cid, ctx) ↦ defaultEnv ∀ctx ∈ Context ∀S ⊂= CFIDs(ctx) ∀cid ∈ (CID $/ Domain_⊥(ctx.containers)) ∩ (CFIDs(ctx) $/ S) ∀cid ∈ CID

`fnEnv'` and `fnEnv` add parameter information to the environment produced by `baseEnv`:

.. fndef::

    fnEnv': Function × Context × ℕ_0 → Environment,
        (fid, ctx, 0) ↦ baseEnv(fid, ctx, ∅) ∀ctx ∈ Context ∀fid ∈ FID,
        (fn, ctx, n) ↦ env[.localInfo ↦ env.localInfo[
            fn.params_n.name ↦ ($const, fn.params_n.type)
        ]] ∀ctx ∈ Context ∀fid ∈ FID ∀n ∈ ℕ_(+):
            fn = ctx.functions(fid) ≠ ⊥
            ∧ n < |fn.params|
            ∧ env = fnEnv'(fn, ctx, n - 1)
            ∧ fn.params_n.type ≠ $anytype,
        (fn, ctx, n) ↦ env[.localInfo ↦ env.localInfo[
            fn.params_n.name ↦ ($const, fn.params_n.type)
        ]] ∀ctx ∈ Context ∀fid ∈ FID ∀n ∈ ℕ_(+):
            fn = ctx.functions(fid) ≠ ⊥
            ∧ n < |fn.params|
            ∧ env = fnEnv'(fn, ctx, n - 1)
            ∧ fn.params_n.type ≠ $anytype
            ∧ n ∈ fn.evaluated,
        (fn, ctx, n) ↦ env[.localInfo ↦ env.localInfo[
            fn.params_n.name ↦ $comptime
        ]] ∀ctx ∈ Context ∀fid ∈ FID ∀n ∈ ℕ_(+):
            fn = ctx.functions(fid) ≠ ⊥
            ∧ n < |fn.params|
            ∧ env = fnEnv'(fn, ctx, n - 1)
            ∧ (fn.params_n.type ≠ $anytype ⇒ n ∈ fn.evaluated),
        (fid, ctx, n) ↦ defaultEnv ∀ctx ∈ Context ∀fid ∈ FID ∀n ∈ ℕ_(+):
            fn = ctx.functions(fid) ≠ ⊥ ⇒ n ≥ |fn.params|

.. fndef::

    fnEnv: FID × Context → Environment,
        (fid, ctx) ↦ fnEnv'(fn, |fn.params|) ∀fid ∈ FID ∀ctx ∈ Context: fn = ctx.functions(fid) ≠ ⊥,
        (fid, ctx) ↦ defaultEnv ∀fid ∈ FID ∀ctx ∈ Context: ctx.functions(fid) = ⊥

`rootEnv` is used to compute the environment applied to the root expression for a given stack frame:

.. fndef::

    rootEnv: FrameHeader × Context → Environment,
        (h, ctx) ↦ baseEnv(cid, ctx, ∅) ∀h ∈ MemberVarHeader ∪ FieldHeader ∀ctx ∈ Context: ((cid, _) = h ∈ MemberVarHeader ∨ (cid, _) = h ∈ FieldHeader) ∧ ctx.containers(cid) ≠ ⊥,
        ((fid, i_param), ctx) ↦ fnEnv'(fid, ctx, i_param) ∀(fid, i_param) ∈ FnParamHeader ∀ctx ∈ Context,
        (fid, ctx) ↦ fnEnv(fid, ctx) ∀fid ∈ FnRetTypeHeader ∀ctx ∈ Context,
        ((fid, mode), ctx) ↦ fnEnv(fid, ctx)[.mode ↦ mode] ∀(fid, mode) ∈ FnBodyHeader,
        ((args, mutables, mtime, fid), ctx) ↦ env[
            .localInfo ↦ env.localInfo[id ↦ args(id) |
                id ∈ Domain_⊥(args)
            ],
            .mutables ↦ mutables,
            .checkMTime ↦ mtime
        ] ∀(args, mutables, mtime, fid) ∈ FnCallHeader ∀ctx ∈ Context: env = baseEnv(fid, ctx, ∅),
        ((cid, id), ctx) ↦ defaultEnv ∀(cid, id) ∈ MemberVarHeader ∪ FieldHeader ∀ctx ∈ Context: ((cid, id) = h ∈ MemberVarHeader ∨ (cid, i_field) = h ∈ FieldHeader) ∧ ctx.containers(cid) = ⊥,
        ((fid, i_param), ctx) ↦ defaultEnv ∀(fid, i_param) ∈ FnParamHeader ∀ctx ∈ Context: ctx.functions(fid) = ⊥

Initial state
-------------

Let `cid_root ∈ CID` arbitrary, `cont_root ∈ Container` be the abstracted form of the root container definition.
The evaluation state `(stack, ctx, exportedSyms, members) ∈ CompilerState` is initially set as follows:

- `stack := ()`
- .. mlmath::

    ctx := (
        []_(VID →_⊥ VarState),
        []_(CID →_⊥ AContainer)[
            cid_root ↦ (0, []_(Ident →_⊥ Decl), ⊥, cont_root)
        ],
        []_(FID →_⊥ Function),
        []_(Container × (Ident →_⊥ Expr) × CID →_⊥ CID),
        []_(FID × Expr →_⊥ FID)
    )
- `exportedSyms := []_(Ident →_⊥ VID ∪ FID)[x ↦ cid_root | x ∈ rootContainer.exported]`
- `members := []_(CID × Ident →_⊥ VID ∪ FID)`

Top level
---------

Given `ctx ∈ Context ∧ S ∈ FID ∪ VID`, `RuntimeReachable(ctx, S)` is the least (by set inclusion) fixpoint of `f: PowF(FID ∪ VID) → PowF(FID ∪ VID), S' ↦ RuntimeReferences(ctx, S')`, starting at `S`.

Let `st = (stack, ctx, exportedSyms, members) ∈ CompilerState`.

- `stack = ()`:

  - .. rule::

      MembersNotVisited = {(cid, id) |
          cid ∈ Domain_⊥(ctx.containers)
          ∧ id ∈ Domain_⊥(ctx.containers(cid).members)
          ∧ exportStatus(ctx.containers(cid).members(id)) = $export
          ∧ members(cid, id) = ⊥
      } ≠ ∅
      (toplevelrootmember) st ∈ CreateFrame stateFrameReq(st) = (cid, id)

    Pick any `(cid, id) ∈ MembersNotVisited`.
  - .. rule:: otherwise if

      FnNotTyped = {
          fid ∈ Domain_⊥(ctx.functions)
          ∩ RuntimeReachable(ctx, {exportedSyms(xid) |
              id ∈ Domain_⊥(exportedSyms)
          }) | $runtime /∈ ctx.functions(fid).evaluated
      } ≠ ∅
      let fn = ctx.functions(fid)
      (toplevelrootfunction) nextState(st) = st[
          .stack ↦ (((fid, $runtime), @as(fn.retType, fn.runtimeBody)),)
      ]

    Pick any `fid ∈ FnNotTyped`.
  - otherwise: (toplevelfinish) comptime evaluation finished; further build steps may use the contents of `ctx.vars`, `ctx.functions` (except comptime function bodies), the type info in `ctx.containers`, and `exportedSyms` to generate an object file or similar.

- .. rule::

    (∃i, j ∈ ℤ:
        0 ≤ i < j < |stack|
        ∧ depKey((stack_i)_0) = depKey((stack_j)_0) ≠ ⊥
    )
    (topleveldepcycle) st ∈ CompilationFailed
- .. mlmath::

    |stack| > 0 ∧ (∀i, j ∈ ℤ:
        0 ≤ i < j < |stack|
        ∧ key = depKey((stack_i)_0) ≠ ⊥
        ⇒ key ≠ depKey((stack_j)_0)
    )

  Let

  .. mlmath::

      (header, e) = stack_0
      cfg = (e, rootEnv(header, ctx), ctx)
      stack' = (stack_(k + 1))_(k = 0)^(|stack| - 1)

  - .. rule::

      cfg → (e', ctx')
      (toplevelstep) nextState(st) = st[
          .stack ↦ stack[0 ↦ (header, e')],
          .context ↦ ctx'
      ]
  - .. rule::

      cfg ∈ Corrupt
      (toplevelconfigcorrupt) st ∈ StateCorrupt
  - `cfg ∈ PushFrame`:

    - .. rule::

        frameReq(cfg) ∈ MemberReq
        ∧ x = members(frameReq(cfg)) ≠ ⊥
        (toplevelmemberlookup) nextState(st) = st[
            .stack ↦ stack[0 ↦ (header, frameResult(cfg, x))]
        ]
    - .. rule:: otherwise

        (toplevelpushframe) st ∈ CreateFrame
        stateFrameReq(st) = frameReq(cfg)

  - `cfg ∈ Generic`:

    - .. rule::

        (fid, i) = header ∈ FnParamHeader
        ∧ fn = ctx.functions(fid) ≠ ⊥
        ∧ i < |fn.params|
        (toplevelgenericparam) nextState(st) = st[
            .stack ↦ stack',
            .context.functions ↦ ctx.functions[fid ↦ fn[
                .generic ↦ fn.generic ∪ {i}
            ]]
        ]
    - .. rule::

        fid = header ∈ FnRetTypeHeader
        ∧ fn = ctx.functions(fid) ≠ ⊥
        (toplevelgenericreturn) nextState(st) = st[
            .stack ↦ stack',
            .context.functions ↦ ctx.functions[fid ↦ fn[
                .generic ↦ fn.generic ∪ {$return}
            ]]
        ]
    - .. rule:: otherwise

        (toplevelgenericnotproto) cfg ∈ StateCorrupt

  - `cfg ∈ Normal`:

    - `(cid, id) = header ∈ MemberVarHeader ∧ cfg ∈ Value`:

        - .. rule:: otherwise if

            ctx.containers(cid) ≠ ⊥
            ⇒ ctx.containers.(cid).members(id) = ⊥
            (toplevelmemberdoesnotexist) st ∈ StateCorrupt
        - otherwise: let `decl = ctx.containers(cid).members(id)` in

          - .. rule::

              ($var: t = e') = decl
              let vid = newVID(Domain_⊥(ctx.vars))
              (toplevelcreatevarmember) st ∈ FrameReturn
              stateReturn(st) = (vid, st[
                  .context.vars ↦ ctx.vars[vid ↦ ($var, e)],
                  .members ↦ members[(cid, id) ↦ vid]
              ])
          - .. rule::

              ($const: t = e') = decl
              let vid = newVID(Domain_⊥(ctx.vars))
              ∧ i_time = ctx.containers(cid).creationTime
              (toplevelcreateconstmember) st ∈ FrameReturn
              stateReturn(st) = (vid, st[
                  .context.vars ↦ ctx.vars[vid ↦ ($const, e, i_time, i_time, $locked)],
                  .members ↦ members[(cid, id) ↦ vid]
              ])
          - `($fn (params) e_ret {stmts}) = decl`:

            - .. rule::

                e /∈ FID
                (toplevelfnnotfid) st ∈ StateCorrupt
            - .. rule:: otherwise

                (toplevelcreatefnmember) st ∈ FrameReturn
                stateReturn(st) = (e, st[.members ↦ members[
                    (cid, id) ↦ e
                ]])

    - `(cid, i) = header ∈ FieldHeader ∧ cfg ∈ Value`:

      - .. rule::

          ctx.containers(cid) = ⊥
          (topleveltypedoesnotexist) st ∈ StateCorrupt
      - otherwise: let `(kind, fields) = ctx.containers(cid).type` in

        - .. rule::

            |fields| ≤ i
            (toplevelfieldoutofbounds) st ∈ StateCorrupt
        - otherwise: let `field = fillOutField(fields_i, cfg) ∧ fields' = fields[i ↦ field]` in

          - .. rule::

              getType(field.type, defaultEnv, ctx) ≠ $type
              (toplevelfieldnottype) st ∈ CompilationFailed
          - .. rule:: otherwise if

              kind = $struct ∧ field.type = $noreturn
              (toplevelstructempty) st ∈ CompilationFailed
          - .. rule:: otherwise if

              kind = $union ∧ i = |fields| - 1
              ∧ ∀f ∈ fields': f.type = $noreturn
              (toplevelunionempty) st ∈ CompilationFailed
          - .. rule:: otherwise

              (toplevelupdatefield) nextState(st) = st[
                  .stack ↦ stack',
                  .context.containers ↦ ctx.containers[
                      cid ↦ ctx.containers(cid)[
                          .type ↦ (kind, fields')
                      ]
                  ]
              ]

    - .. rule::

        header ∈ FnParamHeader ∪ FnRetTypeHeader ∪ FnBodyHeader
        ∧ ctx.functions(headerFID(header)) = ⊥
        (toplevelfunctiondoesnotexist) st ∈ StateCorrupt
    - `(fid, i) = header ∈ FnParamHeader ∧ cfg ∈ ValueOf($type) ∧ fn = ctx.functions(fid) ≠ ⊥`:

      - .. rule::

          |fn.params| ≤ i
          (toplevelparamoutofbounds) cfg ∈ StateCorrupt
      - .. rule:: otherwise

          (toplevelupdateparam) nextState(st) = st[
              .stack ↦ stack',
              .context.functions ↦ ctx.functions[fid ↦ fn[
                  .params ↦ fn.params[i ↦ fn.params_i[1 ↦ e]],
                  .evaluated ↦ fn.evaluated ∪ {i}
              ]]
          ]

    - .. rule::

        fid = header ∈ FnRetTypeHeader
        ∧ cfg ∈ ValueOf($type)
        ∧ fn = ctx.functions(fid) ≠ ⊥
        (toplevelupdaterettype) nextState(st) = st[
            .stack ↦ stack',
            .context.functions ↦ ctx.functions[fid ↦ fn[
                .retType ↦ e,
                .evaluated ↦ fn.evaluated ∪ {$return}
            ]]
        ]
    - `(fid, mode) = header ∈ FnBodyHeader ∧ fn = ctx.functions(fid) ≠ ⊥ ∧ getType(cfg) = fn.retType`:

      - .. rule::

          mode = $comptime
          (toplevelupdatecomptimebody) nextState(st) = st[
              .stack ↦ stack',
              .context.functions ↦ ctx.functions[fid ↦ fn[
                  .comptimeBody ↦ e,
                  .evaluated ↦ fn.evaluated ∪ {$comptime}
              ]]
          ]
      - .. rule::

          mode = $runtime
          (toplevelupdateruntimebody) nextState(st) = st[
              .stack ↦ stack',
              .context.functions ↦ ctx.functions[fid ↦ fn[
                  .runtimeBody ↦ e,
                  .evaluated ↦ fn.evaluated ∪ {$runtime}
              ]]
          ]

    - .. rule::

        header ∈ FnCallHeader ∧ cfg ∈ Value
        (toplevelcallreturn) st ∈ FrameReturn
        stateReturn(st) = (e, st)
    - .. rule:: otherwise

        (toplevelunexpectedresult) st ∈ CompilationFailed

Frame creation
--------------

_`createArgs` adds variables for the arguments to the context:

.. fndef::

    createArgs: Context × Parameters × Expr* → Context × (Ident →_⊥ VID),
        (ctx, params, args) ↦ (ctx, []_(Ident →_⊥ VID)) ∀ctx ∈ Context ∀params ∈ Parameters ∀args ∈ Expr*: |params| = 0 ∨ |args| = 0,
        (ctx, params, args) ↦ (
            ctx'[.vars ↦ ctx.vars[vid ↦ args_n]],
            f[id ↦ vid]
        ) ∀ctx ∈ Context ∀params ∈ Parameters ∀args ∈ Expr*: n = min {|args|, |params|} - 1 ≥ 0 ∧ (ctx', f) = createArgs(ctx, (params_k)_(k = 0)^n, (args_k)_(k = 0)^n) ∧ (id, t) = params_n ∧ vid = newVID(Domain_⊥(ctx'.vars))

For `st = (stack, ctx, exportedSyms, members) ∈ CreateFrame`, the following rules are used:

- `(cid, id) = stateFrameReq(st) ∈ MemberReq ∧ cont = ctx.containers(cid) ≠ ⊥`:

  - .. rule::

      cont.members(id) = ⊥
      (pushmemberdoesnotexist) st ∈ CompilationFailed

    .. note:: this is not `StateCorrupt` because this is the canonical rule to reject qualified references to undefined members.
  - `decl = cont.members(id) ≠ ⊥`:

    - `(η μ: e_type = e_val) = decl`:

      - .. rule::

          e_type = ⊥
          (pushmemberunannotated) nextState(st) = st[
              .stack ↦ (((cid, id), e_val),) ++ stack
          ]`
      - .. rule::

          e_type ≠ ⊥
          (pushmemberannotated) nextState(st) = st[
              .stack ↦ (((cid, id), @as(e_type, e_val)),) ++ stack
          ]

    - .. rule::

        η $fn(params) e_ret {stmts} = decl
        let fid = newFID(Domain_⊥(ctx.functions))
        (pushfid) nextState(st) = st[
            .stack ↦ (((cid, id), fid),) ++ stack,
            .context.functions ↦ ctx.functions[
                fid ↦ (params, e_ret, lab_return: {stmts}, lab_return: {stmts}, time(ctx), cid, ∅)
            ]
        ]`

- `(fid, mutables, mtime, args) = stateFrameReq(st) ∈ CallReq ∧ fn = ctx.functions(fid) ≠ ⊥ ∧ |fn.params| = |args|`:

  - .. rule::

      $comptime /∈ fn.evaluated
      (pushanalyzecomptime) nextState(st) = st[
          .stack ↦ ((
              (fid, $comptime),
              @as(fn.retType, fn.comptimeBody)
          ),) ++ stack
      ]
  - .. rule:: otherwise

      let (ctx', argmap) = createArgs(ctx, fn.params, args)
      (pushcall) nextState(st) = st[
          .stack ↦ ((
              (argmap, mutables, mtime, fid),
              fn.comptimeBody
          ),) ++ stack,
          .context ↦ ctx'
      ]

- .. mlmath::

    cid = header ∈ TypeReq
    ∧ cont = ctx.containers(cid) ≠ ⊥
    ∧ NotDone = {i ∈ ℤ |
        0 ≤ i < |cont.type.fields|
        ∧ (id, e_type, e_default) = cont.type.fields_i
        ∧ (
            (e_type, defaultEnv, ctx) ∈ ValueOf(type)
            ⇒ e_default ≠ ⊥
            ∧ (e_default, defaultEnv, ctx) /∈ ValueOf(e_type)
        )
    } ≠ ∅

  Let `i = min NotDone ∧ (id, e_type, e_default) = cont.type.fields_i` in

  - .. rule::

      e_default ≠ ⊥
      (pushdefaultvalue) nextState(st) = st[
          .stack ↦ (((cid, i), @as(e_type, e_default)),) ++ stack
      ]
  - .. rule::

      e_default = ⊥
      (pushfieldtype) nextState(st) = st[
          .stack ↦ (((cid, i), e_type),) ++ stack
      ]

- `fid = header ∈ PrototypeReq ∧ fn = ctx.functions(fid) ≠ ⊥`:

  - .. rule::

      NotDone = {i ∈ ℤ $/ fn.evaluated |
          0 ≤ i < |fn.params|
      } ≠ ∅
      let i = min NotDone
      (pushparam) nextState(st) = st[
          .stack ↦ (((fid, i), (fn.params_i)_1),) ++ stack
      ]
  - .. rule:: otherwise if

      $return /∈ fn.evaluated
      (pushrettype) nextState(st) = st[
          .stack ↦ ((fid, fn.retType),) ++ stack
      ]
  - .. rule:: otherwise

      (pushprototypedone) st ∈ StateCorrupt

- .. rule:: otherwise

    (pushinvalid) st ∈ StateCorrupt


Frame return
------------

For `st ∈ FrameReturn ∧ (e, st') = stateReturn(st) ∧ (stack, ctx) = st'`, the following rules are applied:

- .. rule::

    |stack| = 1
    (returndiscard) nextState(st) = st'[.stack ↦ ()]
- .. rule::

    |stack| > 1 ∧ (header', e') = stack_1
    ∧ cfg' = (e', rootEnv(header', ctx), ctx) ∈ PushFrame
    (returnresult) nextState(st) = st'[
        .stack ↦ stack'[0 ↦ (header', frameResult(cfg'))]
    ]
- .. rule:: otherwise

    (returninvalid) st ∈ StateCorrupt

Further work
============

Many aspects of the language are not modeled in this document, and may be defined in the future:

- arrays
- slices
- pointers
- enums
- labeled unions
- error values, sets and unions
- vectors
- tuples
- floating-point numbers
- all builtins requiring any of the above type categories in its interface
- comptime blocks in container definitions
- extern type definitions
- packed type definitions
- runtime-only features:
  Runtime execution is an aspect of the language that is not defined in this document.

  In addition, the following runtime-related features are not modeled, not even in terms of type analysis:

  - extern members
  - coroutines
  - inline assembly
  - atomics

- method call syntax
- inline and tail calls
- backward branch limit

In particular, the addition of pointers will make the model significantly more complex.
It was however attempted to anticipate many of the requirements of pointers.
For instance, the indirection of variable access via VIDs stems from this.

This model deals only with compile-time behavior.
A full model of the language needs to define runtime semantics as well.

Many details of the language need further discussion at the time of writing, and the model may need to be updated to reflect the decisions.
For instance, the resolution of language primitives may change from a lexical distinction to primitives being members of an always-in-scope container. [#primitiveres]_

More proposals for the language could be illustrated by implementing them in the model:

- Circular reference errors could be resolved in the cases when the definition has a type annotation and the use only needs the address of the variable.
- Empty user-defined types could be allowed, to be treated the same way as ``noreturn``

Finally, it might be possible to simplify the model.
A possible avenue for this is integrating the activation stack into the AST by way of syntax constructs that "reset" the environment in similar ways as stack frames in the model as described here.

.. [#primitiveres] see `E-Mail from Martin Wickham, 2022-02-08`_

References
==========

.. [issue1470] Andrew Kelley and Martin Wickham, Issue #1470: compile error for modification of comptime data inside runtime block, https://github.com/ziglang/zig/issues/1470 (retrieved 2022-05-14).

.. [Burstall69] R.M. Burstall, Proving properties of programs by structural induction, The Computer Journal 12 (1) (1969) 41–48.

.. [SOS81] G.D. Plotkin, A structural approach to operational semantics, DAIMI FN-19, Computer Science Department, Aarhus University, 1981.

.. [SOS04] G.D. Plotkin, A structural approach to operational semantics,  Journal of Logic and Algebraic Programming 60–61 (2004) 17–139 (doi:10.1016/j.jlap.2004.03.002).

.. [SOSHistory] G.D. Plotkin, The origins of structural operational semantics, Journal of Logic and Algebraic Programming 60–61 (2004) 3–15 (doi:10.1016/j.jlap.2004.03.009)

.. [zigdocs] Zig Software Foundation, Zig Language Reference, version 0.9.1, https://ziglang.org/documentation/0.9.1/ (retrieved 2022-03-01).

.. [ZigRelN0.9] Zig Software Foundation, 0.9.0 Release Notes, https://ziglang.org/download/0.9.0/release-notes.html (retrieved 2022-05-21)

TODO display external refercences here by editing the generated LaTeX (corresponding automatic feature currently unimplemented in docutils, they display at the bottom of the page, like footnotes)

.. raw:: latex

    \appendix

Expert communications
===============================

These are excerpts from E-Mails by Martin Wickham (also known as SpexGuy), a contributor to the Zig project working toward creating a language specification, made available under Creative Commons Attribution 4.0 (CC-BY-4.0) license. [#cc-by-4.0]_
The original E-Mails were in plain text, any formatting was added for the sake of readability.

.. [#cc-by-4.0] see https://creativecommons.org/licenses/by/4.0/ for details on the license.


E-Mail from Martin Wickham, 2021-09-21
--------------------------------------

        - Comptime function call results are memoized based on their arguments.

    Hmm, this seems like a hard problem indeed. I suppose defining it with the "pointer to mutable" rule should be good enough for my semantics definition.
    Do you have a link to the details, or is the truth in the source for now?

I suppose the current truth is in the source, but the long term plan has yet to be fully figured out.
Note that "mutable" in this case refers not to the type of the pointer, but to the actual memory it points to.
It's legal to use ptrToInt + intToPtr to remove the ``const`` qualifier from a pointer and then write through it.
And if you do that to a mutable memory region, that's well defined, even at comptime.
So you can't trust the parameter type, you have to actually look at the memory being passed.
This causes some really weird problems for std.fmt.format.
If you construct a format string at comptime, a lot of rules come together to say that no memoization happens, and this can cause infinite recursion.
Copying the string into a const though, or even moving the code that calculates the string into a function, will fix the problem.
Which is confusing AF for anybody learning the language.
So IMO we need better rules.  zig.godbolt.org can be a good resource if you want to run some tests to see how it behaves.

In terms of your rules, I think just "pointer or struct containing pointer" is good enough.
Although it can get even weirder with defined representation, ``[8]u8`` or ``usize`` could technically be a pointer depending on what comptime code ran before it.

I've been putting a lot of energy lately into an alternative idea where we delete the memoization from the spec and instead have a deduplication system, where struct definitions are found to be equal in certain cases.
In practice the current memoization system handles most of these cases correctly, so we would keep it, but sometimes a little extra work is needed.
There are some thorny vines down that road though.  I'm happy to discuss it more if you're interested, but I'd currently give it only a ~40% chance of making it into the language.
Another approach is to make ``[_]T`` an actual comptime-only type, so that you can pass a string by value.
That one's also thorny, for very different reasons :P

E-Mail from Martin Wickham, 2021-10-14
--------------------------------------

A partial solution has been described in https://github.com/ziglang/zig/issues/7396, but the actual plan we are working with now is slightly different.

The first step (which has already been implemented in master) is to disallow closing over ``var`` names.  Attempting to compile this on master
gives::

    <source>:21:24: error: mutable 'x' not accessible from here
                return gcd(x, b);
                          ^
    <source>:17:9: declared mutable here
        var x = a / 2;
            ^
    <source>:18:17: note: crosses namespace boundary here
        const res = struct {
                  ^

However, this doesn't fix the problem.  You could still make a const pointer to x, and close over that: https://zig.godbolt.org/z/3xj9nTWhq
This exhibits exactly the same behavior you found, and using uniq fixes it: https://zig.godbolt.org/z/7PM8xsEvs

Our stance is that this should be a compile error.  What we would like to say is that closures are only allowed if a closed-over value doesn't change.  However, a simple analysis of closures would yield too many false positives.  For example::

    comptime var y: u32 = 0;
    const py = &y;
    const T = struct {
      fn foo(comptime b: bool) void {
        if (b) bar(py.*);
      }
    };
    y += 1;

If ``b`` is false, there should be no compile error because ``foo`` does not actually close over ``py.*``.  Similarly, if ``foo`` is never used, there is no problem here.  Only if ``foo`` is used *and* ``b`` is ``true`` should this be a compile error.  Here's how we plan to implement this:

- When a type literal is analyzed, any potential closures are saved along with some extra state.  This extra state includes modification counts for any mutable regions that are closed over, for their entire reference graphs.
- When a function is analyzed, any time it reads memory through a closed over variable or derived pointer, it checks the modification count.  If the modification count doesn't match the count when the var was closed over, it's a compile error (variable value was modified after closure).  It then marks the region as "const due to closure".  Writing to memory through a closed over pointer is never allowed.
- When analyzing, if a write occurs to a value marked as "const due to closure", it is a compile error (variable value was modified after closure).

This catches two potential invalid orderings:

- the closure is created and executed before the var is modified (violates caching guarantees, like in your example)
- the var is modified after the closure is created but before it is executed (unexpected side effects)

So that's the current plan.

E-Mail from Martin Wickham, 2022-02-08
--------------------------------------

    So your plan is to have the primitives be members of some container that is in-scope by default and can be referred to explicitly by `@lang()`, and then normal member resolution rules introduced in 0.9 apply (shadowing definitions are allowed, but in that scope the unqualified form cannot be used to refer to either)?
    I can't really complain about that.

Yep pretty much!
I don't know if there's an actual proposal for this yet but it's in the plan in theory.

E-Mail from Martin Wickham, 2022-02-09
--------------------------------------

    I very much hope so, because extracting the value *while* preserving any runtime code in the block sounds like a nightmare to model, especially considering things like the fact that the runtime code might also contain another break targeting the block.

Unfortunately, that is indeed how it works.
The specific rule is that if a block has exactly one reachable break, and the value that it breaks is comptime known, it is transformed like this::

    // before
    if (block: {
        do_stuff(); // runtime stuff that does not break
        comptime var x = true;
        // unconditional break (because x is comptime known)
        if (x) break :block false;
        // can be more stuff that is unreachable, including dead break statements
        break :block true;
    }) 0/0;

    // after
    do_stuff(); // runtime stuff that does not break
    if (false) 0/0;

Any work in the block is moved to before the block, and the block itself is replaced with the comptime known result.  The transformed version may need scoping rules that are no longer a strict tree to be properly represented, but inside the compiler that isn't an issue because names have already been resolved.

    I am concerned about the idea that all function parameters should be considered comptime for function calls during compilation.
    That would imply that functions called during compilation are not analysed before they are executed and are in fact dynamically typed, just like member value expressions.
    I meant typed once for every used valuation for the parameters marked as comptime.

Unfortunately this is the case.  The compiler performs type analysis and comptime value resolution in one pass by executing an untyped IR (ZIR), not by typing the IR and then executing typed IR.
While we could in theory type the IR first and then execute it, it would still be dynamically typed because things like pointers to globals are symbolic values at compile time.
That and similar "lazy values" are not represented in the static
type system of Zig, so knowing that a type is statically an integer doesn't necessarily give you an advantage because it could still be a symbolic constant or lazy value whose concrete value is unknown.

    I thought this was one of the most interesting parts I thought I knew about the language.
    Reading that this was actually a misunderstanding gives me … complicated feelings :(

I would be interested to hear more details about why you think this direction is interesting.
I can't say for certain that the status quo is better, but I also don't really see the tradeoffs involved.  This might be something that could change if the alternative has concrete benefits.

    I've currently modeled the ``defer`` statement to be typed in lexical order w. r. t. statements surrounding it. This mostly matters for side-effects on ``comptime var``\s.
    Is this how it should work or should the comptime side-effects also be deferred?
    (or maybe comptime side-effects should be disallowed in defers in the first place, just like #1470 will forbid them in runtime branches?)

Hmm, so the current behavior is that the side effects are applied each time the defer is instantiated, so at each ``return`` or ``break`` out of the block containing the defer.
But the planned behavior is for ``defer`` to be analyzed once at AST position, like you have implemented.
It should definitely be considered a "runtime branch" under #1470 though, so this is mostly moot.

(unless the analysis is happening in comptime mode, in which case any effects are applied where the defer is executed at compile time)

    The other question is whether an expression of type `noreturn` should be allowed in a `defer`.
    Unlike break/continue/return out of the block, which     would mean the block is exited a second time and calls into question what happens to the first exit, this is mostly a matter of policy, since it would simply mean the block exit is delayed indefinitely (or the process killed).

Yeah that's a good question.  Off the top of my head I don't really see a
reason to disallow it though, it doesn't seem like the sort of thing that
would
happen accidentally.  I'll ask the team what they think.  Is there a
stronger
reason you see that it should be allowed/disallowed?

E-Mail from Martin Wickham, 2022-02-28
--------------------------------------

    Also I've been wondering what the state of generic types vs. side effects is.
    A while back you mentioned you'd prefer a solution that does not segregate comptime functions between "can side-effect, but any types produced will be distinct for each invocation" (not memoized) and "cannot side-effect, but can produce types with consistent identity" (memoized).
    Has a consensus on this been reached already or is there still time for me to throw my proverbial hat in the ring?

I haven't made any significant progress on that, there's definitely still time to consider new options.
Detecting whether two types are identical is surprisingly difficult, because types are a sort of "lazy closure", where they capture the values of everything they close over, but they may not actually use all of those values depending on
comptime state.
But which upvalues (values from outer scopes) they use isn't known until all decls are evaluated, which may not ever happen if some decls are unused.

E-Mail from Martin Wickham, 2022-03-02
--------------------------------------

    Strangely, this function doesn't work as is either – of course it can't be compiled for runtime, but even when using it in a member definition I get a strange error (on 0.9.1) … any idea what that's about?
    Perhaps the compiler already enforces the kinds of restrictions that would result from typing ahead-of-time, right now?
    TBH this is the first time I'm trying to construct a  function that relies on single-pass evaluation but would not be affected by #1470 …

Interesting, I don't know why this doesn't compile.
I think it would probably work with stage 2.
However, I also agree that the language is better with a
separate typing pass.
I still need to consider it more but for now I think you are right.

    My plan here is basically to do memoization based on the type definition expression (and not do memoization on functions at all, notionally).
    Each type definition expression (struct/union/enum expression, @Type construct, or @Struct/@Enum/@Union with #10710) would then carry a Lexical ID (in practice it could be something like the file path + offset into the file) that enforces that ``struct {} != struct {}`` for instance.
    The memoization key would then be the Lexical ID, the values of named locals, and the container ID of named members.

Your understanding is correct, I think it only makes sense to deduplicate types which are created from the same lexical declaration.
But there's a complication that makes your plan more difficult: pointers to comptime vars.
The current plan for comptime var deduplication is that once a comptime var is closed over, it may no longer be modified, but with the caveat that the var is only "closed over" if comptime code inside the closure actually uses it.
This will be implemented in the compiler with modification counts, so the closure will check on usage to make sure that its upvalue has not been modified, and will mark the var as used.
The original scope will check that a var is not used as an upvalue before finalizing mutability.
If either check is violated, it's a modify-after-closure and causes a compile error.

These semantics might be more complicated than they need to be though.
I'm open to suggestions on this front.

[…]

    my idea for modelling the restrictions from #1470.
    Described in both my mail from 2022-02-21 (titled "comptime side-effect hygiene (#1470)") and in the meantime also written in detail in my repo (though I'm still lacking a prose introduction to that mechanic), would be good to know if I'm doing things right.
    As an aside: I quite like calling those restrictions "(comptime) side-effect hygiene",  what do you think about the name?

I think that's a good name for it.
Reading through your description of passing a set of mutable variables down the stack,
I think it makes sense.
It can even handle upvalues nicely, as long as you pass the list through comptime function calls.

E-Mail from Martin Wickham, 2022-03-03
--------------------------------------

    While writing example code for this, I realized there are a bunch of other situations where the compiler could use the same mechanisms to determine a value at comptime, but doesn't:

    -  block targeted by multiple breaks, but all of them have the same fixed value

This one we've thought about a bit, and decided it's probably not worth it.
So for now this is definitely a runtime value.

    - conditional that has all non-noreturn branches with the same fixed value

Same as above, this is runtime.

    - ``x and false`` / ``x or true`` (basically this would be a reverse short-circuit in a way, except it wouldn't surpress side effects)

I can't find the issue anymore, but I believe this was accepted.
I think this is the only case though, things like ``x * 0`` don't do this.
I'm not a huge fan of this behavior, and it's not implemented yet afaik, but it's in the plan.

    - multiplication/bit-and with 0, even if the other operand has unknown value

I don't think this is accepted yet but I think it's consistent with the ``and`` folding behavior.
If we keep that, we will probably add this.
Might be questionable for floats though, since it wouldn't hold for NaN and Inf.

E-Mail from Martin Wickham, 2022-03-18
--------------------------------------

    At the risk of overreaching, if the argument for resolving single breaks was its similarity to comptime-dispatched conditionals, then the two arguments could be combined to support comptime-resolving multiple-breaks-of-same-value. :P

Yep, I totally agree :P

E-Mail from Martin Wickham, 2022-03-21
--------------------------------------

    This isn't really inconsistent or anything, I was just thinking that it might be simpler to consider all expressions in field definitions part of the type definition that needs to be evaluated before things like getting the type info or creating an instance, instead of tracking each default value expression individually.

I agree, and I think stage 2 already works this way.

E-Mail from Martin Wickham, 2022-04-05
--------------------------------------

    This means that the type expression of all parameters (at least up to and including the next comptime/anytype parameter, if applicable) is evaluated before the argument expressions are evaluated, instead of only evaluating the type expression of each parameter just before the argument expression is evaluated.
    Not complaining (after all this matches how the field default values are evaluated in stage2 even if not needed for a compound literal), I just want to make sure there are no intentions to change this.

Yeah that's a weird one.
In stage 1 and the current impl of stage 2, parameters are pre-calculated up to the first one that depends on a previous value, and the rest are determined to be "generic".
This will likely change though, there's at minimum a plan to continue precalculating after the first generic.
Not sure if much more than that will change.
This area is sort of "under construction" so I think you can safely pick whatever behavior is most convenient for your
model.

    I was wondering how the compiler deals with references to generic functions, most importantly what the type looks like.
    Specifically it seems like the type has all parameters after the first comptime/anytype parameter, as well as the return type, set to anytype, which makes sense.
    However, I also saw that the comptime-ness of individual parameters is not reflected in the type of the function reference.
    It probably doesn't matter too much, since references to generic functions are clearly comptime-only, and they can easily be told apart by the fact that the return type will be anytype for the reference to any generic function.
    Again, it would be nice to get a short confirmation that this behavior is there to stay.

This is related to the same precalculation from the last question – it's done to determine the type.
In the current impl, the first parameter that is found to be generic flags itself and everything after it (including the return type) as generic.
But the plan is to eventually mark only the generic parameter and skip to the next one.
Missing comptimeness is probably a bug, but the existence of generic function types at all is something that doesn't really fit right in the current language, it might undergo heavy changes once we finish stage 2 and can dig into it.

    I noticed that you cannot actually write out that type signature in the language, since anytype is not accepted in the return type position.
    I would argue that it should, at least for function type expressions, so these functions don't have an unnameable type; though it might be useful to allow it for function definitions too, with the semantics that the compiler has to infer the type based on the body, similarly to how it can infer error sets (but perhaps this would encourage bad style, so maybe not?).

I believe the lack of `anytype` returns on function definitions is intentional and not planned to change.
It puts a nice damper on how many go-to-definition jumps you need to do to figure out the inferred type of something, and also significantly improves documentation (which can't always do full semantic analysis because you may have e.g. multiple different OS targets).

It might be reasonable in fn type expressions, but like I mentioned above generic fn types are sort of "under construction" and definitely need a closer look
before they will work well.

E-Mail from Martin Wickham, 2022-04-14
--------------------------------------

    types containing noreturn

Yeah, advanced uses of noreturn are pretty buggy in stage 1.
I think the best fix here is to disallow noreturn in fields.  This
would be consistent with the current behavior for empty enums
and unions, and solves complications with RLS that would occur
if we did allow noreturn fields.

But if we were to allow them, we would treat them like noreturn
for analysis reasons.  Calls to functions returning EmptyType
should be treated the same as calls to functions returning noreturn.

    What I would expect is this (this is what my model is trying to describe):

    -  Every subexpression that *must* have a comptime-known value (`comptime` expression, `comptime var` initializer, type expression in compound literal, first argument of @as, switch patterns, type annotations, comptime arguments to function calls, …) behaves like the `comptime` expression (contained function calls are done at comptime, `var` definitions are comptime, no runtime code is allowed).
    - Every subexpression that allows runtime expressions, whether or not the containing construct has special semantics for comptime-known values (like `if` condition, `switch` input value, `const` initializer), do not evaluate function calls at runtime (unless the call is inline, which is something I won't model in the scope of the thesis) and have `var` definitions be runtime.

I think the intended behavior is what you describe.  Stage 2 compiles your example correctly, giving a failure for doing too many backwards branches in the while(true).  https://zig.godbolt.org/z/er5GKrof9
You can test stage 2 generally with the `-fno-stage1` flag on a trunk build.
It's getting to the point where it might more accurately reflect the intended behavior than stage 1.

(Stage 2 also compiles the noreturn example successfully by treating the empty type like noreturn (if you replace the while(true) with unreachable), but there are RLS issues that may come up for more complex examples.)

[…]

            This means that the type expression of all parameters (at least up to and including the next comptime/anytype parameter, if applicable) is evaluated before the argument expressions are evaluated, instead of only evaluating the type expression of each parameter just before the argument expression is evaluated.
            Not complaining (after all this matches how the field default values are evaluated in stage2 even if not needed for a compound literal), I just want to make sure there are no intentions to change this.

        Yeah that's a weird one.
        In stage 1 and the current impl of stage 2, parameters are pre-calculated up to the first one that depends on a previous value, and the rest are determined to be "generic".

    How would "depends on" be determined? Lexical appearance of the name, the same rules as for member dependencies, or something else entirely?

It's currently "can be evaluated without reading the value", so ``if (false) a`` does not depend on ``a`` but ``if (true) a`` does.
As long as the value is not needed during analysis of the expression, there's no dependency.
A more specific definition might require defining all of sema, but let me know if you can think of any corner cases here.

            I was wondering how the compiler deals with references to generic functions, most importantly what the type looks like.
            Specifically it seems like the type has all parameters after the first comptime/anytype parameter, as well as the return type, set to anytype, which makes sense.
            However, I also saw that the comptime-ness of individual parameters is not reflected in the type of the function reference.
            It probably doesn't matter too much, since references to generic functions are clearly comptime-only, and they can easily be told apart by the fact that the return type will be anytype for the reference to any generic function.
            Again, it would be nice to get a short confirmation that this behavior is there to stay.

        This is related to the same precalculation from the last question – it's done to determine the type.
        In the current impl, the first parameter that is found to be generic flags itself and everything after it (including the return type) as generic.

    Wait, I thought the current implementation did some dependency analysis?
    Is my interpretation correct after all?
    Or do you use 'generic' to mean 'depends on comptime/anytype parameter' (instead of referring to a comptime/anytype parameter itself, which would be my intuitive understanding)

I'm kind of flying by the seat of my pants on these definitions but I think it should go like this:

- parameters typed as ``anytype`` are generic
- parameters that are marked comptime but do not have a generic type are not themselves generic
- parameters (or return types) whose type expressions depend on generic parameter types or comptime parameter values are generic
- a function is considered generic if it has any generic parameter types *or* any comptime parameters.

So `fn (comptime u32) void` is generic but has no generic parameters or return type

        But the plan is to eventually mark only the generic parameter and skip to the next one.

    How would that work if the next parameter uses the value or type of that parameter then?

Dependencies on generic parameters are also generic, so for example ``fn foo(x: u32, y: anytype, z: i16, comptime w: @TypeOf(y), comptime S: type) S``
is currently typed as ``fn(u32, generic, generic, generic, generic) generic`` but will eventually be ``fn(u32, generic, i16, generic comptime, type comptime) generic``
A function is generic if it has any generic or comptime parameters.

    Even if there is an interpretation of this objective that works, evaluating the parameter/return types out of order seems like complexity for unclear gain to me.

The gain is that more information shows up in type info.  The comptime var mutability rules prevent parameter type expressions from having side effects, so their order of execution is not actually observable.
In a way they are always executed in order, but twice.
The first time, specific kinds of failures will mark
a parameter as generic.
The second time, failures cause compile errors.

            I noticed that you cannot actually write out that type signature in the language, since anytype is not accepted in the return type position.
            I would argue that it should, at least for function type expressions, so these functions don't have an unnameable type; though it might be useful to allow it for function definitions too, with the semantics that the compiler has to infer the type based on the body, similarly to how it can infer error sets (but perhaps this would encourage bad style, so maybe not?).

        I believe the lack of `anytype` returns on function definitions is intentional and not planned to change.  It puts a nice damper on how many go-to-definition jumps you need to do to figure out the inferred type of something, and also significantly improves documentation (which can't always do full semantic analysis because you may have e.g. multiple different OS targets).

    Makes sense, though with that line of thought I don't really understand why inferred error sets are a thing, since they have the same issue.

Yeah, this is a weird line that we draw.
But at least it requires that the "shape" of the result is known.
You can see immediately the set of operations that it supports, but maybe not the full list of errors.
With the current rules, writing the return type expr is only nontrivial for code making heavy use of metaprogramming, which we kind of want to discourage a little bit.
Errors are all over though, so we think it's good to have some sugar for them.
If they were required to be listed, the easiest thing to do would be `unreachable` or `catch @panic()`, neither of which are usually good solutions.
