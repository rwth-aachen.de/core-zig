// SPDX-License-Identifier: CC0-1.0

fn gcd(a_: u32, b_: u32) u32 {
    var a = a_;
    var b = b_;
    while (true) {
        const r = a % b;
        if (r == 0) {return b;}
        a = b;
        b = r;
    }
}

const c1 = gcd(15, 6);

// =====

fn GcdComputer(a: u32) struct {t: type, v: u32} {
    var x = a / 2;
    const res = struct {
        fn compute(b: u32) u32 {
            x += 1;
            return gcd(x, b);
        }
    };
    const v = res.compute(255);
    x = a;
    return .{.t = res, .v = v};
}

const c2: struct {a: u32, b: u32} = comptime block: {
    const x = GcdComputer(40);
    break :block .{.a = x.t.compute(82), .b = x.v};
};

// =====

const std = @import("std");

pub fn main() !void {
    const stdout = std.io.getStdOut().writer();
    return stdout.print("{} {} {}", .{c1, c2.a, c2.b});
}
