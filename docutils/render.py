from docutils.core import Publisher
from docutils.io import FileInput, FileOutput
from docutils.readers.standalone import Reader
from docutils.parsers.rst import Directive, Parser, roles, directives
from docutils.writers.xetex import Writer
import docutils.nodes as nodes
from tok import tokenize, parse
import re

inp = FileInput(source_path='../README.rst', encoding='UTF-8')
outp = FileOutput(
    destination_path='../tex/readme.tex',
    encoding='UTF-8'
)
reader = Reader()
parser = Parser()
writer = Writer()

def math_role(name, rawtext, text, lineno, inliner, options=None, content=None):
    msgs = []
    def errmsg(*args):
        msgs.append(inliner.reporter.error(" ".join(args), line=lineno))
    text = " ".join(parse(tokenize(text, errmsg), errmsg, []))
    return ([
        inliner.problematic(rawtext, text, msgs[0]) if msgs else
        nodes.math(rawtext, text)
    ], msgs)

roles.register_local_role("m", math_role)

rulenamere = re.compile(r'\s*\(([a-z]+)\)\s+')
breakable_stack_items = {'(', '[', '{'}

def multiline_formula(lines):
    out = []
    errors = []
    stack = []
    indent_stack = [0]
    needs_break = False
    def errmsg(*args):
        errors.append((i, ' '.join(args)))
    for i, line in enumerate(lines):
        toks = []
        nesting_start = len(stack)
        leastdepth = len(stack)
        for tok in parse(tokenize(line, errmsg), errmsg, stack):
            leastdepth = min(leastdepth, len(stack))
            toks.append(tok)

        if any(t not in breakable_stack_items for t in stack[indent_stack[-1]:]):
            errmsg(f"linebreak not allowed in super-/subscript")

        basenest = 1 if leastdepth >= 1 else 0
        text = (
            "\\left. " * (max(0, nesting_start - leastdepth) + basenest)
            + ' '.join(toks)
            + ' \\right.' * (max(0, len(stack) - leastdepth) + basenest)
        )
        if leastdepth > indent_stack[-1]:
            indent_stack.append(leastdepth)
            out.append(nodes.raw('\n', '\\begin{adjustwidth}{1em}{}\\vspace{-\parskip}', format='latex'))
        elif leastdepth < indent_stack[-1]:
            while leastdepth < indent_stack[-1]:
                out.append(nodes.raw('\n', '\\vspace{-\parskip}\\end{adjustwidth}', format='latex'))
                indent_stack.pop()
            if leastdepth != indent_stack[-1]:
                errmsg("nesting level does not match nesting at any previous linebreak")
        elif needs_break:
            out.append(nodes.raw('\n', ' \\\\ ', format='latex'),)
        needs_break = True

        out.append(nodes.math(line, text))

    return out, errors

class RuleDirective(Directive):
    has_content = True
    optional_arguments = 2

    def run(self):
        errors = []
        if (self.arguments and self.arguments[0] != 'otherwise') or (len(self.arguments) == 2 and self.arguments[1] != 'if'):
            errors.append((self.lineno, "unexpected arguments"))
        premise = []
        bindings = []
        bindings_offset = None
        name = None
        conclusion = []
        conclusion_offset = None
        i = 0
        curarray = premise
        while True:
            try:
                line = self.content[i]
            except IndexError:
                break
            i += 1
            m = rulenamere.match(line)
            if m and (curarray is premise or curarray is bindings):
                if name is not None:
                    raise self.error("multiple rule names")
                name = m[1]
                conclusion_offset = i
                curarray = conclusion
                line = line[m.end():]
            elif line.startswith("let ") and curarray is premise:
                if bindings_offset is not None:
                    raise self.error("multiple bindings blocks")
                curarray = bindings
                bindings_offset = i
                line = line[4:]
            elif line == "":
                break
            curarray.append(line)
        if name is None:
            raise self.error("No rule name")
        container_start = i

        out = []
        if self.arguments:
            prefix = " ".join(self.arguments)
            out.append(nodes.paragraph(prefix, prefix))
        premise, errors_ = multiline_formula(premise)
        errors.extend((i, t) for i, t in errors_)
        out.extend(premise)
        if bindings_offset is not None:
            out.append(nodes.raw('', '', format='latex'))
            out.append(nodes.raw('', 'let', format='latex'))
            bindings, errors_ = multiline_formula(bindings)
            errors.extend((i + bindings_offset, t) for i, t, in errors_)
            out.extend(bindings)
            out.append(nodes.raw('', 'in', format='latex'))
        name_ = f"({name})"
        out.append(nodes.raw('', '', format='latex'))
        out.append(nodes.paragraph(name_, name_))
        conclusion, errors_ = multiline_formula(conclusion)
        errors.extend((i + conclusion_offset, t) for i, t, in errors_)
        out.extend(conclusion)
        out.append(nodes.raw('', '', format='latex'))
        if container_start < len(self.content):
            container = nodes.container('')
            self.state.nested_parse(self.content[container_start:], self.content_offset + container_start, container)
            out.append(container)

        if errors:
            raise self.error("\n".join(f"Line +{i}: {t}" for i, t in errors))
        return out

directives.register_directive("rule", RuleDirective)

class FnDefDirective(Directive):
    has_content = True

    def run(self):
        print(self.content_offset, self.content[0])
        msgs = []
        def errmsg(*args):
            msgs.append(f"Line {self.content_offset}: {' '.join(args)}")
        stack = []
        tokens = list(tokenize(self.content[0], errmsg))
        firstline = " ".join(parse(tokens, errmsg, stack))
        out = []
        out.append(nodes.raw('', '', format='latex'))
        out.append(nodes.math(self.content[0], firstline))
        out.append(nodes.raw('', '\\begin{adjustwidth}{1em}{}', format='latex'))
        deflines, errors = multiline_formula(self.content[1:])
        msgs.extend(f"Line {i + self.content_offset + 1}: {t}" for i, t in errors)
        if msgs:
            raise self.error("\n".join(msgs))
        out.extend(deflines)
        out.append(nodes.raw('', '\\end{adjustwidth}', format='latex'))
        return out

directives.register_directive("fndef", FnDefDirective)

class MLMath(Directive):
    has_content = True

    def run(self):
        out, errors = multiline_formula(self.content)
        if errors:
            raise self.error("\n".join(f"Line +{i}: {t}" for i, t in errors))
        return [nodes.raw('', '\\begin{adjustwidth}{1em}{}', format='latex')] + out + [nodes.raw('', '\\end{adjustwidth}', format='latex')]

directives.register_directive("mlmath", MLMath)

pub = Publisher(source=inp, destination=outp, reader=reader, parser=parser, writer=writer)
pub.publish(settings_overrides={
    "traceback": True,
    "raw_enabled": True,
    "documentclass": 'book',
    "latex_preamble": r"""
\usepackage{amsfonts}
\usepackage{changepage}
\usepackage{enumitem}
\setlistdepth{9}
\setlist[itemize,1]{label=$\bullet$}
\setlist[itemize,2]{label=$\bullet$}
\setlist[itemize,3]{label=$\bullet$}
\setlist[itemize,4]{label=$\bullet$}
\setlist[itemize,5]{label=$\bullet$}
\setlist[itemize,6]{label=$\bullet$}
\setlist[itemize,7]{label=$\bullet$}
\setlist[itemize,8]{label=$\bullet$}
\setlist[itemize,9]{label=$\bullet$}
\renewlist{itemize}{itemize}{9}
\usepackage{parskip}

\setcounter{secnumdepth}{3}
    """
})
