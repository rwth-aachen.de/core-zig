import re

tokre = re.compile(r'[@\.]?[a-zA-Z]+|\$[a-zA-Z_0-9]+|[_^]\(?|[\(\){}\[\],:|]')
escapere = re.compile(r'\\([\\_^])')
wordregex = r'([@\.]?)[a-zA-Zα-ωΓ-Ω]+'
atomregex = r'\$([a-zA-Z_0-9]+)'
wordre = re.compile(wordregex)
atomre = re.compile(atomregex)
wordoratomre = re.compile(f"{wordregex}|{atomregex}")
texescapere = re.compile('[%_^]')

def tokenize(inp, err):
    for s in inp.split():
        p = 0
        # TODO \
        while p < len(s):
            p_ = p
            start = len(s)
            while True:
                m = tokre.search(s, p_)
                if not m: break
                start = m.start()
                i = start - 1
                while i >= 0 and s[i] == '$': i -= 1
                if (start - i) % 2 == 1: break
                p_ = start + 1
                start = len(s)
                #err("escaped!")
            if p < start:
                yield s[p:start]
            if m:
                #err("match:", m[0])
                yield m[0]
                p = m.end()
            else:
                break
        yield " "
        #err("ws")

open_delims = {
    "^(": (")", "^{", "}"),
    "_(": (")", "_{", "}"),
    "(": (")", "\\left(", "\\right)"),
    "{": ("}", "\\left\\{", "\\right\\}"),
    "[": ("]", "\\left[", "\\right]"),
}
close_delims = {")", "}", "]"}
separators = {',', ':'}

replaced_tokens = {
    '/∈': '\\notin',
    '⊂=': '\\subseteq',
    '$^': '\\hat{}',
    '$_': '\\mathtt{\_}',
    '+%': '\\mathtt{+\%}',
    '-%': '\\mathtt{-\%}',
    '*%': '\\mathtt{-\%}',
    '%': '\\mathtt{\%}',
    '+$|': '\\mathtt{+|}',
    '-$|': '\\mathtt{-|}',
    '*$|': '\\mathtt{*|}',
    '$|': '\\mathtt{|}',
    '<<$|': '\\mathtt{<<|}',
    '<<$|=': '\\mathtt{<<|=}',
    '$/': '\setminus',
    '&': '\&'
}
range_ops = {
    '∪': ('\\cup', '\\bigcup'),
    '∩': ('\\cap', '\\bigcap'),
    'Σ': ('Σ', '\\sum')
}
quantifiers = {'∀', '∃'}
subscript_toks = {'_', '_('}

def isend(nex):
    return nex is None or nex == " " or nex in close_delims or nex in separators
def isstart(prev):
    return prev is None or prev == " " or prev in open_delims

def parse(inp, err, stack):
    it = iter(inp)
    prev = None
    cur = None
    try:
        nex = next(it)
    except StopIteration: return
    while nex is not None:
        prev = cur
        cur = nex
        #print(cur)
        try:
            nex = next(it)
        except StopIteration: nex = None
        if cur in open_delims:
            yield open_delims[cur][1]
            stack.append(cur)
        elif cur in close_delims:
            try:
                partner = stack.pop()
                try:
                    close, open_form, close_form = open_delims[partner]
                except IndexError:
                    err("stack error!")
                if close != cur: 
                    err(f"non-matching delimiters {partner} and {cur}")
                    continue
                yield close_form
            except IndexError:
                err("empty stack!")
        elif cur == '|':
            end = isend(nex)
            start = isstart(prev)
            if start and end:
                yield "\\:\\middle|\\:"
            elif start:
                yield "\\left|"
            elif end:
                yield "\\right|"
            else:
                err("not start, not end pipe")
        elif cur == '_':
            end = isend(nex)
            start = isstart(prev)
            if start and end:
                yield '\\_'
            elif not start and not end:
                yield '_'
            else:
                err("underscore neither subscript nor catchall")
        elif cur == '=':
            if prev and wordoratomre.match(prev):
                yield '\\!\\!='
            else:
                yield '='
        elif cur == ' ':
            if prev and wordoratomre.match(prev):
                if nex in quantifiers:
                    yield '\\;'
                elif nex and wordoratomre.match(nex):
                    yield '\\,'
        elif cur in range_ops:
            yield range_ops[cur][1 if nex in subscript_toks else 0]
        else:
            m = wordre.match(cur)
            if m:
                typ = m[1]
                if typ == '':
                    yield f'\\mathit{{{cur}}}' if len(cur) > 1 else cur
                elif typ == '@':
                    yield f'\\mathtt{{{cur}}}'
                else:
                    yield f'\\mathrm{{{cur}}}'
            elif cur in replaced_tokens:
                yield replaced_tokens[cur]
            else:
                m = atomre.match(cur)
                if prev == ' ':
                    yield '\\,'
                if m:
                    escaped = texescapere.sub('\\\\\\g<0>', m[1])
                    yield f'\\mathtt{{{escaped}}}'
                else:
                    yield cur

if __name__ == '__main__':
    teststr = r"U →_⊥ V = {c: U → V ∪ {⊥} $^ | ∃K ∈ 2^(U): (∀k ∈ U \ K: c(k) = ⊥) ∧ (∀k ∈ K: c(k) ≠ ⊥)}"
    teststr = r"$_ /∈ Ident"
    teststr = r"$_ = e ∈ Expr ∀e ∈ Expr"
    teststr = r"Domain_⊥(c) = U $/ {k ∈ U | c(k) = ⊥}"
    print(*parse(tokenize(teststr, print), print, []))
    print(wordre.match("κ"))
