// Spdx-License-Identifier: CC0-1.0
// the following would be part of the example in the thesis

fn strstr(haystack: [*:0]const u8, needle: [:0]const u8) ?usize {
    var offset: usize = 0;
    while (haystack[offset] != 0) {
        for (needle) |c, i| {
            if (haystack[offset + i] != c) {break;}
        } else {
            return offset;
        }
        offset += 1;
    }
    return null;
}

pub const pos: usize = strstr("abcde", "cd").?;

// =====
// the following is just for the sake of making it buildable as an executable
const std = @import("std");

pub fn main() !void {
    const stdout = std.io.getStdOut().writer();
    return stdout.print("{}", .{pos});
}
